-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 25, 2018 at 09:57 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sim_spp_smk`
--

-- --------------------------------------------------------

--
-- Table structure for table `spp_admin`
--

CREATE TABLE `spp_admin` (
  `id_admin` varchar(5) NOT NULL,
  `nm_admin` varchar(50) NOT NULL,
  `alamat_admin` varchar(50) NOT NULL,
  `no_hp_admin` varchar(20) NOT NULL,
  `status_del_admin` varchar(1) NOT NULL,
  `keterangan_admin` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_admin`
--

INSERT INTO `spp_admin` (`id_admin`, `nm_admin`, `alamat_admin`, `no_hp_admin`, `status_del_admin`, `keterangan_admin`, `password`) VALUES
('AD-01', 'Admin', 'Nginden 2 No. 10', '082233418595', '1', '', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `spp_anggota_rombel`
--

CREATE TABLE `spp_anggota_rombel` (
  `id_tr_rombel` varchar(40) NOT NULL,
  `id_rombel` varchar(15) NOT NULL,
  `no_induk` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_anggota_rombel`
--

INSERT INTO `spp_anggota_rombel` (`id_tr_rombel`, `id_rombel`, `no_induk`) VALUES
('TR-01', 'RO-01', '123456791'),
('TR-02', 'RO-01', '123456793'),
('TR-05', 'RO-02', '123456796'),
('TR-03', 'RO-02', '123456797'),
('TR-04', 'RO-02', '123456798'),
('TR-07', 'RO-03', '123456804'),
('TR-08', 'RO-03', '123456805'),
('TR-06', 'RO-03', '123456807'),
('TR-10', 'RO-04', '123456801'),
('TR-09', 'RO-04', '123456802');

-- --------------------------------------------------------

--
-- Table structure for table `spp_guru`
--

CREATE TABLE `spp_guru` (
  `id_guru` varchar(5) NOT NULL,
  `email_guru` varchar(70) NOT NULL,
  `nm_guru` varchar(50) NOT NULL,
  `gelar_depan` varchar(10) NOT NULL,
  `gelar_belakang` varchar(10) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `status_del_guru` varchar(1) NOT NULL,
  `keterangan_guru` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_guru`
--

INSERT INTO `spp_guru` (`id_guru`, `email_guru`, `nm_guru`, `gelar_depan`, `gelar_belakang`, `alamat`, `no_hp`, `status_del_guru`, `keterangan_guru`, `password`) VALUES
('GU-01', 'exokirishima@gmail.com', 'Pak Sigit', '', '', 'Kenjenran', '08xx', '1', '', '3227b218d06e31d3abb5ad54cb33b252'),
('GU-02', 'testing@test.com', 'Bu Anisa', '', '', 'Sukolilo', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('GU-03', 'testing@test.com', 'Pak Bayu', '', '', 'Nginden', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('GU-04', 'Testing@testing.co.id', 'Pak Angga', '', '', 'Petemon ', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('GU-05', 'testing@test.com', 'Pak Agung', '', '', 'Banyu Urip', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('GU-06', 'testing@test.com', 'Pak Elvi', '', '', 'Simo', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `spp_petugas`
--

CREATE TABLE `spp_petugas` (
  `id_petugas` varchar(5) NOT NULL,
  `email_petugas` varchar(70) NOT NULL,
  `nm_petugas` varchar(50) NOT NULL,
  `alamat_petugas` varchar(50) NOT NULL,
  `no_hp_petugas` varchar(20) NOT NULL,
  `status_del_petugas` varchar(1) NOT NULL,
  `keterangan_petugas` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_petugas`
--

INSERT INTO `spp_petugas` (`id_petugas`, `email_petugas`, `nm_petugas`, `alamat_petugas`, `no_hp_petugas`, `status_del_petugas`, `keterangan_petugas`, `password`) VALUES
('PE-01', 'exokirishima@gmail.com', 'Mardianto', 'Kenjeran', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('PE-02', 'testing@test.com', 'Bu Tun', 'Sukolilo', '08xx', '1', '', 'e10adc3949ba59abbe56e057f20f883e'),
('PE-03', 'testing@test.com', 'Pak Andre', 'Nginden', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('PE-04', 'testing@test.com', 'Pak Anton', 'Panjang Jiwo', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b'),
('PE-05', 'testing@test.com', 'Pak Lutfy', 'Banyu Urip', '08xx', '1', '', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `spp_prodi`
--

CREATE TABLE `spp_prodi` (
  `id_prodi` varchar(5) NOT NULL,
  `nm_prodi` varchar(50) NOT NULL,
  `status_aktif_prodi` varchar(1) NOT NULL,
  `keterangan_prodi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_prodi`
--

INSERT INTO `spp_prodi` (`id_prodi`, `nm_prodi`, `status_aktif_prodi`, `keterangan_prodi`) VALUES
('TITL', 'Teknik Instalasi Tenaga Listrik', '1', 'testing update 2'),
('TKJ', 'Teknik Komputer & Jaringan', '1', 'testing update 3'),
('TKR', 'Teknik Kendaraan Ringan ', '1', 'testing update 4'),
('TPM', 'Teknik Pemesinan', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `spp_rombel`
--

CREATE TABLE `spp_rombel` (
  `id_rombel` varchar(15) NOT NULL,
  `id_ta` varchar(10) NOT NULL,
  `id_prodi` varchar(5) NOT NULL,
  `id_guru` varchar(5) NOT NULL,
  `tingkat` varchar(2) NOT NULL,
  `nm_rombel` varchar(10) NOT NULL,
  `nominal_spp` mediumint(9) NOT NULL,
  `keterangan_rombel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_rombel`
--

INSERT INTO `spp_rombel` (`id_rombel`, `id_ta`, `id_prodi`, `id_guru`, `tingkat`, `nm_rombel`, `nominal_spp`, `keterangan_rombel`) VALUES
('RO-01', '2016-2017', 'TKJ', 'GU-01', '10', 'TKJ-1', 150000, 'Siswa TKJ angkatan 2016'),
('RO-02', '2016-2017', 'TKR', 'GU-04', '10', 'TKR-1', 150000, 'Siswa TKR angkatan 2016'),
('RO-03', '2016-2017', 'TITL', 'GU-05', '10', 'TITL-1', 150000, ''),
('RO-04', '2016-2017', 'TPM', 'GU-06', '10', 'TPM-1', 150000, '');

-- --------------------------------------------------------

--
-- Table structure for table `spp_siswa`
--

CREATE TABLE `spp_siswa` (
  `jurusan_siswa` varchar(5) NOT NULL,
  `no_induk` varchar(25) NOT NULL,
  `nm_siswa` varchar(50) NOT NULL,
  `kota_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `nm_ibu` varchar(50) NOT NULL,
  `no_hp_1` varchar(20) NOT NULL,
  `no_hp_2` varchar(20) NOT NULL,
  `status_del_siswa` varchar(1) NOT NULL,
  `keterangan_siswa` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_siswa`
--

INSERT INTO `spp_siswa` (`jurusan_siswa`, `no_induk`, `nm_siswa`, `kota_lahir`, `tgl_lahir`, `alamat`, `nm_ibu`, `no_hp_1`, `no_hp_2`, `status_del_siswa`, `keterangan_siswa`, `password`) VALUES
('TKJ', '123456789', 'Eko', 'Surabaya', '1990-12-20', 'Kenjenran', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKJ', '123456790', 'Yogi', 'Surabaya', '1991-12-20', 'Sukolilo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKJ', '123456791', 'Amar', 'Surabaya', '1992-12-20', 'Nginden', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKJ', '123456792', 'Wahyu', 'Surabaya', '1993-12-20', 'Panjang Jiwo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKJ', '123456793', 'Arya', 'Surabaya', '1994-12-20', 'Banyu Urip', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKR', '123456794', 'Nabila', 'Gresik', '1995-12-20', 'Kenjenran', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKR', '123456795', 'Yani', 'Gresik', '1996-12-20', 'Sukolilo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKR', '123456796', 'Ivon', 'Gresik', '1997-12-20', 'Nginden', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKR', '123456797', 'Dinda', 'Gresik', '1998-12-20', 'Panjang Jiwo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TKR', '123456798', 'Irvan', 'Gresik', '1999-12-20', 'Banyu Urip', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TPM', '123456799', 'Nurdin', 'Gresik', '2000-12-20', 'Kenjenran', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TPM', '123456800', 'Nukman', 'Gresik', '2001-12-20', 'Sukolilo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TPM', '123456801', 'Dion', 'Gresik', '2002-12-20', 'Nginden', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TPM', '123456802', 'Danil', 'Gresik', '2003-12-20', 'Panjang Jiwo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TPM', '123456803', 'Dipta', 'Gresik', '2004-12-20', 'Banyu Urip', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TITL', '123456804', 'Deny', 'Sidoarjo', '2005-12-20', 'Kenjenran', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TITL', '123456805', 'Ilham', 'Sidoarjo', '2006-12-20', 'Sukolilo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TITL', '123456806', 'Toyo', 'Sidoarjo', '2007-12-20', 'Nginden', 'Ibu Pertiwi', '08xx', '08xx', '1', '', ''),
('TITL', '123456807', 'Amshar', 'Sidoarjo', '2008-12-20', 'Panjang Jiwo', 'Ibu Pertiwi', '08xx', '08xx', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `spp_ta`
--

CREATE TABLE `spp_ta` (
  `id_ta` varchar(10) NOT NULL,
  `status_aktif_ta` varchar(1) NOT NULL,
  `keterangan_ta` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_ta`
--

INSERT INTO `spp_ta` (`id_ta`, `status_aktif_ta`, `keterangan_ta`) VALUES
('2016-2017', '1', 'testing update yo'),
('2017-2018', '0', ''),
('2018-2019', '0', ''),
('2019-2020', '0', ''),
('2020-2021', '0', 'coba'),
('2022-2023', '0', 'coba'),
('2023-2024', '0', 'testing update 1'),
('2024-2025', '0', 'testing update 2 lagi'),
('2025-2026', '0', 'testing insert'),
('2026-2027', '0', 'Testing Validation'),
('2027-2028', '0', 'testing update');

-- --------------------------------------------------------

--
-- Table structure for table `spp_transaksi`
--

CREATE TABLE `spp_transaksi` (
  `id_tr_spp` varchar(25) NOT NULL,
  `id_tr_rombel` varchar(40) NOT NULL,
  `id_petugas` varchar(5) NOT NULL,
  `waktu_tr` datetime NOT NULL,
  `bulan` varchar(3) NOT NULL,
  `dibayarkan` mediumint(9) NOT NULL,
  `status_del_tr` varchar(1) NOT NULL,
  `keterangan_tr` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp_transaksi`
--

INSERT INTO `spp_transaksi` (`id_tr_spp`, `id_tr_rombel`, `id_petugas`, `waktu_tr`, `bulan`, `dibayarkan`, `status_del_tr`, `keterangan_tr`) VALUES
('TRANS-01', 'TR-01', 'PE-02', '2018-12-23 12:16:12', 'JAN', 150000, '0', 'LUNAS'),
('TRANS-02', 'TR-06', 'PE-02', '2018-12-23 12:36:47', 'JAN', 150000, '0', 'LUNAS'),
('TRANS-03', 'TR-01', 'PE-02', '2018-12-23 12:51:01', 'FEB', 150000, '0', 'LUNAS'),
('TRANS-04', 'TR-03', 'PE-01', '2018-12-23 15:51:06', 'JAN', 150000, '0', 'LUNAS'),
('TRANS-05', 'TR-03', 'PE-01', '2018-12-23 15:51:12', 'FEB', 150000, '0', 'LUNAS'),
('TRANS-06', 'TR-04', 'PE-01', '2018-12-23 15:51:24', 'JAN', 150000, '0', 'LUNAS'),
('TRANS-07', 'TR-05', 'PE-01', '2018-12-23 15:51:34', 'JAN', 150000, '0', 'LUNAS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `spp_admin`
--
ALTER TABLE `spp_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `spp_anggota_rombel`
--
ALTER TABLE `spp_anggota_rombel`
  ADD PRIMARY KEY (`id_tr_rombel`),
  ADD KEY `id_rombel` (`id_rombel`,`no_induk`),
  ADD KEY `no_induk` (`no_induk`);

--
-- Indexes for table `spp_guru`
--
ALTER TABLE `spp_guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `spp_petugas`
--
ALTER TABLE `spp_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `spp_prodi`
--
ALTER TABLE `spp_prodi`
  ADD PRIMARY KEY (`id_prodi`);

--
-- Indexes for table `spp_rombel`
--
ALTER TABLE `spp_rombel`
  ADD PRIMARY KEY (`id_rombel`),
  ADD KEY `id_ta` (`id_ta`),
  ADD KEY `id_prodi` (`id_prodi`,`id_guru`),
  ADD KEY `id_guru` (`id_guru`);

--
-- Indexes for table `spp_siswa`
--
ALTER TABLE `spp_siswa`
  ADD PRIMARY KEY (`no_induk`);

--
-- Indexes for table `spp_ta`
--
ALTER TABLE `spp_ta`
  ADD PRIMARY KEY (`id_ta`);

--
-- Indexes for table `spp_transaksi`
--
ALTER TABLE `spp_transaksi`
  ADD PRIMARY KEY (`id_tr_spp`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `spp_transaksi_ibfk_3` (`id_tr_rombel`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `spp_anggota_rombel`
--
ALTER TABLE `spp_anggota_rombel`
  ADD CONSTRAINT `spp_anggota_rombel_ibfk_1` FOREIGN KEY (`no_induk`) REFERENCES `spp_siswa` (`no_induk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spp_anggota_rombel_ibfk_2` FOREIGN KEY (`id_rombel`) REFERENCES `spp_rombel` (`id_rombel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spp_rombel`
--
ALTER TABLE `spp_rombel`
  ADD CONSTRAINT `spp_rombel_ibfk_1` FOREIGN KEY (`id_ta`) REFERENCES `spp_ta` (`id_ta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spp_rombel_ibfk_2` FOREIGN KEY (`id_prodi`) REFERENCES `spp_prodi` (`id_prodi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spp_rombel_ibfk_3` FOREIGN KEY (`id_guru`) REFERENCES `spp_guru` (`id_guru`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spp_transaksi`
--
ALTER TABLE `spp_transaksi`
  ADD CONSTRAINT `spp_transaksi_ibfk_2` FOREIGN KEY (`id_petugas`) REFERENCES `spp_petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spp_transaksi_ibfk_3` FOREIGN KEY (`id_tr_rombel`) REFERENCES `spp_anggota_rombel` (`id_tr_rombel`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
