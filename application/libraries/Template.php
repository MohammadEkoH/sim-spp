<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template
{
	function __construct()
	{
		$this->ci =&get_instance();
	}

	function auth($template, $data='')
	{
		$data_['content'] 	= $this->ci->load->view($template, $data, TRUE); 

		$this->ci->load->view('auth/vtemplate', $data_);
	}

	function admin($template, $data='')
	{ 
		$data_['content'] 	= $this->ci->load->view($template, $data, TRUE);
		$data_['modal']		= $this->ci->load->view('admin/vmodal', $data, TRUE); 

		$this->ci->load->view('admin/vtemplate', $data_);
	}

	function petugas($template, $data='')
	{ 
		$data_['content'] 	= $this->ci->load->view($template, $data, TRUE);
		$data_['modal']		= $this->ci->load->view('petugas/vmodal', $data, TRUE); 

		$this->ci->load->view('petugas/vtemplate', $data_);
	}

	function guru($template, $data='')
	{ 
		$data_['content'] 	= $this->ci->load->view($template, $data, TRUE);
		$data_['modal']		= $this->ci->load->view('guru/vmodal', $data, TRUE);

		$this->ci->load->view('guru/vtemplate', $data_);
	}

}
?>
