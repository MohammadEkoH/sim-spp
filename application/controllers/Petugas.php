<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

	public function index()
	{
		$id = $this->session->userdata('id_petugas');
		if ($id) {
			$where = array('id_petugas' => $id);
			$query = $this->M_petugas->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_petugas' => $row->nm_petugas,
						'email'		 => $row->email_petugas
					); 
			} 
			$this->template->petugas('petugas/vdashboard', $data);
		}else {
			redirect('login');
		}
	}

	function pembayaran()
	{
		$id = $this->session->userdata('id_petugas');
		if ($id) {
			$where = array('id_petugas' => $id);
			$query = $this->M_petugas->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_petugas' => $row->nm_petugas,
						'email'		=> $row->email_petugas
					); 
			} 
			$this->template->petugas('petugas/vpembayaran', $data);
		}else {
			redirect('login');
		}
	}

	function pencarian_data_pembayaran($cari="", $jurusan="")
	{
		header('Content-Type: application/json');
		$cari = array('nm_siswa' => $cari); 
		$jurusan = array('jurusan_siswa' => $jurusan); 
		$query = $this->M_petugas->pencarian_data_pembayaran($cari, $jurusan);
		$data = array();
		foreach ($query->result() as $row)
		{ 
			// $ec = str_replace(array('+', '=', '/', '&'), array('!', '*', '~', '^'), $this->encryption->encrypt($row->id_tr_rombel));
			$data[] = array(
				'id_tr_rombel'=> $row->id_tr_rombel,
				'no_induk' => $row->no_induk,
				// 'dec' => $this->encryption->decrypt(str_replace(array('!', '*', '~', '^'), array('+', '=', '/', '&'), $ec)),
				'nm_siswa'		=> $row->nm_siswa,
				'tingkat'			=> $row->tingkat,
				'nm_rombel'		=> $row->nm_rombel,
				'status_del_siswa' => $row->status_del_siswa,
				'id_ta'	=> $row->id_ta,
				'status_aktif_ta' => $row->status_aktif_ta

			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	function get_where_detail_anggota_pembayaran($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_tr_rombel' => $id);
		$query = $this->M_petugas->get_where_detail_anggota_pembayaran($where);
			$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_tr_rombel'	=> $row->id_tr_rombel,
				'id_rombel' 	=> $row->id_rombel,
				'no_induk'		=> $row->no_induk,
				'nm_siswa'		=> $row->nm_siswa,
				'tingkat' 		=> $row->tingkat,
				'nm_rombel' 	=> $row->nm_rombel,
				'nominal_spp' 	=> $row->nominal_spp,
				'nm_prodi' 		=> $row->nm_prodi,
				'nm_guru' 		=> $row->nm_guru,
				'id_ta'			=> $row->id_ta
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	} 

	function data_pembayaran()
	{ 
		$id = $this->session->userdata('id_petugas');
		$id_tr_rombel = $this->uri->segment(4);
		if ($id) {
			$where = array('id_petugas' => $id);
			$query = $this->M_petugas->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_petugas' => $row->nm_petugas,
						'id_tr_rombel' => $id_tr_rombel,
						'email'		=> $row->email_petugas
					); 
			} 
			$data['id_tr_rombel'] = $this->uri->segment(4);
			$this->template->petugas('petugas/vdata_pembayaran', $data);
		}else {
			redirect('login');
		}
	}

	function insert_pembayaran()
	{
		$query = $this->M_petugas->get_id_tr();
		$id = $query->row();
		$id = $id->id_tr_spp;

		$id_tr = substr($id, 6, 7);
		if ($id_tr == 0) {
			$id_tr = 1;
		}else {
			$id_tr++;
		}

		$id_tr = $this->kode->kode_ID("TRANS-", $id_tr, $query);
		$id_tr_rombel = $this->input->post('id_tr_rombel');
		$id_petugas = $this->input->post('id_petugas');
		date_default_timezone_set("Asia/Kolkata");
		$waktu_tr =  Date('Y-m-d H:i:s');
		$bulan = $this->input->post('bulan');
		$nominal_spp = $this->input->post('nominal_spp');

		$data = array(
			'id_tr_spp' => $id_tr,
			'id_tr_rombel' => $id_tr_rombel,
			'id_petugas' => $id_petugas,
			'waktu_tr' => $waktu_tr,
			'bulan' => $bulan,
			'dibayarkan' => $nominal_spp,
			'status_del_tr' => 0,
			'keterangan_tr'=> "LUNAS"
		); 
		$this->M_petugas->insert_data('spp_transaksi', $data);
		echo "Bayar Berhasil!";
	}

	function get_where_data_pembayaran_siswa($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_tr_rombel' => $id);
		$query = $this->M_petugas->where_data_pembayaran_siswa($where);
		$data = array();
		foreach ($query->result() as $row)
		{
			$nama = substr($row->nm_siswa, 0, 16).'...';
			$nominal_spp_show = "Rp.".number_format($row->nominal_spp,0,',','.').",-";
			$data[] = array(
				'id_tr_rombel'=> $row->id_tr_rombel,
				'nm_siswa'		=> $nama,
				'tingkat' 		=> $row->tingkat,
				'nm_rombel' 	=> $row->nm_rombel, 
				'id_ta'			=> $row->id_ta,
				'nominal_spp'	=> $row->nominal_spp, 
				'nominal_spp_show'=> $nominal_spp_show,
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	} 

	function get_petugas()
	{
		header('Content-Type: application/json');
		$id = $this->session->userdata('id_petugas');

		$data[] = array(
			'id_petugas'=> $id
		);

		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function get_show_data_pembayaran($id="")
	{
		header('Content-Type: application/json');
		$where = array('spp_anggota_rombel.id_tr_rombel' => $id);
		$query = $this->M_petugas->where_show_data_pembayaran($where);
		$data = array();
		foreach ($query->result() as $row)
		{
			if ($row->bulan == "JAN") {
				$bulan = "Januari";
			}else if($row->bulan == "FEB"){
				$bulan = "Februari";
			}else if($row->bulan == "MAR"){
				$bulan = "Maret";
			}else if($row->bulan == "APR"){
				$bulan = "April";
			}else if($row->bulan == "MEI"){
				$bulan = "Mei";
			}else if($row->bulan == "JUN"){
				$bulan = "Juni";
			}else if($row->bulan == "JUL"){
				$bulan = "Juli";
			}else if($row->bulan == "AGU"){
				$bulan = "Agustus";
			}else if($row->bulan == "SEP"){
				$bulan = "September";
			}else if($row->bulan == "NOV"){
				$bulan = "November";
			}else if($row->bulan == "DES"){
				$bulan = "Desember";
			} 

			$data[] = array(
				'id_tr_rombel'	=> $row->id_tr_rombel,
				'id_tr_spp'		=> $row->id_tr_spp,
				'nm_siswa'		=> $row->nm_siswa,
				'id_ta'			=> $row->id_ta, 
				'tingkat' 		=> $row->tingkat,
				'nm_rombel' 	=> $row->nm_rombel,
				'bulan'			=> $bulan,
				'nm_petugas'	=> $row->nm_petugas,
				'waktu_tr'		=> $row->waktu_tr,
				'dibayarkan'	=> $row->dibayarkan,
				'keterangan'	=> $row->keterangan_tr
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function get_ta()
	{
		header('Content-Type: application/json'); 
		$query = $this->M_petugas->get_data('spp_ta', 'id_ta');
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_ta' => $row->id_ta,
				'status_aktif_ta' => $row->status_aktif_ta
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	function where_ta($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_rombel' => $id);
    	$query = $this->M_admin->get_where_rombel($where);
		$data = array();
		foreach ($query->result() as $row)
		{
				$data[] = array(
					'id_rombel' => $row->id_rombel,
					'id_ta' => $row->id_ta,
					'status_aktif_ta' => $row->status_aktif_ta,
					'tingkat' => $row->tingkat,
					'nm_rombel' => $row->nm_rombel,
					'id_prodi' => $row->id_prodi,
					'id_guru' => $row->id_guru,
					'nominal_spp' => $row->nominal_spp,
					'keterangan_rombel' => $row->keterangan_rombel
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function akun()
	{
		$id = $this->session->userdata('id_petugas');
		if ($id) {
			$where = array('id_petugas' => $id);
			$query = $this->M_petugas->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_petugas' => $row->nm_petugas,
						'email'		=> $row->email_petugas 
					); 
			} 
			$data['callout'] = "";
			$this->template->petugas('petugas/vakun', $data);
		}else {
			redirect('login');
		}
	}

	function akun_user()
	{
		$id = $this->session->userdata('id_petugas');  
		$where = array('id_petugas' => $id);
		$query = $this->M_petugas->get_data_akun($where);
		foreach ($query->result() as $row) {
			$data = array(
				'email_petugas'	=> $row->email_petugas,
				'nm_petugas' 	=> $row->nm_petugas,
				'alamat_petugas'=> $row->alamat_petugas,
				'no_hp_petugas' => $row->no_hp_petugas
			); 
		}  
		echo json_encode($data); 
	}

	function update_user()
	{ 
		$id = $this->session->userdata('id_petugas'); 
		$where = array('id_petugas' => $id); 
		
		$email = $this->input->post('email_petugas');
		$nama = $this->input->post('nm_petugas');
		$alamat = $this->input->post('alamat_petugas');
		$no_hp = $this->input->post('no_hp_petugas');
 
		$data = array(
			'email_petugas' => $email,
			'nm_petugas'	=> $nama,
			'alamat_petugas'=> $alamat,
			'no_hp_petugas'	=> $no_hp
		);
		$this->M_admin->update_data($where, 'spp_petugas', $data);
		echo "Update Berhasil";
	}

	function cek_pass(){ 
		$id = $this->session->userdata('id_petugas');    

		$lama		= md5($this->input->post('password_lama'));
		$baru		= $this->input->post('password_baru');
		$confirm	= $this->input->post('password_confirm');

		$where = array(
			'id_petugas'	=> $id,
			'password' 		=> $lama
		);

		$cek = $this->M_petugas->get_data_akun($where)->row(); 

		$pass = $cek->password; 

		if($pass == $lama &&  $baru == $confirm){
			$where_id = array( 'id_petugas' => $id );
			$baru = md5($baru);  
			$new = array('password' => $baru);
			
			$this->M_admin->update_data($where_id, 'spp_petugas', $new);

			$data = 'berhasil';
		}else{
			$data = 'gagal';
		}	

		$data = array('data' => $data);

		echo json_encode($data); 
	}

	function struk()
	{ 
		$id = $this->session->userdata('id_petugas');
		if ($id) {
			$where = array('id_petugas' => $id);
			$query = $this->M_petugas->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_petugas' => $row->nm_petugas,
						'email'		=> $row->email_petugas
					); 
			} 

			$data['id_tr_spp'] = $this->input->post('id_tr_spp');
			$data['id_tr_rombel'] = $this->input->post('id_tr_rombel');
			$this->load->view('petugas/vstruk', $data);
		}else {
			redirect('login');
		}
	}

	function get_tr_struk($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_tr_spp' => $id);
		$query = $this->M_petugas->where_data_pembayaran($where);
		$data = array();
		foreach ($query->result() as $row)
		{
			$tgl_tr = date('H:i:s', strtotime($row->waktu_tr));
			$waktu_tr =  Date('d-m-Y', strtotime($row->waktu_tr));
			$dibayarkan = "Rp.".number_format($row->dibayarkan,0,',','.').",-";
			$string = $row->nm_petugas;
			$nm_petugas = substr($string, 0, 18);
			$bulan = $row->bulan;

			if ($bulan == "JAN") {
				$bulan = "Januari";
			}else if($bulan == "FEB"){
				$bulan = "Februari";
			}else if($bulan == "MAR"){
				$bulan = "Maret";
			}else if($bulan == "APR"){
				$bulan = "April";
			}else if($bulan == "MEI"){
				$bulan = "Mei";
			}else if($bulan == "JUN"){
				$bulan = "Juni";
			}else if($bulan == "JUL"){
				$bulan = "Juli";
			}else if($bulan == "AGU"){
				$bulan = "Agustus";
			}else if($bulan == "SEP"){
				$bulan = "September";
			}else if($bulan == "NOV"){
				$bulan = "November";
			}else if($bulan == "DES"){
				$bulan = "Desember";
			} 
			
			$data[] = array(
				'id_tr_rombel' => $row->id_tr_rombel,
				'bulan'	=> $bulan,
				'tgl_tr' => $tgl_tr,
				'waktu_tr' => $waktu_tr,
				'dibayarkan' => $dibayarkan,
				'nm_petugas' => $nm_petugas,
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	function get_siswa_struk($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_tr_rombel' => $id);
		$query = $this->M_petugas->where_data_pembayaran_siswa($where);
		$data = array();
		foreach ($query->result() as $row)
		{
				$string = $row->nm_siswa;
				$nm_siswa = substr($string, 0, 25);

				$data[] = array(
					'id_tr_rombel'=> $row->id_tr_rombel,
					'id_ta' 			=> $row->id_ta,
					'no_induk'		=> $row->no_induk,
					'nm_siswa'		=> $nm_siswa,
					'tingkat' 		=> $row->tingkat,
					'nm_rombel' 	=> $row->nm_rombel,
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}
}
