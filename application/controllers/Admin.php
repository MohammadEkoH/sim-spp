<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/vdashboard', $data); 
		}else {
			redirect('login');
		} 
	}


	// Dashboard ------------------------------------------------------------------------

	function get_jumlah_siswa()
	{
		$query  = $this->M_admin->get_jumlah_siswa();
		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array(  
				'id_prodi'	=> $row->id_prodi,
				'Total'		=> $row->Total 
			);    
		}     
		$output = array(
			'data' => $data
		);
		header('Content-Type: application/json');   
		echo json_encode($output);
		
	}

	function data_jurusan($jurusan='')
	{
		$jurusan = $this->uri->segment(4);
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			} 

			switch ($jurusan) {
				case 'TKJ':
					$data['jurusan'] = 'TKJ';
					break;

				case 'TKR':
					$data['jurusan'] = 'TKR';
				break;

				case 'TITL':
					$data['jurusan'] = 'TITL';
				break;

				case 'TPM':
					$data['jurusan'] = 'TPM';
				break;
				
				default:
					# code...
					break;
			}

			$this->template->admin('admin/vdata_jurusan', $data); 
		}else {
			redirect('login');
		}  
	}

	// Get where rombel
	function get_where_rombel_dashboard($id_prodi="", $id_ta="")
	{
		header('Content-Type: application/json'); 
		$id_ta = array('spp_rombel.id_ta' => $id_ta);  
		$id_prodi = array('id_prodi' => $id_prodi);   
		$query = $this->M_admin->get_where_rombel_dashboard($id_prodi, $id_ta);
		foreach ($query->result() as $row)
		{
		$data[] = array(
				'id_rombel' => $row->id_rombel,
				'rombel'	=> $row->tingkat." - (".$row->nm_rombel.")",
				'id_ta' 	=> $row->id_ta
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	} 

	function get_where_show_data_dashboard($id_ta, $id_rombel)
	{ 
		header('Content-Type: application/json');
		// $id_rombel = array('spp_anggota_rombel.id_rombel' => $id_rombel);  
		// $id_ta = array('spp_rombel.id_ta' => $id_ta);  
    	$query = $this->M_admin->get_where_show_data_dashboard($id_ta, $id_rombel); 
		$data = array();
		foreach ($query->result() as $row)
		{
			// if($row->keterangan_tr == 'LUNAS'){
			// 	$keterangan = 'LUNAS';
			// }else{ 
			// 	$keterangan = 'BELUM LUNAS'; 
			// }

			$Total = "Rp.".number_format($row->Total,0,',','.').",-";
			$data[] = array(
				// 'id_tr_rombel'	=> $row->id_tr_rombel, 
				// 'id_rombel'		=> $row->id_rombel,
				// 'nm_siswa'		=> $row->nm_siswa,
				// 'tingkat'		=> $row->tingkat,
				// 'nm_rombel'		=> $row->nm_rombel, 
				// 'id_ta'	 		=> $row->id_ta,
				// 'status_del_siswa' => $row->status_del_siswa,
				// 'bulan'			=> "JAN",
				'no_induk'	=> $row->no_induk,
				'nm_siswa' 	=> $row->nm_siswa,
				'Januari'	=> $row->Januari,
				'Februari'	=> $row->Februari,
				'Maret'		=> $row->Maret,
				'April'		=> $row->April,
				'Mei'		=> $row->Mei,
				'Juni'		=> $row->Juni,
				'Juli'		=> $row->Juli,
				'Agustus'	=> $row->Agustus,
				'September'	=> $row->September,
				'Oktober'	=> $row->Oktober,
				'November'	=> $row->November,
				'Desember'	=> $row->Desember,
				'Total'		=> $Total
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	} 

	// End Dashboard ------------------------------------------------------------------------

	// Guru ------------------------------------------------------------------------

	function guru()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/master_data/vguru', $data);
		}else {
			redirect('login');
		}  
	}

	function insert_guru()
	{ 

		$query = $this->M_admin->get_id_guru();
		$id = $query->row();
		$id = $id->id_guru;

		$id_guru = substr($id, 3, 4);
		if ($id_guru == 0) {
			$id_guru = 1;
		}else {
			$id_guru++;
		}

		$this->form_validation->set_rules('email_guru', 'Email Guru', 'required');
		$this->form_validation->set_rules('nama_guru', 'Nama Guru', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('no_hp', 'No HP', 'required');
		$this->form_validation->set_rules('nama_guru', 'Nama Guru', 'required');  

		$id_guru = $this->kode->kode_ID("GU-", $id_guru, $query);
		$email_guru = $this->input->post('email_guru');
		$nm_guru = $this->input->post('nama_guru');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$keterangan = $this->input->post('keterangan');
		$status = 1;
 
		if ($this->form_validation->run() == FALSE)
		{
			echo "Please fill in the form";
		}
		else
		{
			$data = array(
				'id_guru' => $id_guru,
				'email_guru' => ucwords($email_guru),
				'nm_guru' => ucwords($nm_guru),
				'alamat' => ucwords($alamat),
				'no_hp' => $no_hp,
				'status_del_guru' => $status,
				'keterangan_guru' => $keterangan
			);   
			$this->M_admin->insert_data('spp_guru', $data);
			echo "Insert Berhasil!";
		}
		
		 
	}
 
	function update_guru()
	{
		$id_guru = $this->input->post('id_guru');
		$where = array('id_guru' => $id_guru);

		$email_guru = $this->input->post('email_guru');
		$nm_guru = $this->input->post('nm_guru');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'email_guru' => $email_guru,
			'nm_guru' => ucwords($nm_guru),
			'alamat' => ucwords($alamat),
			'no_hp' => $no_hp,
			'keterangan_guru' => $keterangan
		);
		$this->M_admin->update_data($where, 'spp_guru', $data);
		echo "Update Berhasil!";
	}

	function pencarian_guru($nama=null, $status=null)
	{
		header('Content-Type: application/json'); 
		$nama = array('nm_guru' => $nama);
		$status = array('status_del_guru' => $status);
		$query = $this->M_admin->pencarian_guru($nama, $status);
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_guru'		=> $row->id_guru,
				'email_guru' 	=> $row->email_guru,
				'nm_guru' 		=> $row->nm_guru,
				'alamat' 		=> $row->alamat,
				'no_hp' 		=> $row->no_hp,
				'status_del_guru' => $row->status_del_guru,
				'keterangan_guru' => $row->keterangan_guru
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output); 
	}

	function get_where_edit_guru($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_guru' => $id);
    	$query = $this->M_admin->get_where_data('spp_guru', $where);
		$data = array();
		foreach ($query->result() as $row)
		{
				$data[] = array(
					'id_guru' => $row->id_guru,
					'email_guru' => $row->email_guru,
					'nm_guru' => $row->nm_guru,
					'alamat' => $row->alamat,
					'no_hp' => $row->no_hp,
					'keterangan_guru' => $row->keterangan_guru
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function where_delete_guru($id="")
	{
		$where = array('id_guru' => $id);
		$data = array('status_del_guru' => 0);
		$this->M_admin->where_status_data($where, 'spp_guru', $data);
		echo "Non-aktif";
	}

	function where_restore_guru($id="")
	{
		$where = array('id_guru' => $id);
		$data = array('status_del_guru' => 1);
		$this->M_admin->where_status_data($where, 'spp_guru', $data);
		echo "pulihkan!";
	}   

	// End Guru ------------------------------------------------------------------------

	// Siswa ------------------------------------------------------------------------

	function siswa()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/master_data/vsiswa', $data);
		}else {
			redirect('login');
		}  
	}

	function pencarian_siswa($nama=null, $status=null)
	{
		header('Content-Type: application/json');
		$nama = array('nm_siswa' => $nama);
		$status = array('status_del_siswa' => $status);
		$query = $this->M_admin->pencarian_siswa($nama, $status); 
		foreach ($query->result() as $row) {
			$tgl_lahir =  Date('d-m-Y', strtotime($row->tgl_lahir));
			if ($row->no_hp_2 == NULL) {
				$no_hp_2 = "-";
			}else {
			 	$no_hp_2 = $row->no_hp_2;
			}
			$data[] = array(
				'no_induk'    => $row->no_induk,
				'nm_siswa'    => $row->nm_siswa,
				'kota_lahir'  => $row->kota_lahir,
				'tgl_lahir'   => $tgl_lahir,
				'alamat'      => $row->alamat,
				'nm_ibu'      => $row->nm_ibu,
				'no_hp_1'     => $row->no_hp_1,
				'no_hp_2'     => $no_hp_2,
				'status_del_siswa' => $row->status_del_siswa,
				'keterangan_siswa' => $row->keterangan_siswa
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output); 
	}

	// Insert siswa
	function insert_siswa()
	{
		$this->form_validation->set_rules('prodi', 'Jurusan', 'required');
		$this->form_validation->set_rules('no_induk', 'No. Induk', 'required');
		$this->form_validation->set_rules('nm_siswa', 'Nama Siswa', 'required');
		$this->form_validation->set_rules('kota_lahir', 'Kota Lahir', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tgl Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('nm_ibu', 'Nama Ibu', 'required');
		$this->form_validation->set_rules('no_hp_1', 'No. HP', 'required');


		$prodi = $this->input->post('prodi');
		$no_induk = $this->input->post('no_induk');
		$nm_guru = $this->input->post('nm_siswa');
		$kota_lahir = $this->input->post('kota_lahir');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$nm_ibu = $this->input->post('nm_ibu');
		$no_hp_1 = $this->input->post('no_hp_1');
		$no_hp_2 = $this->input->post('no_hp_2');
		$status = 1;
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'jurusan_siswa' => $prodi,
			'no_induk' => $no_induk,
			'nm_siswa' => ucwords($nm_guru),
			'kota_lahir' => ucwords($kota_lahir),
			'tgl_lahir' => $tgl_lahir,
			'alamat' => ucwords($alamat),
			'nm_ibu' => ucwords($nm_ibu),
			'no_hp_1' => $no_hp_1,
			'no_hp_2' => $no_hp_2,
			'status_del_siswa' => $status,
			'keterangan_siswa' => $keterangan,
		);
		
		if ($this->form_validation->run() == FALSE)
		{
			echo "Please fill in the form";
		}
		else
		{ 
			$this->M_admin->insert_data('spp_siswa', $data);
			echo "Insert Berhasil!";
		} 
	}

	//Update siswa
	function update_siswa()
	{
		$no_induk = $this->input->post('no_induk');
		$where = array('no_induk' => $no_induk);

		$nm_guru = $this->input->post('nm_siswa');
		$kota_lahir = $this->input->post('kota_lahir');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$nm_ibu = $this->input->post('nm_ibu');
		$no_hp_1 = $this->input->post('no_hp_1');
		$no_hp_2 = $this->input->post('no_hp_2');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'no_induk' 		=> $no_induk,
			'nm_siswa' 		=> ucwords($nm_guru),
			'kota_lahir' 	=> ucwords($kota_lahir),
			'tgl_lahir' 	=> $tgl_lahir,
			'alamat' 			=> ucwords($alamat),
			'nm_ibu' 			=> ucwords($nm_ibu),
			'no_hp_1' 		=> $no_hp_1,
			'no_hp_2' 		=> $no_hp_2,
			'keterangan_siswa' => $keterangan
		);

		$this->M_admin->update_data($where, 'spp_siswa', $data);
		echo "Update Berhasil!";
	}

	function get_where_edit_siswa($no_induk="")
	{
		header('Content-Type: application/json');
		$where = array('no_induk' => $no_induk);
    	$query = $this->M_admin->get_where_data('spp_siswa', $where);
		$data = array();
		foreach ($query->result() as $row)
		{
				if ($row->no_hp_2 == NULL) {
					$no_hp_2 = "";
				}else {
					$no_hp_2 = $row->no_hp_2;
				}
				$data[] = array(
		'no_induk'    => $row->no_induk,
		'nm_siswa'    => $row->nm_siswa,
		'kota_lahir'  => $row->kota_lahir,
		'tgl_lahir'   => $row->tgl_lahir,
		'alamat'      => $row->alamat,
		'nm_ibu'      => $row->nm_ibu,
		'no_hp_1'     => $row->no_hp_1,
				'no_hp_2'   	 => $no_hp_2,
				'keterangan_siswa' => $row->keterangan_siswa
		);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function where_delete_siswa($no_induk="")
	{
		$where = array('no_induk' => $no_induk);
		$data = array('status_del_siswa' => 0);
		$this->M_admin->where_status_data($where, 'spp_siswa', $data);
		echo "Non-aktif";
	}

	function where_restore_siswa($no_induk="")
	{
		$where = array('no_induk' => $no_induk);
		$data = array('status_del_siswa' => 1);
		$this->M_admin->where_status_data($where, 'spp_siswa', $data);
		echo "Pulihkan";
	}

	// End Siswa ------------------------------------------------------------------------

	// Petugas ------------------------------------------------------------------------

	function petugas()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/master_data/vpetugas', $data);
		}else {
			redirect('login');
		}  
	}

	function pencarian_petugas($cari="", $status="")
	{
		header('Content-Type: application/json'); 
		$cari = array('nm_petugas' => $cari);
		$status = array('status_del_petugas' => $status);
		$query = $this->M_admin->pencarian_petugas($cari, $status);
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_petugas' 			=> $row->id_petugas,
				'email_petugas' 		=> $row->email_petugas,
				'nm_petugas' 			=> $row->nm_petugas,
				'alamat_petugas' 		=> $row->alamat_petugas,
				'no_hp_petugas' 		=> $row->no_hp_petugas,
				'status_del_petugas'	=> $row->status_del_petugas,
				'keterangan_petugas'	=> $row->keterangan_petugas
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output); 
	}  

	function insert_petugas()
	{
		$query = $this->M_admin->get_id_petugas();
		$id = $query->row();
		$id = $id->id_petugas;

		$id_petugas = substr($id, 3, 4);
		if ($id_petugas == 0) {
			$id_petugas = 1;
		}else {
			$id_petugas++;
		}


		$this->form_validation->set_rules('email_petugas', 'Email Petugas', 'required');
		$this->form_validation->set_rules('nm_petugas', 'Nama Petugas', 'required');
		$this->form_validation->set_rules('alamat_petugas', 'Alamat', 'required');
		$this->form_validation->set_rules('no_hp_petugas', 'No.HP', 'required');

		$id_petugas = $this->kode->kode_ID("PE-", $id_petugas, $query);
		$email_petugas = $this->input->post('email_petugas');
		$nm_petugas = $this->input->post('nm_petugas');
		$alamat_petugas = $this->input->post('alamat_petugas');
		$no_hp_petugas = $this->input->post('no_hp_petugas');
		$status = 1;
		$keterangan_petugas = $this->input->post('keterangan');

		$data = array(
			'id_petugas' 	=> $id_petugas,
			'email_petugas' => $email_petugas,
			'nm_petugas' 	=> ucwords($nm_petugas),
			'alamat_petugas'=> ucwords($alamat_petugas),
			'no_hp_petugas' => $no_hp_petugas,
			'status_del_petugas' => $status,
			'keterangan_petugas' => $keterangan_petugas
		); 

		if ($this->form_validation->run() == FALSE)
		{
			echo "Please fill in the form";
		}
		else
		{ 
			$this->M_admin->insert_data('spp_petugas', $data);
			echo "Insert Berhasil!";
		} 
	}

	function update_petugas()
	{
		$id_petugas = $this->input->post('id_petugas');
		$where = array('id_petugas' => $id_petugas);

		$email_petugas = $this->input->post('email_petugas');
		$nm_petugas = $this->input->post('nm_petugas');
		$alamat_petugas = $this->input->post('alamat_petugas');
		$no_hp_petugas = $this->input->post('no_hp_petugas');
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'email_petugas' => $email_petugas,
			'nm_petugas' => ucwords($nm_petugas),
			'alamat_petugas' => ucwords($alamat_petugas),
			'no_hp_petugas' => $no_hp_petugas,
			'keterangan_petugas' => $keterangan
		);
		$this->M_admin->update_data($where, 'spp_petugas', $data);
		echo "Update Berhasil!";
	}

	function get_where_edit_petugas($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_petugas' => $id);
		$query = $this->M_admin->get_where_data('spp_petugas', $where);
			$data = array();
		foreach ($query->result() as $row)
		{
				$data[] = array(
					'id_petugas' => $row->id_petugas,
					'email_petugas' => $row->email_petugas,
					'nm_petugas' => $row->nm_petugas,
					'alamat_petugas' => $row->alamat_petugas,
					'no_hp_petugas' => $row->no_hp_petugas,
					'keterangan_petugas' => $row->keterangan_petugas
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function where_delete_petugas($id="")
	{
		$where = array('id_petugas' => $id);
		$data = array('status_del_petugas' => 0);
		$this->M_admin->where_status_data($where, 'spp_petugas', $data);
		echo "Non-aktif";
	}

	function where_restore_petugas($id="")
	{
		$where = array('id_petugas' => $id);
		$data = array('status_del_petugas' => 1);
		$this->M_admin->where_status_data($where, 'spp_petugas', $data);
		echo "Pulihkan!";
	}

	// End Petugas ------------------------------------------------------------------------ 

	// Tahun Ajaran ------------------------------------------------------------------------
 
    function tahun_ajaran()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/management/vta', $data);
		}else {
			redirect('login');
		}  
	}

	function pencarian_ta($cari="")
	{
		header('Content-Type: application/json');
		$query = $this->M_admin->pencarian_ta($cari);
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_ta'		=> $row->id_ta,
				'status_aktif_ta' 	=> $row->status_aktif_ta,
				'keterangan_ta' => $row->keterangan_ta
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	// Insert ta
	function insert_ta()
	{
		$this->form_validation->set_rules('id_ta', 'ID TA', 'required'); 

		$id_ta = $this->input->post('id_ta');
		$status = 0;
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'id_ta' => $id_ta,
			'status_aktif_ta' => $status,
			'keterangan_ta' => $keterangan,
		);

		if ($this->form_validation->run() == FALSE)
		{
			echo "Please fill in the form";
		}
		else
		{ 
			$this->M_admin->insert_data('spp_ta', $data);
			echo "Insert Berhasil";
		} 
	}

	function aktifkan_ta($id_ta)
	{   
		$aktif = array('status_aktif_ta'=>1);
		$checkAktif = $this->M_admin->get_where_data('spp_ta',$aktif)->first_row();
		if($checkAktif != null){
			// non aktifkan yang sudah aktif
			$nonAktifkan = array('id_ta' => $checkAktif->id_ta);
			$status = array('status_aktif_ta' => 0);
			$this->M_admin->update_data($nonAktifkan,'spp_ta',$status);

			// aktifkan yang mau diaktifkan
			$aktifkan = array('id_ta' => $id_ta);
			$status = array('status_aktif_ta' => 1);
			$this->M_admin->update_data($aktifkan,'spp_ta',$status);
		}else{
			// aktifkan yang mau diaktifkan
			$aktifkan = array('id_ta' => $id_ta);
			$status = array('status_aktif_ta' => 1);
			$this->M_admin->update_data($aktifkan,'spp_ta',$status);
		}

		header('Content-Type: application/json');
		$query = $this->M_admin->get_data('spp_ta','id_ta');
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_ta'		=> $row->id_ta,
				'status_aktif_ta' 	=> $row->status_aktif_ta,
				'keterangan_ta' => $row->keterangan_ta
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	// Update ta
	function update_ta()
	{
		$id_ta = $this->input->post('id_ta');
		$keterangan = $this->input->post('keterangan');
		$where = array('id_ta'=>$id_ta);
		$data = array(
			'keterangan_ta' => $keterangan
		);
		$this->M_admin->update_data($where, 'spp_ta', $data);
		echo "Update Berhasil";
	}

	function get_where_edit_ta($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_ta' => $id);
    	$query = $this->M_admin->get_where_data('spp_ta', $where);
		$data = array();
		foreach ($query->result() as $row)
		{
				$data[] = array(
					'id_ta' => $row->id_ta,
					'keterangan_ta' => $row->keterangan_ta,
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function where_delete_ta($id="")
	{
		$where = array('id_ta' => $id);
		$data = array('status_aktif_ta' => 0);
		$this->M_admin->where_delete_ta($where, $data);
		echo "Non_Aktif";
	}
	
    // End Tahun Ajaran ------------------------------------------------------------------------

	// Jurusan ------------------------------------------------------------------------
	
	function jurusan()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/management/vjurusan', $data);
		}else {
			redirect('login');
		}  
	}

	// Pencarian prodi
	function pencarian_prodi($cari="")
	{
		header('Content-Type: application/json');
		$query = $this->M_admin->pencarian_prodi($cari);
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_prodi'		=> $row->id_prodi,
				'nm_prodi' 		=> $row->nm_prodi,
				'status_aktif_prodi' => $row->status_aktif_prodi,
				'keterangan_prodi' => $row->keterangan_prodi
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	// Insert prodi
	function insert_prodi()
	{ 
		$this->form_validation->set_rules('id_prodi', 'ID PRODI', 'required');
		$this->form_validation->set_rules('nm_prodi', 'Nama Prodi', 'required');

		$id_prodi = $this->input->post('id_prodi');;
		$nm_prodi = $this->input->post('nm_prodi');
		$status = 1;
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'id_prodi' => strtoupper($id_prodi),
			'nm_prodi' => ucwords($nm_prodi),
			'status_aktif_prodi' => $status,
			'keterangan_prodi' => $keterangan,
		);

		if ($this->form_validation->run() == FALSE)
		{
			echo "Please fill in the form";
		}
		else
		{ 
			$this->M_admin->insert_data('spp_prodi', $data);
			echo "Insert Berhasil";
		}
	}

	// Update prodi
	function update_prodi()
	{
		$id_prodi = $this->input->post('id_prodi');
		$nm_prodi = $this->input->post('nm_prodi');
		$status = 1;
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'id_prodi' => $id_prodi,
			'nm_prodi' => ucwords($nm_prodi),
			'status_aktif_prodi' => $status,
			'keterangan_prodi' => $keterangan,
		);
		$where = array('id_prodi' => $id_prodi);
		// print_r($data);
		$this->M_admin->update_data($where, 'spp_prodi', $data);
		echo "Update Berhasil";
	}

	function get_where_edit_prodi($id="")
	{
		header('Content-Type: application/json');
		$where = array('id_prodi' => $id);
    	$query = $this->M_admin->get_where_data('spp_prodi', $where);
		$data = array();
		foreach ($query->result() as $row)
		{
				$data[] = array(
					'id_prodi'		=> $row->id_prodi,
					'nm_prodi' 		=> $row->nm_prodi,
					'keterangan_prodi' => $row->keterangan_prodi
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function where_delete_prodi($id="")
	{
		$where = array('id_prodi' => $id);
		$data = array('status_aktif_prodi' => 0);
		$this->M_admin->where_delete_data($where, 'spp_prodi', $data);
		echo "Non_Aktif";
	}

    // End Jurusan ------------------------------------------------------------------------

	// Rombel ------------------------------------------------------------------------
	
	function rombel()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/management/vrombel', $data);
		}else {
			redirect('login');
		}  
	}

	function pencarian_rombel($cari='', $ta='')
	{
		header('Content-Type: application/json');
		$ta = array('spp_rombel.id_ta' => $ta);  
		$query = $this->M_admin->pencarian_rombel($cari, $ta);
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_rombel'		=> $row->id_rombel,
				'nm_rombel' 	=> $row->nm_rombel,
				'tingkat'		=> $row->tingkat,
				'id_ta'			=> $row->id_ta,
				'nm_guru'		=> $row->nm_guru,
				'nm_prodi'		=> $row->nm_prodi,
				'nominal_spp' 	=> $row->nominal_spp,
				'keterangan_rombel' => $row->keterangan_rombel
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	// Insert rombel
	function insert_rombel()
	{
		$query = $this->M_admin->get_id_rombel();
		$id = $query->row();
		$id = $id->id_rombel;

		$id_rombel = substr($id, 3, 4);
		if ($id_rombel == 0) {
			$id_rombel = 1;
		}else {
			$id_rombel++;
		}


		$this->form_validation->set_rules('id_ta', 'ID TA', 'required');
		$this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
		$this->form_validation->set_rules('nm_rombel', 'Nama Rombel', 'required');
		$this->form_validation->set_rules('id_prodi', 'ID PRODI', 'required');
		$this->form_validation->set_rules('id_guru', 'ID GURU', 'required');
		$this->form_validation->set_rules('nominal_spp', 'Nominal Spp', 'required');

		$id_rombel = $this->kode->kode_ID("RO-", $id_rombel, $query);
		$id_ta = $this->input->post('id_ta');
		$tingkat = $this->input->post('tingkat');
		$nm_rombel = $this->input->post('nm_rombel');
		$id_prodi = $this->input->post('id_prodi');
		$id_guru = $this->input->post('id_guru');
		$nominal_spp = $this->input->post('nominal_spp');
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'id_rombel' => $id_rombel,
			'id_ta' => $id_ta,
			'tingkat' => $tingkat,
			'nm_rombel' => strtoupper($nm_rombel),
			'id_prodi' => $id_prodi,
			'id_guru' => $id_guru,
			'nominal_spp' => $nominal_spp,
			'keterangan_rombel' => $keterangan
		);

		if ($this->form_validation->run() == FALSE)
		{
			echo "Please fill in the form";
		}
		else
		{ 
			$this->M_admin->insert_data('spp_rombel', $data);
			echo "Insert Berhasil";
		}
	}

	function update_rombel()
	{
		$id_rombel = $this->input->post('id_rombel');
		$id_ta = $this->input->post('id_ta');
		$tingkat = $this->input->post('tingkat');
		$nm_rombel = $this->input->post('nm_rombel');
		$id_prodi = $this->input->post('id_prodi');
		$id_guru = $this->input->post('id_guru');
		$nominal_spp = $this->input->post('nominal_spp');
		$keterangan = $this->input->post('keterangan');
		$data = array(
			'id_ta' => $id_ta,
			'tingkat' => $tingkat,
			'nm_rombel' => strtoupper($nm_rombel),
			'id_prodi' => $id_prodi,
			'id_guru' => $id_guru,
			'nominal_spp' => $nominal_spp,
			'keterangan_rombel' => $keterangan
		);
		$where = array('id_rombel' => $id_rombel);
		$this->M_admin->update_data($where, 'spp_rombel', $data);
		echo "Update Berhasil";
	}

	function get_where_edit_rombel($id="")
	{ 
		header('Content-Type: application/json');
		$where = array('id_rombel' => $id);
    	$query = $this->M_admin->get_where_rombel($where);
		$data = array();
		foreach ($query->result() as $row)
		{
				$data[] = array(
					'id_rombel' => $row->id_rombel,
					'id_ta' => $row->id_ta,
					'status_aktif_ta' => $row->status_aktif_ta,
					'tingkat' => $row->tingkat,
					'nm_rombel' => $row->nm_rombel,
					'id_prodi' => $row->id_prodi,
					'id_guru' => $row->id_guru,
					'nominal_spp' => $row->nominal_spp,
					'keterangan_rombel' => $row->keterangan_rombel
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	function where_delete_rombel($id="")
	{
		$where = array('id_rombel' => $id);
		// $data = array('status_del_rombel' => 0);
		$this->M_admin->where_delete_data($where, 'spp_rombel', $data);
		echo "Non_Aktif";
	}

  	// Get guru
	function get_guru()
	{
		header('Content-Type: application/json');
    	$query = $this->M_admin->get_guru();
		foreach ($query->result() as $row)
		{
		$data[] = array(
			'id_guru' => $row->id_guru,
			'nm_guru' => $row->nm_guru,
			'status_del_guru' => $row->status_del_guru
		);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	// Get TA
	function get_ta()
	{
		header('Content-Type: application/json');
    	$query = $this->M_admin->get_ta();
		foreach ($query->result() as $row)
		{
		$data[] = array(
					'id_ta' => $row->id_ta,
					'status_aktif_ta' => $row->status_aktif_ta
				);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	// Get prodi
	function get_prodi()
	{
		header('Content-Type: application/json');
    	$query = $this->M_admin->get_prodi();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_prodi' => $row->id_prodi,
				'nm_prodi' => $row->nm_prodi,
				'status_aktif_prodi' => $row->status_aktif_prodi
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

    // End Rombel ------------------------------------------------------------------------

	// Anggota Rombel ------------------------------------------------------------------------
	
	function anggota_rombel()
	{
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/management/vanggota_rombel', $data);
		}else {
			redirect('login');
		}  
	}

	function pencarian_anggota_rombel($cari="", $ta="")
	{ 
		header('Content-Type: application/json');  
		$query = $this->M_admin->pencarian_anggota_rombel($cari, $ta);
		$data = array();
		foreach ($query->result() as $row)
		{  
			$data[] = array(
				'id_tr_rombel'	=> $row->id_tr_rombel,
				'no_induk'		=> $row->no_induk,
				'nm_siswa'		=> $row->nm_siswa,
				'tingkat'		=> $row->tingkat,
				'nm_rombel'		=> $row->nm_rombel
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	} 

	function get_where_jumlah_anggota_rombel($id_rombel='')
	{
		header('Content-Type: application/json');     
		$query  = $this->M_admin->get_where_jumlah_anggota_rombel();
		$data[] = array(); 
		foreach ($query->result() as $row) {   
			$data[] = array( 
				'id_tr_rombel'	=> $row->id_tr_rombel,
				'id_rombel'		=> $row->id_rombel,
				'id_prodi'		=> $row->id_prodi,
				'id_ta'			=> $row->id_ta,
				'tingkat'		=> $row->tingkat,
				'nm_rombel'		=> $row->nm_rombel,
				'Total'			=> $row->Total

			);    
		}     
		$output = array(
			'data' => $data
		); 
		echo json_encode($output);
		
	}

	function get_data_rombel()
	{
		header('Content-Type: application/json');
    	$query = $this->M_admin->get_data_rombel();
		foreach ($query->result() as $row)
		{   
			$data[] = array(
				'id_rombel' => $row->id_rombel,  
				'data_count' => $row,
				'tingkat' => $row->tingkat,
				'nm_rombel' => $row->nm_rombel
			);

		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	}

	// End Anggota Rombel ------------------------------------------------------------------------ 
 
	// Data Anggota Rombel ------------------------------------------------------------------------

	function data_anggota_rombel()
	{ 
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$data['id_rombel'] = $this->uri->segment(4);
			$data['id_prodi'] = $this->uri->segment(5);
			$this->template->admin('admin/management/vdata_anggota_rombel', $data);
		}else {
			redirect('login');
		} 
	}

	// Insert anggota rombel
	function insert_anggota_rombel()
	{ 
		$query = $this->M_admin->get_id_tr_rombel();
		$id = $query->row();
		$id = $id->id_tr_rombel;

		$id_tr_rombel = substr($id, 3, 4);
		if ($id_tr_rombel == 0) {
			$id_tr_rombel = 1;
		}else {
			$id_tr_rombel++;
		}

		$no_induk = $this->input->post('no_induk');
		$id_rombel = $this->input->post('id_rombel'); 

		$data = array();
		foreach($no_induk as $val_no_induk){
			$data[] = array(
				'id_tr_rombel' => $this->kode->kode_ID("TR-", $id_tr_rombel, $query),
				'id_rombel' => $id_rombel,
				'no_induk' => $val_no_induk
			);
			$id_tr_rombel++;
		}
		// print_r($data);
		$this->M_admin->insert_data_batch('spp_anggota_rombel', $data);
		echo "Insert Berhasil";
	}  

	// Get where rombel
	function get_where_rombel()
	{
		header('Content-Type: application/json');
		$where = array('status_aktif_ta' => 1);
		$query = $this->M_admin->get_where_rombel($where);
		foreach ($query->result() as $row)
		{
		$data[] = array(
				'id_rombel' => $row->id_rombel,
				'rombel' => $row->tingkat." - (".$row->nm_rombel.")",
				'id_prodi'	=> $row->id_prodi
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	} 

	// Get siswa
	function get_where_siswa($prodi)
	{
		header('Content-Type: application/json');
		$prodi = array('jurusan_siswa' => $prodi);
		$query = $this->M_admin->get_where_data_siswa($prodi,'nm_siswa');
		$data = array();
		foreach ($query->result() as $row)
		{
			$tgl_lahir =  Date('d-m-Y', strtotime($row->tgl_lahir));
			$data[] = array(
				'jurusan_siswa' => $row->jurusan_siswa,
				'no_induk' => $row->no_induk,
				'nm_siswa' => $row->nm_siswa,
				'ttl' => $row->kota_lahir.', '.$tgl_lahir,
				'status_del_siswa' => $row->status_del_siswa				
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}

	// Get siswa
	function ge_ta()
	{
		header('Content-Type: application/json');
    	$query = $this->M_admin->get_data('spp_ta', 'id_ta');
		foreach ($query->result() as $row)
		{ 
			$data[] = array(
				'id_ta' => $row->id_ta, 
			);
		}
		$output = array(
		'data' => $data
		);
		echo json_encode($output);
	}


	function show_data_rombel($id_rombel)
	{
		header('Content-Type: application/json');
		$id_rombel = array('spp_anggota_rombel.id_rombel' => $id_rombel);   
    	$query = $this->M_admin->where_show_data_rombel($id_rombel); 
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_tr_rombel'	=> $row->id_tr_rombel, 
				'no_induk'		=> $row->no_induk,
				'nm_siswa'		=> $row->nm_siswa,
				'tingkat'		=> $row->tingkat,
				'nm_rombel'		=> $row->nm_rombel, 
				'id_ta'	 		=> $row->id_ta,
				'status_del_siswa' => $row->status_del_siswa
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	} 

	function where_show_data_rombel($id_rombel='')
	{
		header('Content-Type: application/json'); 
		$id_rombel = array('spp_anggota_rombel.id_rombel' => $id_rombel);   
    	$query = $this->M_admin->where_show_data_rombel($id_rombel); 
		$data = array();
		foreach ($query->result() as $row)
		{
			$data[] = array(
				'id_tr_rombel'	=> $row->id_tr_rombel, 
				'no_induk'		=> $row->no_induk,
				'nm_siswa'		=> $row->nm_siswa,
				'tingkat'		=> $row->tingkat,
				'nm_rombel'		=> $row->nm_rombel, 
				'id_ta'	 		=> $row->id_ta,
				'status_del_siswa' => $row->status_del_siswa
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	} 

	function update_ta_anggota_rombel()
	{
		$id_rombel = $this->input->post('id_rombel');
		$id_ta = $this->input->post('id_ta'); 
		$data = array(
			'id_ta' => $id_ta
		);
		$where = array('id_rombel' => $id_rombel);
		$this->M_admin->update_data($where, 'spp_rombel', $data); 

		echo "Update Berhasil";
	}

	// End Data Anggota Rombel ------------------------------------------------------------------------
	 
	 
}
