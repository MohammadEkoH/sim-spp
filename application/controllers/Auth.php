<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('id_admin')) {
			redirect('ad/dashboard');
		}elseif ($this->session->userdata('id_guru')) {
			redirect('gu/dashboard');
		}elseif ($this->session->userdata('id_petugas')) {
			redirect('pe/dashboard');
		}else {
			$this->template->auth('auth/vlogin');
		}
	} 
	
	function login()
  	{
		$username = $this->input->post('id');
		$password = $this->input->post('password');
		$uname = substr($username, 0, 2);
    	$password = $this->input->post('password');
		$password = md5($password);

		if ($uname == "AD")
		{
			$where = array('id_admin' => $username, 'password' => $password);
			$this->session->set_userdata($where);
			redirect('auth/auth_admin');
		}
		else if ($uname == "GU")
		{
			$where = array('id_guru' => $username, 'password' => $password);
			$this->session->set_userdata($where);
			redirect('auth/auth_guru');
		}
		else if ($uname == "PE")
		{
			$where = array('id_petugas' => $username, 'password' => $password);
			$this->session->set_userdata($where);
			redirect('auth/auth_petugas');
		}
  }

	function auth_admin()
	{
		$username = $this->session->userdata('id_admin');
		$password = $this->session->userdata('password');
		$where = array('id_admin' => $username, 'password' =>$password, 'status_del_admin' => 1);
		$query = $this->M_auth->auth_admin($where);

		if ($query) {
			redirect('ad/dashboard');
		}else{
			$this->session->sess_destroy();
			redirect('login');
			echo "gagal";
		}
	}

	function auth_guru()
	{
		$username = $this->session->userdata('id_guru');
		$password = $this->session->userdata('password');
		$where = array('id_guru' => $username, 'password' =>$password, 'status_del_guru' => 1);
		$query = $this->M_auth->auth_guru($where);

		if ($query) {
			redirect('gu/dashboard');
		}else {
			$this->session->sess_destroy();
			redirect('login');
			echo "gagal";
		}
	}

	function auth_petugas()
	{
		$username = $this->session->userdata('id_petugas');
		$password = $this->session->userdata('password');
		$where = array('id_petugas' => $username, 'password' =>$password, 'status_del_petugas' => 1);
		$query = $this->M_auth->auth_petugas($where); 

		if ($query) {
			redirect('pe/dashboard');
		}else {
			$this->session->sess_destroy();
			redirect('login');
			echo "gagal";
		}
	}

	function logout()
		{
		$this->session->sess_destroy();
		redirect('login');
	}

	function forget_pass()
	{
		$this->template->auth('auth/vforget_password');
	}

	function send_email()
	{
		$this->form_validation->set_rules('id_guru', 'ID Guru', 'required');
		// $this->form_validation->set_rules('email', 'Email', 'required');

		$id = $this->input->post('id_guru');
		$where = substr($id, 0, 2);

		$newPassword 	= random_string('alnum', 8);
		$pass 		= md5($newPassword);
		
		// $to = $this->input->post('email');
		
		if ($this->form_validation->run() == FALSE)
		{
			redirect('auth/forget_pass');
		}
		else
		{ 
			$email = "exokirishima@gmail.com";
			$password = "927337exo";

			$ci = get_instance();
			$ci->load->library('email');
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "ssl://smtp.gmail.com";
			$config['smtp_port'] = "465";
			$config['smtp_user'] = $email;
			$config['smtp_pass'] = $password;
			$config['charset'] = "utf-8";
			$config['mailtype'] = "html";
			$config['newline'] = "\r\n";

			$ci->email->initialize($config); 

			if ($where == 'PE') {
				$id = array('id_petugas' => $id);
				$query = $this->M_auth->get_password('spp_petugas', $id); 
				foreach ($query->result() as $row) {
					$nama 	= $row->nm_petugas;
					$to 	= $row->email_petugas;
				}  

				$data = array('password' => $pass); 

				$this->M_admin->update_data($id, 'spp_petugas', $data);
			}else if($where == 'GU'){
				$id = array('id_guru' => $id);
				$query = $this->M_auth->get_password('spp_guru', $id);

				foreach ($query->result() as $row) {
					$nama 	= $row->nm_guru;
					$to 	= $row->email_guru;
				}  

				$data = array('password' => $pass); 
				$this->M_admin->update_data($id, 'spp_guru', $data);
			} 
 
			$ci->email->from('smkantartikasby@gmail.com', 'SMK ANTARTIKA SBY');
			$ci->email->to($to);
			
			$html = '<h5>Hi, '.$nama.'</h5> 
					Password anda disetel ulang<br>
					Password baru : <b>'.$newPassword.'</b>';

			$ci->email->subject("RESET PASSWORD");
			$ci->email->message($html);

			if ($this->email->send()) {
				echo 'Email sent.'; 
			} else {
				show_error($this->email->print_debugger());
			} 

			redirect('auth');
		}  
	}
}
