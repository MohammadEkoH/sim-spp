<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

    // Guru Import ------------------------------------------------------------------------

    function guru_import()
    {
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/master_data/vguru_import', $data);
		}else {
			redirect('login');
		} 
    }

    function fetch_guru()
	{
	  $data = $this->M_admin->get_data('spp_guru', 'id_guru');
	  $output = '
			<h4 align="right">Total Data : '.$data->num_rows().'</h4>
	    <table class="table table-striped table-bordered">
	      <tr>
					<th width="30px">No.</th>
			<th>ID</th>
			<th>Email</th>
	        <th>Nama</th>
	        <th>Alamat</th>
	        <th>No. Hp</th>
	        <th>Keterangan</th>
	      </tr>
	    ';
		$no=1;
	  foreach($data->result() as $row)
	  {
      $output .= '
      <tr>
		<td>'.$no++.'</td>
        <td>'.$row->id_guru.'</td>
		<td>'.$row->email_guru.'</td>
        <td>'.$row->nm_guru.'</td>
        <td>'.$row->alamat.'</td>
        <td>'.$row->no_hp.'</td>
        <td>'.$row->keterangan_guru.'</td>
      </tr>
     ';
	  }
	  $output .= '</table>';
	  echo $output;
	}

	 function import_guru()
	 {
	  if(isset($_FILES["file"]["name"]))
	  {
	   $path = $_FILES["file"]["tmp_name"];
	   $object = PHPExcel_IOFactory::load($path);
	   foreach($object->getWorksheetIterator() as $worksheet)
	   {
			$highestRow = $worksheet->getHighestRow();
			$highestColumn = $worksheet->getHighestColumn();
			for($row=2; $row<=$highestRow; $row++)
			{
				$id_guru 	= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
				$email_guru = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
				$nm_guru	= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
				$alamat		= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
				$no_hp 		= $worksheet->getCellByColumnAndRow(4, $row)->getValue();
				$keterangan = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
				$password 	= md5(12345);

				$data[] = array(
					'id_guru' 			=> $id_guru,
					'email_guru'   		=> $email_guru,
					'nm_guru'   		=> ucwords($nm_guru),
					'alamat'    		=> ucwords($alamat),
					'no_hp'  			=> $no_hp,
					'status_del_guru'	=> 1,
					'keterangan_guru' 	=> $keterangan,
					'password' 			=> $password
				);
			}
	   }
		// print_r($data);
	   $this->M_admin->insert_data_batch('spp_guru', $data);
	   echo 'Import Data Berhasil';
	  }
	 }

    // End Guru Import ------------------------------------------------------------------------

    // Siswa Import ------------------------------------------------------------------------

    function siswa_import()
    {
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/master_data/vsiswa_import', $data);
		}else {
			redirect('login');
		} 
    }

    function fetch_siswa()
	{
		$data = $this->M_admin->get_data('spp_siswa', 'no_induk');
		$output = '
				<h4 align="right">Total Data : '.$data->num_rows().'</h4>
                <table class="table table-striped table-bordered table-hover">
					<tr>
						<th width="30px">No</th>
						<th>Jurusan</th>
						<th>No. Induk</th>
						<th>Nama</th>
						<th>TTL</th>
						<th>Alamat</th>
						<th>Nama Ibu</th>
						<th>No. Hp 1</th>
						<th>No. Hp 2</th>
					</tr>
			';
		$no=1;
		foreach($data->result() as $row)
		{
			$tgl =  Date('d-m-Y', strtotime($row->tgl_lahir));
			if ($row->no_hp_2 == NULL) {
				$no_hp_2 = "-";
			}else {
				$no_hp_2 = $row->no_hp_2;
			}
			$output .= '
			<tr>
				<td>'.$no++.'</td>
				<td>'.$row->jurusan_siswa.'</td>
				<td>'.$row->no_induk.'</td>
				<td>'.$row->nm_siswa.'</td>
				<td>'.$row->kota_lahir.", ".$tgl.'</td>
				<td>'.$row->alamat.'</td>
				<td>'.$row->nm_ibu.'</td>
				<td>'.$row->no_hp_1.'</td>
				<td>'.$no_hp_2.'</td>
			</tr>
		 ';
		}
		$output .= '</table>';
		echo $output;
	}

	function import_siswa()
	{
	 if(isset($_FILES["file"]["name"]))
	 {
		$path = $_FILES["file"]["tmp_name"];
		$object = PHPExcel_IOFactory::load($path);
		foreach($object->getWorksheetIterator() as $worksheet)
		{
		 $highestRow = $worksheet->getHighestRow();
		 $highestColumn = $worksheet->getHighestColumn();
		 for($row=2; $row<=$highestRow; $row++)
		 {
			$jurusan_siswa = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
			$no_induk = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
			$nm_siswa = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
			$kota_lahir= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
			$tgl_lahir = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
			$tgl =  Date('Y-m-d', strtotime($tgl_lahir));
			$alamat = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
			$nm_ibu= $worksheet->getCellByColumnAndRow(6, $row)->getValue();
			$no_hp_1 = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
			$no_hp_2 = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
			$status_del_siswa = 1;
			$keterangan_siswa = $worksheet->getCellByColumnAndRow(9, $row)->getValue();

			$data[] = array(
			 'jurusan_siswa'    => $jurusan_siswa,
			 'no_induk'    => $no_induk,
			 'nm_siswa'    => ucwords($nm_siswa),
			 'kota_lahir'  => ucwords($kota_lahir),
			 'tgl_lahir'   => $tgl,
			 'alamat'      => ucwords($alamat),
			 'nm_ibu'      => ucwords($nm_ibu),
			 'no_hp_1'     => $no_hp_1,
			 'no_hp_2'     => $no_hp_2,
			 'status_del_siswa' => $status_del_siswa,
			 'keterangan_siswa' => $keterangan_siswa
			);
		 }
		}
		// print_r($data);
		$this->M_admin->insert_data_batch('spp_siswa', $data);
		echo 'Import Data Berhasil';
	 }
	}

    // Siswa Import ------------------------------------------------------------------------

    // Petugas Import ------------------------------------------------------------------------

    function petugas_import()
    {
		$id = $this->session->userdata('id_admin');
		if ($id) {
			$where = array('id_admin' => $id);
			$query = $this->M_admin->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_admin' => $row->nm_admin
					);
			}
			$this->template->admin('admin/master_data/vpetugas_import', $data);
		}else {
			redirect('login');
		} 
    } 

	function fetch_petugas()
    {
        $data = $this->M_admin->get_data('spp_petugas', 'id_petugas');
        $output = '
                <h4 align="right">Total Data : '.$data->num_rows().'</h4>
        <table class="table table-striped table-bordered">
            <tr>
                        <th width="30px">No.</th>
			<th>ID</th>
			<th>Email</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>No. Hp</th>
            <th>Keterangan</th>
            </tr>
        ';
            $no=1;
        foreach($data->result() as $row)
        {
        $output .= '
        <tr>
			<td>'.$no++.'</td>
			<td>'.$row->id_petugas.'</td>
			<td>'.$row->email_petugas.'</td>
            <td>'.$row->nm_petugas.'</td>
            <td>'.$row->alamat_petugas.'</td>
            <td>'.$row->no_hp_petugas.'</td>
            <td>'.$row->keterangan_petugas.'</td>
        </tr>
        ';
        }
        $output .= '</table>';
        echo $output;
    }

    function import_petugas()
    {
        if(isset($_FILES["file"]["name"]))
        {
        $path = $_FILES["file"]["tmp_name"];
        $object = PHPExcel_IOFactory::load($path);
        foreach($object->getWorksheetIterator() as $worksheet)
        {
			$highestRow = $worksheet->getHighestRow();
			$highestColumn = $worksheet->getHighestColumn();
			for($row=2; $row<=$highestRow; $row++)
			{
				$id_petugas = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
				$email_petugas = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
				$nm_petugas = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
				$alamat_petugas = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
				$no_hp_petugas = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
				$keterangan_petugas = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
				$password = md5(12345);

				$data[] = array(
					'id_petugas' 			=> $id_petugas,
					'email_petugas' 		=> $email_petugas,
					'nm_petugas'   			=> ucwords($nm_petugas),
					'alamat_petugas'    	=> ucwords($alamat_petugas),
					'no_hp_petugas'  		=> $no_hp_petugas,
					'status_del_petugas'	=> 1,
					'keterangan_petugas'  	=> $keterangan_petugas,
					'password' 				=> $password
				);
			}
		}
		// print_r($data);
        $this->M_admin->insert_data_batch('spp_petugas', $data);
        echo 'Import Data Berhasil';
        }
    }
    // End Petugas Import ------------------------------------------------------------------------

    // Download ------------------------------------------------------------------------

	function download($filename = NULL){
		$data = file_get_contents(base_url("assets/template_excel/".$filename));
		force_download($filename, $data);
	}
	
    // End RoDownloadmbel ------------------------------------------------------------------------
 
}
