<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {

	public function index()
	{
		$id = $this->session->userdata('id_guru'); 
		if ($id) {
			$where = array('spp_guru.id_guru' => $id);
			$query = $this->M_guru->get_where_user($where);
			$data = array();
			foreach ($query->result() as $row) {
				$data = array(
					'nm_guru' 	=> $row->nm_guru,
					'email'		=> $row->email_guru
				); 
			}    
			$this->template->guru('guru/vdashboard', $data);
		}else {
			redirect('login');
		}
	}
 
	function pencarian_siswa($nama="")
	{
		header('Content-Type: application/json'); 
		$nama = array('nm_siswa' => $nama);
		$id = $this->session->userdata('id_guru'); 
		$status = array(
			'status_del_siswa' => 1,
			'spp_rombel.id_guru' => $id
		);
		$query = $this->M_guru->pencarian_siswa($nama, $status); 
		$data = array();
		foreach ($query->result() as $row) {
			$tgl_lahir =  Date('d-m-Y', strtotime($row->tgl_lahir));
			if ($row->no_hp_2 == NULL) {
				$no_hp_2 = "-";
			}else {
			 	$no_hp_2 = $row->no_hp_2;
			}
			$data[] = array(
				'no_induk'    => $row->no_induk,
				'nm_siswa'    => $row->nm_siswa,
				'kota_lahir'  => $row->kota_lahir,
				'tgl_lahir'   => $tgl_lahir,
				'alamat'      => $row->alamat,
				'nm_ibu'      => $row->nm_ibu,
				'no_hp_1'     => $row->no_hp_1,
				'no_hp_2'     => $no_hp_2,
				'status_del_siswa' => $row->status_del_siswa,
				'keterangan_siswa' => $row->keterangan_siswa,
				'id_rombel'	  => $row->id_rombel
			);
		} 
		$output = array(
			'data' => $data
		);
		echo json_encode($output); 
	}

	function show_data_pembayaran($no_induk)
	{ 
		header('Content-Type: application/json'); 
    	$query = $this->M_guru->get_data_pembayaran($no_induk); 
		$data = array();
		foreach ($query->result() as $row)
		{  
			$Total = "Rp.".number_format($row->Total,0,',','.').",-";
			$data[] = array( 
				'no_induk'	=> $row->no_induk,
				'nm_siswa' 	=> $row->nm_siswa,
				'Januari'	=> $row->Januari,
				'Februari'	=> $row->Februari,
				'Maret'		=> $row->Maret,
				'April'		=> $row->April,
				'Mei'		=> $row->Mei,
				'Juni'		=> $row->Juni,
				'Juli'		=> $row->Juli,
				'Agustus'	=> $row->Agustus,
				'September'	=> $row->September,
				'Oktober'	=> $row->Oktober,
				'November'	=> $row->November,
				'Desember'	=> $row->Desember,
				'Total'		=> $Total
			);
		}
		$output = array(
			'data' => $data
		);
		echo json_encode($output);
	} 

	function akun()
	{
		$id = $this->session->userdata('id_guru');
		if ($id) {
			$where = array('id_guru' => $id);
			$query = $this->M_guru->get_where_user($where);
			foreach ($query->result() as $row) {
					$data = array(
						'nm_guru' => $row->nm_guru,
						'email'		=> $row->email_guru
					); 
			} 
			$data['callout'] = "";
			$this->template->guru('guru/vakun', $data);
		}else {
			redirect('login');
		}
	}

	function akun_user()
	{
		$id = $this->session->userdata('id_guru');  
		$where = array('id_guru' => $id);
		$query = $this->M_guru->get_data_akun($where);
		foreach ($query->result() as $row) {
			$data = array(
				'email_guru'	=> $row->email_guru,
				'nm_guru' 	=> $row->nm_guru,
				'alamat_guru'=> $row->alamat,
				'no_hp_guru' => $row->no_hp
			); 
		}  
		echo json_encode($data); 
	}

	function update_user()
	{ 
		$id = $this->session->userdata('id_guru'); 
		$where = array('id_guru' => $id); 
		
		$email = $this->input->post('email_guru');
		$nama = $this->input->post('nm_guru');
		$alamat = $this->input->post('alamat_guru');
		$no_hp = $this->input->post('no_hp_guru');
 
		$data = array(
			'email_guru' => $email,
			'nm_guru'	=> $nama,
			'alamat'=> $alamat,
			'no_hp'	=> $no_hp
		);
		$this->M_admin->update_data($where, 'spp_guru', $data);
		echo "Update Berhasil";
	}

	function cek_pass(){ 
		$id = $this->session->userdata('id_guru');    

		$lama		= md5($this->input->post('password_lama'));
		$baru		= $this->input->post('password_baru');
		$confirm	= $this->input->post('password_confirm');

		$where = array(
			'id_guru'	=> $id,
			'password' 		=> $lama
		);

		$cek = $this->M_guru->get_data_akun($where)->row(); 

		$pass = $cek->password; 

		if($pass == $lama &&  $baru == $confirm){
			$where_id = array( 'id_guru' => $id );
			$baru = md5($baru);  
			$new = array('password' => $baru);
			
			$this->M_admin->update_data($where_id, 'spp_guru', $new);

			$data = 'berhasil';
		}else{
			$data = 'gagal';
		}	

		$data = array('data' => $data);

		echo json_encode($data); 
	}
}
