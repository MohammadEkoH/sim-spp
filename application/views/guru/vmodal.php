<div class="modal fade" id="detail-siswa-pembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:80%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Detail Pembayaran SPP</h4>
      </div>
      <div class="modal-body">
      <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="60px">No.</th> 
                    <th>No. Induk</th>
                    <th>Nama</th> 
                    <th>jAN</th>
                    <th>FEB</th>
                    <th>MAR</th>
                    <th>APR</th>
                    <th>MEI</th>
                    <th>JUN</th>
                    <th>JUL</th>
                    <th>AGU</th>
                    <th>SEP</th> 
                    <th>OKT</th> 
                    <th>NOV</th>
                    <th>DES</th>  
                    <th>Total</th>  
                </tr>
                </thead>
                <tbody id="show-data-pembayaran">

                </tbody>
            </table>
            </div>
            </div>
        </div>
      </div>    
    </div>
  </div>
</div> 