<div class="container"> 
    <section class="content-header">
    <h1>
        Data Siswa
        <small id="jurusan"> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> 
    </ol>
    </section>
 
    <section class="content"> 
    <div class="box box-primary"> 
        <div class="box-body">
        <div class="table-responsive"> 
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="30px">No.</th>
                    <th>No. Induk</th>
                    <th>Nama</th>
                    <th>TTL</th>
                    <th>Alamat</th>
                    <th>Nama Ibu Kandung</th>
                    <th>No. Hp-1</th>
                    <th>No. Hp-2</th>
                    <th>Action</th>  
                </tr>
                </thead>
                <tbody id="data-siswa">

                </tbody>
            </table>
        </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </section>
    <!-- /.content -->
</div>

