<div class="container"> 
<section class="content-header">
    <h1>
    Dashboard
    <small> Akun</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Akun</a></li> 
    </ol> 

    <div class="callout callout-success"  style="display: none">
        <h4>Password berhasil diperbarui</h4> 
    </div>
    <div class="callout callout-danger" style="display: none">
        <h4>Password gagal diperbarui</h4> 
    </div> 
</section> 
<section class="content">
    <div class="row">
        <div class="col-md-5">
            <div class="box box-success" > 
            <div class="box-header with-border">
                <h3 class="box-title"> Change Profile</h3>
            </div>
            <div class="box-body">   
                <form class="form-user" method="post">
                    <div class="row form-group col-md-12">  
                        <div class="col-md-12">
                            Email
                            <input type="email" name="email_guru" class="form-control email-akun-guru" required>
                        </div>
                    </div>
                    <div class="row form-group col-md-12">  
                        <div class="col-md-12">
                            Nama
                            <input type="text" name="nm_guru" class="form-control nm-akun-guru" required>
                        </div>
                    </div>
                    <div class="row form-group col-md-12">  
                        <div class="col-md-12">
                            Alamat
                            <input type="text" name="alamat_guru" class="form-control alamat-akun-guru" required>
                        </div>
                    </div>
                    <div class="row form-group col-md-12">  
                        <div class="col-md-12">
                            No. HP
                            <input type="text" name="no_hp_guru" class="form-control no_hp-akun-guru" required>
                        </div>
                    </div> 
                    <div class="row form-group col-md-12">  
                        <div class="col-md-12" style="text-align: right">
                            <button type="submit" class="btn btn-success">Save Changed</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>    
        </div>
        <div class="col-md-5">
        <div class="box box-success" > 
        <div class="box-header with-border">
            <h3 class="box-title"> Change Password</h3>
        </div>
        <div class="box-body">    
            <form class="form-pass" method="post">
                <div class="row form-group col-md-12">  <br>
                    <div class="col-md-12"> 
                        <input type="password" id="lama" name="password_lama" placeholder="Password Lama" class="form-control" required>
                    </div>
                </div>
                <div class="row form-group col-md-12">  <br> <br>
                    <div class="col-md-12">
                        <input type="password" id="baru" name="password_baru" placeholder="Password Baru" class="form-control" required>
                    </div>
                </div> 
                <div class="row form-group col-md-12"> 
                    <div class="col-md-12">
                        <input type="password" id="confirm" name="password_confirm" placeholder="Ulangi Password Baru" class="form-control" required>
                    </div>
                </div>
                <div class="row form-group col-md-12">   
                    <div class="col-md-12" style="text-align: right">
                        <button type="submit" id="submit" class="btn btn-success">Save Changed</button>
                    </div>
                </div>
            </form>
        </div>
    </div>   
        </div>
    </div>
</section>
</div> 

  