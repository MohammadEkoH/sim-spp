<section class="content-header">
    <h1>
    Manajement
    <small>Data</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Dashboard</a></li>
    <li class="active">Data</li>    
    </ol>
</section>

<section class="content">
    <div class="box box-primary"> 
        <div class="box-body">  
            <div class="col-md-2">
                <select class="form-control show_ta" id="id-ta">  

                </select>
            </div> 
            <div class="col-md-2">
                <select class="form-control show_id_rombel" id="id-rombel">  
                    <option>-Pilih-</option>
                </select>
            </div>
            <div class="col-md-1" align="right"> 
                <button class="btn btn-default btn-tampilkan"> Tampilkan </button>
            </div>
        </div> 
    </div>  
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success show-data-box" style="display:none"> 
            <!-- /.box-header -->
            <input type="hidden" id="id-prodi" value="<?=$jurusan?>"> 
            <div class="box-body pad">
                
                <div class="col-lg-12">
                    <div class="table-responsive"> 
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="60px">No.</th> 
                                <th>No. Induk</th>
                                <th>Nama</th> 
                                <th>jAN</th>
                                <th>FEB</th>
                                <th>MAR</th>
                                <th>APR</th>
                                <th>MEI</th>
                                <th>JUN</th>
                                <th>JUL</th>
                                <th>AGU</th>
                                <th>SEP</th> 
                                <th>OKT</th> 
                                <th>NOV</th>
                                <th>DES</th>  
                                <th>Total</th>  
                            </tr>
                            </thead>
                            <tbody id="show-data">

                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>