<section class="content-header">
    <h1>
    Manajement
    <small>Anggota Rombel</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Dashboard</a></li>
    <li class="active">Anggota Rombel</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body pad"> 
                <div class="col-md-3"> 
                    <div class="form-group input-group">
                    <input type="text" class="form-control" id="id-search-anggota-rombel" name="" value="" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" id="btn-search-anggota-rombel"> <i class="fa fa-search"></i> </button>
                    </span>
                    </div>
                </div>  
                <!-- <div class="col-md-9" align="right"> 
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#insert-data-anggota-rombel"> <i class="fa fa-plus"></i>Insert </button>
                </div> -->
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr align="center">
                                    <th width="30px">No.</th> 
                                    <th width="300px" >Kelas</th>
                                    <!-- <th>Anggota Rombel</th>  -->
                                    <th width="400px" id="petugas-th">
                                        <center>
                                            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#update-kenaikan"><i class="fa fa-level-up"></i> kenaikan</button>
                                            <!-- <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#update-kelulusan"><i class="fa fa-graduation-cap"></i> kelulusan</button> -->
                                        </center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="data-anggota-rombel">
                                
                            </tbody> 
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>