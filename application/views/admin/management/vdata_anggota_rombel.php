<section class="content-header">
    <h1>
    Manajement
    <small>Anggota Rombel</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Manajement</a></li>
    <li class="active">Anggota Rombel</li>
    </ol>
</section>


<section class="content">
    <div class="row"> 
        <div class="col-md-5">
          <div class="box box-default">
            <div class="box-header with-border">
              <i class="fa fa-users"></i>

              <h3 class="box-title">Anggota Rombel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive scroll_rombel">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr align="center">
                                <th width="30px">No.</th> 
                                <th width="200px" >No. Induk</th>
                                <th>Nama Siswa</th>  
                            </tr>
                        </thead>
                        <tbody id="where-data-anggota-rombel">
                            
                        </tbody> 
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <form class="form-insert-anggota-rombel" method="post"> 
            <div class="col-md-1">
                <br><br><br><br><br>
                <br><br><br><br><br>
                <button type="submit" class="btn btn-primary btn-sm" style="float:right"> <i class="fa fa-arrow-left"> Insert</i> </button>
            </div>
            <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                <i class="fa fa-user"></i>

                <h3 class="box-title">Siswa</h3>
                </div>
                <!-- /.box-header --> 
                    <input type="hidden" id="get-id_rombel" name="id_rombel" value="<?=$id_rombel?>">
                    <input type="hidden" id="get-id-prodi" value="<?=$id_prodi?>">
                    <div class="box-body">
                        <div class="table-responsive scroll_rombel">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="60px">#</th> 
                                        <th>No. Induk</th>
                                        <th>Nama</th>
                                        <th>TTL</th> 
                                    </tr>
                                </thead>
                                <tbody id="where-data-siswa">

                                </tbody>
                            </table> 
                        </div>
                    </div> 
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </div>
</section>