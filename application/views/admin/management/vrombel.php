<section class="content-header">
    <h1>
    Manajement
    <small>Rombel</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Dashboard</a></li>
    <li class="active">Rombel</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body pad"> 
                <div class="col-md-2">
                    <select class="form-control show_ta_rombel" id="rombel-ta"> 
                    
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group input-group">
                    <input type="text" class="form-control" id="id-search-rombel" name="" value="" placeholder="Search Wali/Jurusan...">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" id="btn-search-rombel"> <i class="fa fa-search"></i> </button>
                    </span>
                    </div>
                </div> 
                <div class="col-md-7" align="right"> 
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#insert-rombel"> <i class="fa fa-plus"></i>Insert </button>
                </div>
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                <th width="30px">No.</th> 
                                <th>Tahun Ajaran</th>
                                <th>Kelas</th> 
                                <th>Jurusan</th>
                                <th>Wali Kelas</th>
                                <th>Nominal SPP</th>
                                <th>Keterangan</th>
                                <th width="100px" style="display:none" id="rombel-th"></th>
                                </tr>
                            </thead>
                            <tbody id="data-rombel">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>