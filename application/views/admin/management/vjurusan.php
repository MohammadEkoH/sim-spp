<section class="content-header">
    <h1>
    Manajement
    <small>Jurusan</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Dashboard</a></li>
    <li class="active">Jurusan</li>
    </ol>
</section>

<section class="content"> 
    <div class="box box-primary"> 
    <div class="box-body"> 
        <!-- /.panel-heading --> 
        <div class="col-md-3">
            <div class="form-group input-group">
            <input type="text" class="form-control" id="id-search-prodi" placeholder="Search...">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" id="btn-search-prodi"> <i class="fa fa-search"></i> </button>
            </span>
            </div>
        </div>
        <div class="col-md-9" align="right">
            <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#insert-prodi"> <i class="fa fa-plus"></i>Insert </button>
        </div>
        <br>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                        <th width="30px">No.</th>
                        <th>ID</th>
                        <th>prodi</th>
                        <th>Keterangan</th> 
                        <th width="100px" style="display:none" id="prodi-th"></th>
                        </tr>
                    </thead>
                    <tbody id="data-prodi">

                    </tbody>
                </table>
            </div>
        </div> 
        <!-- /.panel-body -->
    </div>  
</section>