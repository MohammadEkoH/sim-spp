<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/pace/pace.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>  
      .scroll-full{
        width: 100%;   
        overflow: scroll;
        height: 500px;
        
        /*script tambahan khusus untuk IE */
        scrollbar-face-color: #CE7E00; 
        scrollbar-shadow-color: #FFFFFF; 
        scrollbar-highlight-color: #6F4709; 
        scrollbar-3dlight-color: #11111; 
        scrollbar-darkshadow-color: #6F4709; 
        scrollbar-track-color: #FFE8C1; 
        scrollbar-arrow-color: #6F4709;
      } 
      .scroll_rombel{
        width: 100%;  
        overflow: scroll;
        height: 385px;
        background: #dfe6e9;
        
        /*script tambahan khusus untuk IE */
        scrollbar-face-color: #CE7E00; 
        scrollbar-shadow-color: #FFFFFF; 
        scrollbar-highlight-color: #6F4709; 
        scrollbar-3dlight-color: #11111; 
        scrollbar-darkshadow-color: #6F4709; 
        scrollbar-track-color: #FFE8C1; 
        scrollbar-arrow-color: #6F4709;
      } 
      .scroll{
        width: 100%;  
        overflow: scroll;
        height: 300px;
        
        /*script tambahan khusus untuk IE */
        scrollbar-face-color: #CE7E00; 
        scrollbar-shadow-color: #FFFFFF; 
        scrollbar-highlight-color: #6F4709; 
        scrollbar-3dlight-color: #11111; 
        scrollbar-darkshadow-color: #6F4709; 
        scrollbar-track-color: #FFE8C1; 
        scrollbar-arrow-color: #6F4709;
      } 
    </style>
</head>
<body class="fixed hold-transition skin-blue sidebar-mini">
<div class=" wrapper"> 
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>ad/dashboard/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav"> 
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url()?>assets/dist/img/user.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url()?>assets/dist/img/user.png" class="img-circle" alt="User Image">

                <p>
                  <?=$nm_admin?>
                  <small>administrator</small>
                </p>
              </li> 
              <!-- Menu Footer-->
              <li class="user-footer"> 
                <div class="pull-right">
                  <a href="<?=base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li> 
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>assets/dist/img/user.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$nm_admin?></p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> 
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="<?=base_url()?>ad/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container"> 
            </span>
          </a> 
        </li> 
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()?>ad/dashboard/guru"><i class="fa fa-circle-o"></i> Guru</a></li>
            <li><a href="<?=base_url()?>ad/dashboard/siswa"><i class="fa fa-circle-o"></i> Siswa</a></li>
            <li><a href="<?=base_url()?>ad/dashboard/petugas"><i class="fa fa-circle-o"></i> Petugas</a></li> 
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i> <span>Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url()?>ad/dashboard/tahun-ajaran"><i class="fa fa-circle-o"></i> Tahun Ajaran</a></li>
            <li><a href="<?=base_url()?>ad/dashboard/jurusan"><i class="fa fa-circle-o"></i> Jurusan</a></li>
            <li><a href="<?=base_url()?>ad/dashboard/room-belajar"><i class="fa fa-circle-o"></i> Rombel</a></li>
            <li><a href="<?=base_url()?>ad/dashboard/anggota-room-belajar"><i class="fa fa-circle-o"></i> Kenaikan/Kelulusan</a></li>
          </ul>
        </li>    
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper"> 
    <?=$content?>  
    <?=$modal?>  
  </div>
  
  
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="<?=base_url()?>ad/dashboard">SMK ANTARTIKA SBY</a>.</strong> 
  </footer>
 
</div>
<!-- ./wrapper -->
<script type="text/javascript">
  // To make Pace works on Ajax calls
  $(document).ajaxStart(function () {
    Pace.restart()
  })
  $('.ajax').click(function () {
    $.ajax({
      url: '#', success: function (result) {
        $('.ajax-content').html('<hr>Ajax Request Completed !')
      }
    })
  })
</script>
<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?=base_url()?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?=base_url()?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=base_url()?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url()?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url()?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/dist/js/demo.js"></script>
<!-- PACE -->
<script src="<?=base_url()?>assets/bower_components/PACE/pace.min.js"></script>
<!-- My custom js -->
<script>let base_url = '<?=base_url()?>'; </script>
<script src="<?=base_url()?>assets/MyCustomJS/master_data/siswa.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/master_data/guru.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/master_data/petugas.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/master_data/import.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/manage/ta.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/manage/jurusan.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/manage/rombel.js"></script>
<script src="<?=base_url()?>assets/MyCustomJS/manage/anggota_rombel.js"></script> 
<script src="<?=base_url()?>assets/MyCustomJS/dashboard.js"></script> 
<script src="<?=base_url()?>assets/MyCustomJS/global_get_data.js"></script> 
</body>
</html>
