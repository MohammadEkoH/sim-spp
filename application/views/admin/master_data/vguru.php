<section class="content-header">
    <h1>
    Manajement
    <small>Guru</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Dashboard</a></li>
    <li class="active">Guru</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body pad">
                <div class="col-md-2">
                    <select class="form-control show_petugas" id="guru-aktif"> 
                    <option value="1">Aktif</option>
                    <option value="0">Non-Aktif</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group input-group">
                    <input type="text" class="form-control" id="nama-guru" name="" value="" placeholder="Search...">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" id="btn-search-guru"> <i class="fa fa-search"></i> </button>
                    </span>
                    </div>
                </div>
                <div class="col-md-7" align="right">
                    <a class="btn btn-default btn-sm" href="<?=base_url()?>ad/dashboard/guru/import" class=""> <i class="fa fa-upload"></i>import </a> &nbsp;&nbsp;
                    <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#insert-guru"> <i class="fa fa-plus"></i>Insert </button>
                </div>
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="60px">No.</th> 
                                <th>Email</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>No. Hp</th>
                                <th>Keterangan</th>
                                <th width="100px" style="display:none" id="guru-th"></th>
                            </tr>
                            </thead>
                            <tbody id="data-guru">

                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>