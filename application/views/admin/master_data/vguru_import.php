<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
            File to import:
            </div>
        </div>
        <div class="col-lg-12">
        Nama file harus diakhir dengan .[format] Contoh: <b>.xlsx.xls.csv</b> <br>
        <a href="<?=base_url('import/download/Template_Guru.xlsx')?>" > <i class="fa fa-download"></i>Download Template</a>
        <form method="post" id="import_form_guru" enctype="multipart/form-data">
            <input type="file" name="file" id="file_guru"  required accept=".xls, .xlsx"><br>
            <button type="submit" class="btn btn-primary btn-sm">kirim</button>
        </form>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
        <div class="table-responsive" id="customer_data_guru">

        </div>
        </div>
    </div>

</section>