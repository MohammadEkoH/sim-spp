<!-- add siswa -->
<div class="modal fade" id="insert-siswa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:40%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Siswa</h4>
      </div>
      <div class="modal-body"> 
        <form class="form-insert-siswa" method="post">  
            <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Jurusab</label>
              </div>
              <div class="col-12 col-md-9">
                  <select name="prodi" id="get-prodi-siswa" class="form-control"></select>
              </div>
          </div>  
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Induk</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="no_induk-siswa-edit" name="no_induk" placeholder="Input No. Induk" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="nama-siswa-edit" name="nm_siswa" placeholder="Input Nama" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">TTL</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="kota_lahir" placeholder="Input Kota Lahir" class="form-control" required>
                  <input type="date" name="tgl_lahir" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class="form-control-label">Alamat</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="text-input" name="alamat" placeholder="Input Alamat" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama Ibu</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="text-input" name="nm_ibu" placeholder="Input Nama Ibu" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp-1</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="text-input" name="no_hp_1" placeholder="Input No. Hp-1 " class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp-2</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="text-input" name="no_hp_2" placeholder="Input No. Hp-2 " class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
        </div>
        <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-insert-siswa" style="float:right">Insert</button>
              </div>
          </div>
        </form> 
    </div>
  </div>
</div>
<!-- end add siswa -->

<!-- edit siswa -->
<div class="modal fade" id="edit-siswa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog"  style="width:40%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Siswa</h4>
      </div>
      <div class="modal-body">
        <form class="form-update-siswa" method="post">
          <input type="hidden" id="no-induk-siswa-edit" name="no_induk" value="">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="nm-siswa-edit" name="nm_siswa" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">TTL</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="kota-lahir-siswa-edit" name="kota_lahir" class="form-control">
                  <input type="date" id="tgl-lahir-siswa-edit" name="tgl_lahir" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class="form-control-label">Alamat</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="alamat-siswa-edit" name="alamat" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama Ibu</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="nm-ibu-siswa-edit" name="nm_ibu" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp-1</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="no-hp-1-siswa-edit" name="no_hp_1" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp-2</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="no-hp-2-siswa-edit" name="no_hp_2" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" placeholder="Input keterangan" class="form-control keterangan-siswa-edit"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button class="btn btn-success btn-update-siswa" style="float:right"> <i class="fa fa-save"></i> Save Changes</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end edit siswa -->

<!-- add guru -->
<div class="modal fade" id="insert-guru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:40%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Guru</h4>
      </div>
      <div class="modal-body">
        <form class="form-insert-guru" method="post">
        <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Email</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="email-guru-insert" name="email_guru" placeholder="Input Email" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="nm-guru-insert" name="nama_guru" placeholder="Input Nama" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Alamat</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="alamat-guru-insert" name="alamat" placeholder="Input Alamat" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="no_hp-guru-insert" name="no_hp" placeholder="Input No. Hp" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
        <div class="modal-footer">
            <div class="col-12 col-md-12">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button class="btn btn-primary btn-insert-guru" style="float:right">Insert</button>
            </div>
        </div>
        </form>
    </div>
  </div>
</div>
<!-- end add guru -->

<!-- edit guru -->
<div class="modal fade" id="edit-guru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:40%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Guru</h4>
      </div>
      <div class="modal-body">
        <form class="form-update-guru" method="post">
          <input type="hidden" id="id-guru-edit" name="id_guru">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Email</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="email-guru-edit" name="email_guru" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="nm-guru-edit" name="nm_guru" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Alamat</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="alamat-guru-edit" name="alamat" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="no_hp-guru-edit" name="no_hp" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control keterangan-guru-edit" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
      <div class="modal-footer">
        <div class="col-12 col-md-12">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button class="btn btn-success btn-update-guru" style="float:right"> <i class="fa fa-save"></i> Save Changes</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
<!-- end edit guru -->

<!-- add petugas -->
<div class="modal fade" id="insert-petugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:40%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Petugas</h4>
      </div>
      <div class="modal-body">
        <form class="form-insert-petugas" method="post">
        <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Email</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="email_petugas" placeholder="Input Email" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="nm_petugas" placeholder="Input Nama" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Alamat</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="alamat_petugas" placeholder="Input Alamat" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="no_hp_petugas" placeholder="Input No. Hp" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-insert-petugas" style="float:right">Insert</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end add petugas -->

<!-- edit petugas -->
<div class="modal fade" id="edit-petugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:40%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Petugas</h4>
      </div>
      <div class="modal-body">
        <form class="form-update-petugas" method="post">
          <input type="hidden" id="id-petugas-edit" name="id_petugas">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Email</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="email-petugas-edit" name="email_petugas"class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="nm-petugas-edit" name="nm_petugas"class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class="form-control-label">Alamat</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="alamat-petugas-edit" name="alamat_petugas" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">No. Hp</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" id="no_hp-petugas-edit" name="no_hp_petugas" class="form-control">
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control keterangan-petugas-edit" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>  
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-update-petugas" style="float:right"> <i class="fa fa-save"></i> Save Changes</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end edit petugas -->

<!-- add tahun ajaran -->
<div class="modal fade" id="insert-ta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog"  style="width:30%" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Petugas</h4>
      </div>
      <div class="modal-body">
        <form class="form-insert-ta" method="post">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Tahun Ajaran</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="id_ta" placeholder="Contoh : 2019-2020" class="form-control"required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-insert-ta" style="float:right">Insert</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end add tahun ajaran -->

<!-- edit tahun ajaran -->
<div class="modal fade" id="edit-ta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Petugas</h4>
      </div>
      <div class="modal-body">
        <form class="form-update-ta" method="post">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Tahun Ajaran</label>
              </div>
              <div class="col-12 col-md-9">
                  <input id="id-ta-edit" name="id_ta" style="color:red" class="form-control" required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class="form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea id="keterangan-ta-edit" name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
      <div class="modal-footer">
        <div class="col-12 col-md-12">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button class="btn btn-success btn-update-ta" style="float:right"><i class="fa fa-save"></i> Save Changes</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
<!-- end edit tahun ajaran -->

<!-- add prodi -->
<div class="modal fade" id="insert-prodi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data prodi</h4>
      </div>
      <div class="modal-body">
        <form class="form-insert-prodi" method="post">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">ID</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="id_prodi" placeholder="Input ID prodi" class="form-control"required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama prodi</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="nm_prodi" placeholder="Input Nama prodi" class="form-control"required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-insert-prodi" style="float:right">Insert</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end add prodi -->

<!-- edit prodi -->
<div class="modal fade" id="edit-prodi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data prodi</h4>
      </div>
      <div class="modal-body">
        <form class="form-update-prodi" method="post">
          <input type="hidden" name="id_prodi" id="id-prodi-edit" class="form-control"required>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama prodi</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="nm_prodi" id="nm_prodi-prodi-edit" placeholder="Input Nama prodi" class="form-control"required>
                  <!-- <small class="form-text text-muted" style="color:red">*Wajib</small> -->
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control keterangan-edit-prodi" placeholder="Input Keterangan"></textarea>
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-update-prodi" style="float:right"><i class="fa fa-save"></i> Save Changes</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end edit prodi -->
<!-- add rombel -->
<div class="modal fade" id="insert-rombel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Rombel</h4>
      </div>
      <div class="modal-body">
        <form class="form-insert-rombel" method="post">
          <input type="hidden" name="id_rombel" placeholder="Input ID prodi" class="form-control">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Tahun Ajaran</label>
              </div>
              <div class="col-12 col-md-9">
                  <select class="form-control" id="insert-ta-rombel" name="id_ta" required>

                  </select>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Tingkat/Jenjang</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="number" name="tingkat" placeholder="Input Tingat/Jenjang" class="form-control"required>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama Rombel</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="nm_rombel" placeholder="Input Nama Rombel" class="form-control"required>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">prodi</label>
              </div>
              <div class="col-12 col-md-9">
                  <select class="form-control" id="get-prodi" name="id_prodi" required>

                  </select>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Guru</label>
              </div>
              <div class="col-12 col-md-9">
                  <select class="form-control" id="get-guru" name="id_guru" required>

                  </select>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nominal SPP</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="number" name="nominal_spp" placeholder="Input Nominal SPP" class="form-control" required>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control" placeholder="Input Keterangan"></textarea>
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-insert-rombel" style="float:right">Insert</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end add rombel -->
<!-- edit rombel -->
<div class="modal fade" id="edit-rombel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Edit Data Rombel</h4>
      </div>
      <div class="modal-body"> 
        <form class="form-update-rombel" method="post">
          <input type="hidden" name="id_rombel" class="id_rombel-edit-rombel" placeholder="Input ID prodi" class="form-control">
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Tahun Ajaran</label>
              </div>
              <div class="col-12 col-md-9">
                  <select class="form-control id_ta-edit-rombel" id="get-ta" name="id_ta" required>

                  </select>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Tingkat/Jenjang</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="number" name="tingkat" placeholder="Input Tingat/Jenjang" class="form-control tingkat-jenjang"required>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nama Rombel</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="text" name="nm_rombel" placeholder="Input Nama Rombel" class="form-control nm_rombel-edit-rombel"required>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">prodi</label>
              </div>
              <div class="col-12 col-md-9">
                  <select class="form-control id_prodi-edit-rombel" id="get-prodi" name="id_prodi" required>

                  </select>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Guru</label>
              </div>
              <div class="col-12 col-md-9">
                  <select class="form-control id_guru-edit-rombel" id="get-guru" name="id_guru" required>

                  </select>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Nominal SPP</label>
              </div>
              <div class="col-12 col-md-9">
                  <input type="number" name="nominal_spp" placeholder="Input Nominal SPP" class="form-control nominal_spp-edit-rombel"required>
              </div>
          </div>
          <div class="row form-group">
              <div class="col col-md-3">
                  <label for="text-input" class=" form-control-label">Keterangan</label>
              </div>
              <div class="col-12 col-md-9">
                  <textarea name="keterangan" rows="4" cols="21" class="form-control keterangan-edit-rombel" placeholder="Input Keterangan"></textarea>
              </div>
          </div> 
      </div>
      <div class="modal-footer">
              <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>   
                    <button class="btn btn-success btn-update-rombel" style="float:right"><i class="fa fa-save"></i> Save  Changes</button>
              </div>
          </div>
        </form>
    </div>
  </div>
</div>
<!-- end edit rombel -->
 
<!-- add data anggota rombel -->
<!-- <div class="modal fade" id="insert-data-anggota-rombel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Tambah Data Anggota Rombel</h4>
      </div>
      <div class="modal-body">
        <form class="form-insert-data-anggota-rombel" method="post"> 
            <div class="row form-group"> 
              <div class="col-12 col-md-12">
                  <select class="form-control" name="id_rombel" id="get-anggota-rombel-insert">

                  </select>
              </div>
          </div> 
          <hr>
          <div class="row form-group">  
              <div class="col-12 col-md-12 scroll" > 
                  <div id="get-siswa">

                  </div> 
              </div>
          </div> 
      </div>
      <div class="modal-footer"> 
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>   
                <button class="btn btn-primary btn-insert-data-anggota-rombel" style="float:right">Insert</button>
          </div>  
        </form>
    </div>
  </div>
</div> -->
<!-- end add anggota rombel --> 

<!-- detail anggota rombel -->
<div class="modal fade" id="detail-data-anggota-rombel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Data Detail Anggota Rombel</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>No. Induk</th>
                <th>Nama Siswa</th>
                <th>Jenjang</th>
                <th>prodi</th>
                <th>Wali Kelas</th>
                <th>Tahun Ajaran</th>
                <th>Nominal SPP</th>
              </tr>
            </thead>
            <tbody id="data-detail-data-anggota-rombel">

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end detail anggota rombel -->

<!-- detail show data anggota rombel -->
<div class="modal fade" id="detail-show-data-anggota-rombel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Data Anggota Rombel</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive scroll-full">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>No. Induk</th>
                <th>Nama Siswa</th> 
              </tr>
            </thead>
            <tbody id="show-data-anggota-rombel">

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end detail anggota rombel -->


<!-- update kenaikan -->
<div class="modal fade" id="update-kenaikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:20%" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Kenaikan</h4>
      </div>
      <div class="modal-body"> 
        <form class="form-update-ta-anggota-rombel">
            <div class="row form-group"> 
                    <div class="col-12 col-md-12">
                    Tahun Ajaran
                        <select class="form-control" id="get-ta-kenaikan" name="id_ta" required>

                        </select>
                    </div>
                </div>
                <div class="row form-group"> 
                    <div class="col-12 col-md-12">
                    Kelas
                        <select class="form-control" id="get-id-rombel-kenaikan" name="id_rombel" required>

                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">  
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>  
                    <button class="btn btn-success btn-update-ta-anggota-rombel" style="float:right">Save Changes</button> 
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- end update kenaikan --> 

<!-- update kelulusan -->
<div class="modal fade" id="update-kelulusan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:20%" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Kelulusan</h4>
      </div>
      <div class="modal-body"> 
        <div class="row form-group"> 
            <div class="col-12 col-md-12">
            Tahun Ajaran
                <select class="form-control" id="get-ta-kelulusan" name="id_ta" required>

                </select>
            </div>
        </div>
        <div class="row form-group"> 
            <div class="col-12 col-md-12">
            Kelas
                <select class="form-control" id="get-id-rombel-kelulusan" name="id_tr_rombel" required>

                </select>
            </div>
        </div>
      </div>
      <div class="modal-footer">  
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>  
            <button class="btn btn-warning btn-update-data-anggota-rombel" style="float:right">Save Changes</button> 
      </div>
    </div>
  </div>
</div>
<!-- end update kelulusan -->
