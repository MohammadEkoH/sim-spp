<div class="login-box">
  <div class="login-logo">
    <a><b>Reset</b> password</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Please enter your ID to request a password reset.</p> 
    <form action="<?=base_url()?>auth/send_email" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="id_guru" placeholder="ID">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>  
      <div class="row"> 
        <!-- /.col --> 
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Send</button> 
        </div>
        <!-- /.col -->
      </div>
    </form>   
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->