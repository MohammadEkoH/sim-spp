
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
      table{
        border: 1px solid black;
        font-size: 14px;
      }
      th, td {
          padding: 2px;
      }
      th, td {
          text-align: left;
      }

      @page{margin: 0.2in 0.2in 0.2in 0.2in;}

    </style>
  </head>
  <body background="<?=base_url()?>assets/lunas.jpg" style="height: 10; width: 10px;">
    <input type="hidden" id="id-struk" value="<?=$id_tr_spp?>">
    <input type="hidden" id="id-agt-rombel-yo" value="<?=$id_tr_rombel?>">
      <table border="0">
        <tr>
          <td colspan="2"> <center><b>SMK ANTARTIKA SBY</b></center> </td>
        </tr>
        <tr>
          <td colspan="2"> <center><i>Bukti Pembayar SPP</i></center> </td>
        </tr>
        <tr>
          <td>Tanggal Bayar</td>
          <td> : <i id="waktu-tr"></i> &nbsp; <i style="font-size:10px" id="tgl-tr"></i> </td>
        </tr>
        <tr>
          <td colspan="2">---------------------------------------------------------------------</td>
        </tr>
        <tr>
          <td>No. Induk</td>
          <td> : <i id="no_induk-siswa"></i> </td>
        </tr>
        <tr>
          <td>Nama</td>
          <!-- <?php
            $kalimat = $user['nama'];
            $nama = substr($kalimat,0,18);
          ?> -->
          <td> : <i id="nm-siswa"></i> </td>
        </tr>
        <tr>
          <td>Kelas</td>
          <td> : <i id="kelas-siswa"></i></td>
        </tr>
        <tr>
          <td>Tahun Ajaran</td>
          <td> : <i id="id_ta-siswa"></i></td>
        </tr>
        <tr>
          <td colspan="2">---------------------------------------------------------------------</td>
        </tr>
        <tr>
          <td>Spp Bulan&nbsp; <i id="spp-bulan"></i> </td>
          <td> : <i>LUNAS</i> </td>
        </tr>
        <tr>
          <td>Total Bayar</td>
          <td> : <i id="dibayar"></i></td>
        </tr>
        <tr>
          <td colspan="2">---------------------------------------------------------------------</td>
        </tr> 
        <tr>
          <td colspan="2"> <i>MOHON DISIMPAN, STRUK INI ADALAH BUKTI</i> </td>
        </tr>
        <tr>
          <td colspan="2"> <i>PEMBAYARAN YANG SAH.</i> </td>
        </tr>
      </table>
    </div>

    <!-- jQuery -->
    <script src="<?=base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="<?=base_url()?>assets/MyCustomJS/struk.js"></script>  

  </body>
</html>
