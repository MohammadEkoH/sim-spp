<!-- detail anggota rombel -->
<div class="modal fade" id="detail-anggota-transaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>No. Induk</th>
                <th>Nama Siswa</th>
                <th>Jenjang</th>
                <th>prodi</th> 
                <th>Tahun Ajaran</th>
                <th>Nominal SPP</th>
              </tr>
            </thead>
            <tbody id="data-detail-anggota-pembayaran">

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>