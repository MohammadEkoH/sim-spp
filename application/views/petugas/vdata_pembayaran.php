<section class="content-header">
      <h1>
        Pembayaran SPP
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> 
        <li><a href="#"> Dashboard</a></li>  
        <li class="active">Pembayaran SPP</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3"> 
          <input type="hidden" value="<?=$id_tr_rombel?>" id="id_tr_rombel">
          <!-- Profile Image -->
          <div class="box box-primary">
            <form class="form-insert-bayar" method="post">
              <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="<?=base_url()?>assets/dist/img/user.png" alt="User profile picture">
                <br>
                <h3 class="profile-username text-center" id="nm-siswa-pembayaran"></h3> 
                <input type="hidden" id="id-tr-rombel-pembayaran" name="id_tr_rombel" required>
                <input type="hidden" id="nominal-spp" name="nominal_spp" required>
                <input type="hidden" id="id-petugas" name="id_petugas" required> 
                <input type="hidden" id="get-id-tr-rombel" value="<?=$id_tr_rombel?>" required> 
                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Kelas</b> <a class="pull-right" style="color:black" id="kelas-siswa-pembayaran"></a>
                  </li>
                  <li class="list-group-item">
                    <b>Tahun Ajaran</b> 
                    <a class="pull-right" style="color:black">
                      <!-- <select id="ta-siswa-pembayaran" required>
                      
                      </select> -->
                      <span id="ta-siswa-pembayaran"></span>
                    </a> 
                  </li>
                  <li class="list-group-item">
                    <b>Bulan</b> <a class="pull-right" style="color:black">
                      <select name="bulan" id="">
                          <option value="JAN">Januari</option>
                          <option value="FEB">Februari</option>
                          <option value="MAR">Maret</option>
                          <option value="APR">April</option>
                          <option value="MEI">Mei</option>
                          <option value="JUN">Juni</option>
                          <option value="JUL">Juli</option>
                          <option value="AGU">Agustus</option>
                          <option value="SEP">September</option>
                          <option value="OKT">Oktober</option>
                          <option value="NOV">November</option>
                          <option value="DES">Desember</option>
                      </select>
                    </a>
                  </li>
                  <li class="list-group-item">
                    <b>Nominal</b> <a class="pull-right" style="color:black" id="nominal-spp-show"> 
                    </a>
                  </li> 
                </ul> 
                <a class="btn btn-success btn-block btn-insert-bayar"><b class="fa fa-money"> Bayar</b></a>
              </div> 
            </form>
          </div> 
        </div>
        
        <div class="col-md-9">
          <div class="box box-primary">  
            <div class="box-group" id="accordion">
              <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it --> 
                <div class="box-header with-border">
                  <h4 class="box-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      Tahun Ajaran <span id="th-ajaran"></span> 
                    </a>
                  </h4>
                </div>
                <div >  
                  <div class="box-body">
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover">
                              <thead>
                              <tr>
                                  <th width="30px">No.</th>
                                  <th>Bulan</th>
                                  <th>Waktu Bayar</th>
                                  <th>Petugas</th>
                                  <th>Dibayarkan</th> 
                                  <th>Keterangan</th> 
                                  <th width="100px" style="display:none" id="siswa-th"></th>
                              </tr>
                              </thead>
                              <tbody id="data-bayar">

                              </tbody>
                          </table>
                      </div> 
                  </div>
                </div> 
              </div>
            </div>
          </div>
        </div> 
      </div> 

    </section>