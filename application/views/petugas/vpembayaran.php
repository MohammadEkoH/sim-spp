<section class="content-header">
    <h1>
    Pembayaran
    <small> SPP</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Dashboard</a></li> 
    <li class="active"> Pembayaran</li>
    </ol>
</section>
 
<section class="content">  
    <div class="box box-primary"> 
        <div class="box-body">  
            <div class="col-md-1">
                <select class="select2-selection__rendered" id="jurusan-siswa">  
                    <option>-Pilih-</option>
                    <option value="TKJ">TKJ</option>
                    <option value="TKR">TKR</option>
                    <option value="TPM">TPM</option>
                    <option value="TITL">TITL</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form-group input-group">
                <input type="text" class="form-control" id="id-search-data-pembayaran" name="" value="" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" id="btn-search-pembayaran"> <i class="fa fa-search"></i> </button>
                </span>
                </div>
            </div> 
        </div> 
    </div>  
     

    <div class="box box-success" style="display:none" id="show-data-siswa-pembayaran"> 
        <div class="box-body"> 
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="30px">No.</th>
                            <th>No. Induk</th>
                            <th>Nama</th>
                            <th>Kelas</th> 
                            <th width="100px" style="display:none" id="pembayaran-th"></th>
                        </tr>
                        </thead>
                        <tbody id="data-pembayaran">

                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>  
    </div>   
</section> 
 