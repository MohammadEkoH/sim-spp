<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_petugas extends CI_Model {
    function get_where_user($where)
    {
        $this->db->select('*')
                ->where($where);
        $query = $this->db->get('spp_petugas');
        return $query;
    } 

    function get_id_tr()
    {
        $this->db->select_max('id_tr_spp');
        $query = $this->db->get('spp_transaksi');
        return $query;
    }

    function insert_data($table, $data)
    {
        $this->db->insert($table, $data);
    }

    function get_data($table, $data)
	{
		$this->db->order_by($data, "ASC");
        $query = $this->db->get($table);
        return $query;
    } 

    function get_where_data($where, $table, $data)
	{
        $this->db->order_by($data, "ASC"); 
        $query = $this->db->get_where($table,$where);
        return $query;
    } 

    function pencarian_data_pembayaran($cari, $jurusan)
    {
        $this->db->select('*')
                ->from('spp_anggota_rombel')
                ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner')
                ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner')
                ->join('spp_ta', 'spp_rombel.id_ta = spp_ta.id_ta', 'inner');
        $this->db->order_by('nm_rombel', 'ASC');
        $this->db->order_by('tingkat', 'ASC');
        $this->db->order_by('nm_siswa', 'ASC');
        $this->db->like($cari, 'both');
        $this->db->where($jurusan);
        $query = $this->db->get();
        return $query;
    }

    function get_where_detail_anggota_pembayaran($where)
    {
        $this->db->select('*')
                ->from('spp_anggota_rombel')
                ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner')
                ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner')
                ->join('spp_guru', 'spp_guru.id_guru = spp_rombel.id_guru', 'inner')
                ->join('spp_prodi', 'spp_prodi.id_prodi = spp_rombel.id_prodi', 'inner');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    } 

    function where_show_data_pembayaran($where)
    {
        $this->db->select('*')
                ->from('spp_transaksi')
                ->join('spp_anggota_rombel', 'spp_anggota_rombel.id_tr_rombel = spp_transaksi.id_tr_rombel', 'inner')
                ->join('spp_petugas', 'spp_petugas.id_petugas = spp_transaksi.id_petugas', 'inner')
                ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner')
                ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner');
        // $this->db->order_by('waktu_tr', 'ASC');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    } 

    function get_data_akun($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('spp_petugas', $where);
        return $query;
    }  

    function where_data_pembayaran_siswa($where)
    {
        $this->db->select('*')
                ->from('spp_anggota_rombel')
                ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner')
                ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner') ;
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
 
    function where_data_pembayaran($where)
    {
        $this->db->select('*')
                ->from('spp_transaksi')
                ->join('spp_anggota_rombel', 'spp_anggota_rombel.id_tr_rombel = spp_transaksi.id_tr_rombel', 'inner')
                ->join('spp_petugas', 'spp_petugas.id_petugas = spp_transaksi.id_petugas', 'inner');
        $this->db->order_by('waktu_tr', 'ASC');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }
}

