<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {
    
    function get_where_user($where)
	{
		$this->db->select('spp_admin.nm_admin')
				->where($where);
		$query = $this->db->get('spp_admin');
		return $query;
    }
    
    // Dashboard ------------------------------------------------------------------------

    function get_jumlah_siswa()
    {  
        $query = $this->db->query(" SELECT DISTINCT(spp_rombel.id_prodi) AS id_prodi, 
                                    COUNT(spp_anggota_rombel.no_induk) AS Total     
                                    FROM spp_anggota_rombel
                                    INNER JOIN spp_rombel
                                    ON spp_anggota_rombel.id_rombel = spp_rombel.id_rombel  
                                    INNER JOIN spp_ta
                                    ON spp_rombel.id_ta = spp_ta.id_ta 
                                    INNER JOIN spp_prodi
                                    ON spp_rombel.id_prodi = spp_prodi.id_prodi 
                                    INNER JOIN spp_siswa
                                    ON spp_anggota_rombel.no_induk = spp_siswa.no_induk   
                                    WHERE spp_ta.status_aktif_ta = 1 
                                    AND spp_siswa.status_del_siswa = 1
                                    GROUP BY spp_rombel.id_prodi
                                    HAVING Total > 0");
        return $query; 
    }

    function get_data_jurusan()
    {
        $this->db->select('*')
                 ->from('spp_transaksi')
                 ->join('spp_anggota_rombel', 'spp_anggota_rombel.id_tr_rombel = spp_transaksi.id_tr_rombel')
                 ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel');
        // $this->db->where('id_rombel', 'RO-01');
        $query = $this->db->get();
        return $query;
    }

    function get_where_show_data_dashboard($id_ta, $id_rombel){ 
        $query = $this->db->query(" SELECT spp_siswa.nm_siswa,
                                    COUNT(spp_siswa.no_induk) AS K, spp_siswa.no_induk, 
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'JAN') THEN 1 ELSE 0
                                    END
                                    ) AS Januari,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'FEB') THEN 1 ELSE 0
                                    END
                                    ) AS Februari,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'MAR') THEN 1 ELSE 0
                                    END
                                    ) AS Maret,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'APR') THEN 1 ELSE 0
                                    END
                                    ) AS April,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'MEI') THEN 1 ELSE 0
                                    END
                                    ) AS Mei,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'JUN') THEN 1 ELSE 0
                                    END
                                    ) AS Juni,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'JUL') THEN 1 ELSE 0
                                    END
                                    ) AS Juli,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'AGU') THEN 1 ELSE 0
                                    END
                                    ) AS Agustus,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'SEP') THEN 1 ELSE 0
                                    END
                                    ) AS September,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'OKT') THEN 1 ELSE 0
                                    END
                                    ) AS Oktober,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'NOV') THEN 1 ELSE 0
                                    END
                                    ) AS November,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'DES') THEN 1 ELSE 0
                                    END
                                    ) AS Desember,
                                    SUM(spp_transaksi.dibayarkan) AS Total
                                    FROM spp_transaksi
                                    RIGHT JOIN spp_anggota_rombel
                                    ON spp_anggota_rombel.id_tr_rombel = spp_transaksi.id_tr_rombel
                                    RIGHT JOIN spp_siswa 
                                    ON spp_siswa.no_induk = spp_anggota_rombel.no_induk  
                                    RIGHT JOIN spp_rombel
                                    ON spp_rombel.id_rombel = spp_anggota_rombel.id_rombel
                                    RIGHT JOIN spp_ta
                                    ON spp_ta.id_ta = spp_rombel.id_ta
                                    WHERE spp_ta.status_aktif_ta = 1
                                    AND spp_siswa.status_del_siswa = 1
                                    AND spp_rombel.id_ta = '$id_ta'
                                    AND spp_anggota_rombel.id_rombel = '$id_rombel'
                                    GROUP BY spp_siswa.nm_siswa, spp_siswa.no_induk");
        return $query;
    }

    function get_where_rombel_dashboard($id_prodi, $id_ta){ 
        $this->db->select('*')  
                 ->from('spp_rombel')
                 ->join('spp_ta', 'spp_ta.id_ta = spp_rombel.id_ta', 'inner') 
                 ->where($id_prodi)
                 ->where($id_ta)
                 ->where('status_aktif_ta', 1);
        $query = $this->db->get();
        return $query;
    }
    // End Dashboard ------------------------------------------------------------------------

    // Siswa ------------------------------------------------------------------------

    function pencarian_siswa($nama, $status)
    { 
        $this->db->order_by('nm_siswa', 'ASC');
        $this->db->like($nama, 'both')
                 ->where($status);
        $query = $this->db->get('spp_siswa');
        return $query;
    }  

    function update_siswa($where, $data)
    {
        $this->db->where($where);
        $this->db->update('spp_siswa', $data);
    }    

    // End Siswa ------------------------------------------------------------------------

    // Guru ------------------------------------------------------------------------

    function get_id_guru()
    {
        $this->db->select_max('id_guru');
        $query = $this->db->get('spp_guru');
        return $query;
    }
    
    function pencarian_guru($nama, $status)
    {
        $this->db->order_by('nm_guru', 'ASC');
        $this->db->like($nama, 'both')
                ->where($status);
        $query = $this->db->get('spp_guru');
        return $query;
    }
 
    // End Guru ------------------------------------------------------------------------

    // Petugas ------------------------------------------------------------------------
    
    function get_id_petugas()
    {
        $this->db->select_max('id_petugas');
        $query = $this->db->get('spp_petugas');
        return $query;
    }
    
    function pencarian_petugas($cari, $status)
    {
        $this->db->order_by('nm_petugas', 'ASC');
        $this->db->like($cari, 'both')
                ->where($status);
        $query = $this->db->get('spp_petugas');
        return $query;
    } 

    // End Petugas ------------------------------------------------------------------------

    // Multi Function ------------------------------------------------------------------------

    function insert_data($table, $data)
    {
        $this->db->insert($table, $data);
    }

    function insert_data_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    function get_data($table, $urutkan)
	{
		$this->db->order_by($urutkan, "ASC");
        $query = $this->db->get($table);
        return $query;
    }  
    
    function get_where_data($table, $where, $urutkan='')
    {
        $this->db->order_by($urutkan, "ASC");
        $query = $this->db->get_where($table, $where);
        return $query;
    }

    function update_data($where, $table, $data)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function update_data_batch($where, $table, $data)
    {
        $this->db->where($where);
        $this->db->update_batch($table, $data);
    }

    function where_status_data($where, $table, $data)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    // Multi Function ------------------------------------------------------------------------

    // Tahun Ajaran ------------------------------------------------------------------------
 
    function pencarian_ta($id)
    {
        $this->db->like('id_ta', $id, 'both');
        $query = $this->db->get('spp_ta');
        return $query;
    } 

    // End Tahun Ajaran ------------------------------------------------------------------------

    // Jurusan ------------------------------------------------------------------------

    function pencarian_prodi($id)
    {
        $this->db->like('id_prodi', $id, 'both');
        $this->db->or_like('nm_prodi', $id, 'both');
        $query = $this->db->get('spp_prodi');
        return $query;
    }
    

    // End Jurusan ------------------------------------------------------------------------

    // Rombel ------------------------------------------------------------------------

    function get_id_rombel()
    {
        $this->db->select_max('id_rombel');
        $query = $this->db->get('spp_rombel');
        return $query;
    }
    
    function pencarian_rombel($cari, $ta)
    {
        $this->db->select('*')
                ->from('spp_rombel')
                ->join('spp_ta', 'spp_ta.id_ta = spp_rombel.id_ta', 'inner')
                ->join('spp_guru', 'spp_guru.id_guru = spp_rombel.id_guru', 'inner')
                ->join('spp_prodi', 'spp_prodi.id_prodi = spp_rombel.id_prodi', 'inner');
        $this->db->order_by('nm_rombel', 'ASC')
                 ->order_by('tingkat', 'ASC'); 
        $this->db->where($ta); 
        $this->db->like('nm_guru', $cari, 'both'); 
        $query = $this->db->get();
        return $query;
    }

    // get guru (rombel)
    function get_guru()
    {
        $this->db->order_by("nm_guru", "ASC");
        $query = $this->db->get('spp_guru');
        return $query;
    }

    // get TA (rombel)
    function get_ta()
    {
        $this->db->order_by("id_ta", "ASC");
        $query = $this->db->get('spp_ta');
        return $query;
    }

    // get prodi (rombel)
    function get_prodi()
    {
        $this->db->order_by("id_prodi", "ASC");
        $query = $this->db->get('spp_prodi');
        return $query;
    }
   
    // End Rombel ------------------------------------------------------------------------

    // Anggota Rombel ------------------------------------------------------------------------
    
    function get_id_tr_rombel()
    {
        $this->db->select_max('id_tr_rombel');
        $query = $this->db->get('spp_anggota_rombel');
        return $query;
    } 
    
    function data_prodi()
    {
        $this->db->order_by('nm_prodi', 'ASC'); 
        $query = $this->db->get('spp_prodi');
        return $query;
    }
    
    function pencarian_anggota_rombel($cari, $ta)
    {
        $this->db->select('*')
                ->from('spp_anggota_rombel')
                ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner')
                ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner');
        $this->db->order_by('nm_rombel', 'ASC');
        $this->db->like('nm_siswa', $cari, 'both');
        $this->db->where('id_ta', $ta);
        $query = $this->db->get();
        return $query;
    } 

    // function get_where_jumlah_anggota_rombel()
    // {  
    //     $query = $this->db->query(" SELECT DISTINCT(spp_anggota_rombel.id_rombel) AS id_rombel,
    //                                 COUNT(spp_anggota_rombel.id_rombel) AS Total, spp_rombel.tingkat, spp_rombel.nm_rombel, spp_rombel.id_prodi,
    //                                 COUNT(spp_anggota_rombel.id_tr_rombel) AS id_tr_rombel
    //                                 FROM spp_anggota_rombel
    //                                 RIGHT JOIN spp_rombel
    //                                 ON spp_anggota_rombel.id_rombel = spp_rombel.id_rombel 
    //                                 RIGHT JOIN spp_ta
    //                                 ON spp_rombel.id_ta = spp_ta.id_ta  
    //                                 RIGHT JOIN spp_siswa
    //                                 ON spp_anggota_rombel.no_induk = spp_siswa.no_induk  
    //                                 WHERE spp_ta.status_aktif_ta = 1
    //                                 AND spp_siswa.status_del_siswa = 1
    //                                 GROUP BY spp_anggota_rombel.id_rombel
    //                                 HAVING Total > 0");
    //     return $query; 
    // }  

    function get_where_jumlah_anggota_rombel()
    {
        $query = $this->db->query(" SELECT spp_rombel.id_rombel, 
                                    COUNT(spp_rombel.id_rombel) AS K, spp_rombel.id_rombel, spp_rombel.id_prodi, spp_rombel.tingkat,spp_rombel.nm_rombel, spp_rombel.id_ta,
                                    COUNT(spp_anggota_rombel.id_tr_rombel) AS id_tr_rombel,
                                    COUNT(spp_anggota_rombel.no_induk) AS Total
                                    FROM spp_anggota_rombel   
                                    RIGHT JOIN spp_siswa 
                                    ON spp_siswa.no_induk = spp_anggota_rombel.no_induk  
                                    RIGHT JOIN spp_rombel
                                    ON spp_rombel.id_rombel = spp_anggota_rombel.id_rombel
                                    RIGHT JOIN  spp_prodi  
                                    ON spp_prodi.id_prodi = spp_rombel.id_prodi
                                    RIGHT JOIN spp_ta
                                    ON spp_rombel.id_ta = spp_ta.id_ta 
                                    WHERE spp_ta.status_aktif_ta = 1 
                                    -- or spp_siswa.status_del_siswa = 1
                                    GROUP BY spp_rombel.id_rombel, spp_anggota_rombel.id_rombel  ");
        return $query;
        
    }
    // End Anggota Rombel ------------------------------------------------------------------------

    // Data Anggota Rombel ------------------------------------------------------------------------
    
    function where_show_data_rombel($id_rombel){
        $this->db->select('*')
                    ->from('spp_anggota_rombel')
                    ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner')
                    ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner')
                    ->order_by('tingkat', 'ASC')
                    ->order_by('nm_rombel', 'ASC')
                    ->order_by('nm_siswa', 'ASC') 
                    ->where($id_rombel);
        $query = $this->db->get();
        return $query;
    }

    function get_where_rombel($where)
	{ 
        $this->db->select('*')
                 ->from('spp_rombel') 
                 ->join('spp_ta', 'spp_ta.id_ta = spp_rombel.id_ta', 'inner');
        $this->db->where($where); 
        $query = $this->db->get();
        return $query;
    } 

    function get_where_data_siswa($where, $urutkan='')
    {
        $this->db->select('*')
                    ->from('spp_anggota_rombel')
                    ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'right')
                    ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'right') 
                    ->order_by('nm_siswa', 'ASC') 
                    ->where($where)
                    ->where('spp_anggota_rombel.no_induk', NULL);
        $query = $this->db->get();
        return $query;
    }
  
      // End Data Anggota Rombel ------------------------------------------------------------------------
}