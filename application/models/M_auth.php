<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {
	function auth_admin($where)
	{
		$this->db->select('spp_admin.id_admin', 'spp_admin.password', 'spp_admin.status_del_admin');
		$this->db->from('spp_admin');
		$this->db->where($where);
		$this->db->limit(1);
		$query = $this->db->get();
    if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
    
	function auth_guru($where)
	{
		$this->db->select('spp_guru.id_guru', 'spp_guru.password', 'spp_guru.status_del_guru');
		$this->db->from('spp_guru');
		$this->db->where($where);
		$this->db->limit(1);
		$query = $this->db->get();
    if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	} 
    
	function auth_petugas($where)
	{
		$this->db->select('spp_petugas.id_petugas', 'spp_petugas.password', 'spp_petugas.status_del_petugas');
		$this->db->from('spp_petugas');
		$this->db->where($where);
		$this->db->limit(1);
		$query = $this->db->get();
    if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	} 

	function get_password($table, $where)
	{         
		return $this->db->select('*')
				 		->get_where($table, $where);
	}
}