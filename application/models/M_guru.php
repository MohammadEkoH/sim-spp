<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_guru extends CI_Model {
	function get_where_user($where)
    {
        $this->db->select('*'); 
        $query = $this->db->get_where('spp_guru', $where);
        return $query;
    } 

    function pencarian_siswa($nama, $status)
    { 
        $this->db->order_by('nm_siswa', 'ASC'); 
        $this->db->select('*')
                 ->from('spp_anggota_rombel')
                 ->join('spp_rombel', 'spp_rombel.id_rombel = spp_anggota_rombel.id_rombel', 'inner')
                 ->join('spp_siswa', 'spp_siswa.no_induk = spp_anggota_rombel.no_induk', 'inner')
                 ->join('spp_ta', 'spp_ta.id_ta = spp_rombel.id_ta', 'inner');
        $this->db->like($nama, 'both')
                 ->where($status);
        $query = $this->db->get();
        return $query;
    } 

    function get_data_pembayaran($no_induk){ 
        $query = $this->db->query(" SELECT spp_siswa.nm_siswa,
                                    COUNT(spp_siswa.no_induk) AS K, spp_siswa.no_induk, 
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'JAN') THEN 1 ELSE 0
                                    END
                                    ) AS Januari,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'FEB') THEN 1 ELSE 0
                                    END
                                    ) AS Februari,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'MAR') THEN 1 ELSE 0
                                    END
                                    ) AS Maret,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'APR') THEN 1 ELSE 0
                                    END
                                    ) AS April,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'MEI') THEN 1 ELSE 0
                                    END
                                    ) AS Mei,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'JUN') THEN 1 ELSE 0
                                    END
                                    ) AS Juni,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'JUL') THEN 1 ELSE 0
                                    END
                                    ) AS Juli,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'AGU') THEN 1 ELSE 0
                                    END
                                    ) AS Agustus,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'SEP') THEN 1 ELSE 0
                                    END
                                    ) AS September,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'OKT') THEN 1 ELSE 0
                                    END
                                    ) AS Oktober,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'NOV') THEN 1 ELSE 0
                                    END
                                    ) AS November,
                                    SUM(
                                        CASE WHEN(spp_transaksi.bulan = 'DES') THEN 1 ELSE 0
                                    END
                                    ) AS Desember,
                                    SUM(spp_transaksi.dibayarkan) AS Total
                                    FROM spp_transaksi
                                    RIGHT JOIN spp_anggota_rombel
                                    ON spp_anggota_rombel.id_tr_rombel = spp_transaksi.id_tr_rombel
                                    RIGHT JOIN spp_siswa 
                                    ON spp_siswa.no_induk = spp_anggota_rombel.no_induk  
                                    RIGHT JOIN spp_rombel
                                    ON spp_rombel.id_rombel = spp_anggota_rombel.id_rombel
                                    RIGHT JOIN spp_ta
                                    ON spp_ta.id_ta = spp_rombel.id_ta
                                    WHERE spp_ta.status_aktif_ta = 1
                                    AND spp_siswa.status_del_siswa = 1 
                                    AND spp_anggota_rombel.no_induk = '$no_induk'
                                    GROUP BY spp_siswa.nm_siswa, spp_siswa.no_induk");
        return $query;
    }

    function get_data_akun($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('spp_guru', $where);
        return $query;
    }  
}