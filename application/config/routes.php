<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth'; 
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'auth';
$route['forget-password'] = 'auth/forget_pass';

// Dashboard
$route['ad/dashboard/data-jurusan/(:any)'] = 'admin/data_jurusan';

// admin
$route['ad/dashboard'] = 'admin';

// Master Data
$route['ad/dashboard/guru'] = 'admin/guru';
$route['ad/dashboard/guru/import'] = 'import/guru_import';
$route['ad/dashboard/siswa'] = 'admin/siswa';
$route['ad/dashboard/siswa/import'] = 'import/siswa_import';
$route['ad/dashboard/petugas'] = 'admin/petugas';
$route['ad/dashboard/petugas/import'] = 'import/petugas_import';

// Manajement
$route['ad/dashboard/tahun-ajaran'] = 'admin/tahun_ajaran';
$route['ad/dashboard/jurusan'] = 'admin/jurusan';
$route['ad/dashboard/room-belajar'] = 'admin/rombel';
$route['ad/dashboard/anggota-room-belajar'] = 'admin/anggota_rombel'; 

$route['ad/dashboard/data-anggota-room-belajar/(:any)/(:any)'] = 'admin/data_anggota_rombel';   

// Petugas
$route['pe/dashboard'] = 'petugas';  

// Pembayaran
$route['pe/dashboard/pembayaran'] = 'petugas/pembayaran';  
$route['pe/dashboard/data-pembayaran/(:any)'] = 'petugas/data_pembayaran';   

// guru
$route['gu/dashboard'] = 'guru';