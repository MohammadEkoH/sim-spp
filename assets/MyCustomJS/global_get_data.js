// get data Prodi
get_prodi();
function get_prodi()
{
  $.ajax({
    url         : base_url+'admin/get_prodi',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let option = "";
      option += '<option>'+"-pilih-"+'</option>';
      for (let i = 0; i < data.data.length; i++)
      {
        if (data.data[i].status_aktif_prodi == 1) {
          option += '<option value="'+data.data[i].id_prodi+'">'+data.data[i].id_prodi+' ('+data.data[i].nm_prodi+')'+'</option>';
        }else {
          option += "";
        }
      }
      $('#get-prodi').html(option); 
      $('.id_prodi-edit-rombel').html(option);
      $('#get-prodi-siswa').html(option);
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}
// get data guru
get_guru();
function get_guru()
{
  $.ajax({
    url         : base_url+'admin/get_guru',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let option = "";
      option += '<option>'+"-pilih-"+'</option>';
      for (let i = 0; i < data.data.length; i++)
      {
        if (data.data[i].status_del_guru == 1) {
          option += '<option value="'+data.data[i].id_guru+'">'+data.data[i].nm_guru+''+'</option>';
        }else {
          option += "";
        }
      }
      $('#get-guru').html(option);
      $('.id_guru-edit-rombel').html(option);
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
} 

// get where TA
get_where_ta();
function get_where_ta()
{
  $.ajax({
    url         : base_url+'admin/get_ta',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let option = "";
      option += '<option>'+"-pilih-"+'</option>';
      for (let i = 0; i < data.data.length; i++)
      {
        if (data.data[i].status_aktif_ta == 1) {
          option += '<option value="'+data.data[i].id_ta+'">'+data.data[i].id_ta+'</option>';
        }else {
          option += "";
        }
      }
      $('.show_ta_rombel').html(option); 
      $('.id_ta-edit-rombel').html(option); 
      $('.show_ta').html(option);  
      $('#insert-ta-rombel').html(option); 
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}
 