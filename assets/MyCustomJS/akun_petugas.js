get_data_user();
function get_data_user()
{
  $.ajax({
    url         : base_url+'petugas/akun_user/',
    method      : 'POST',
    datatype    : 'JSON', 
    cache:false,
    success: function(data){ 
        let obj = JSON.parse(data)  

        $('.email-akun-petugas').val(obj['email_petugas']);
        $('.nm-akun-petugas').val(obj['nm_petugas']);
        $('.alamat-akun-petugas').val(obj['alamat_petugas']);
        $('.no_hp-akun-petugas').val(obj['no_hp_petugas']);  
         
    },
    error : function(data)
    {
      console.error("Edit Gagal!");
    }
  });
}
 
$(document).ready(function(){ 
    $('.form-user').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'petugas/update_user/',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            alert(data);
            location.reload();
        },
        error : function(data)
        {
            alert("Update Gagal");
        }
        })
    });
}); 

cek();
function cek() {
    $(document).ready(function(){ 
        $('.form-pass').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url: base_url + 'petugas/cek_pass/',
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data){   
 
                    let obj = JSON.parse(data)  
    
                    alert(obj['data']);
                    if (obj['data'] == 'berhasil') {  
                        $('.callout-success').show();
                        $('.callout-danger').hide(); 
                    }

                    if (obj['data'] == 'gagal') {
                        $('.callout-danger').show(); 
                        $('.callout-success').hide();
                    }

                    $('#lama').val(''); 
                    $('#baru').val(''); 
                    $('#confirm').val(''); 
                },
                error : function(data) 
                {

                }
            })
        });
    }); 
}

 