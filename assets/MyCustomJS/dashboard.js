get_jumlah_siswa();
function get_jumlah_siswa()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'admin/get_jumlah_siswa/',
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        
    },
    success: function(data){  
        let html = ""; 
        for (let i = 0; i < data.data.length; i++)
        {
            if (data.data[i].id_prodi == 'TKJ') {
                if (data.data[i].Total > 0) {
                    $('#jumlah-tkj').html(data.data[i].Total);
                }else{
                    $('#jumlah-tkj').html("0");
                }
            }

            if (data.data[i].id_prodi == 'TKR') { 
                if (data.data[i].Total > 0) {
                    $('#jumlah-tkr').html(data.data[i].Total);
                }else{
                    $('#jumlah-tkr').html("0");
                }
            }
 
            if (data.data[i].id_prodi == 'TPM') {
                if (data.data[i].Total > 0) { 
                    $('#jumlah-tpm').html(data.data[i].Total);
                }else{
                    $('#jumlah-tpm').html("0");
                }
            }

            if (data.data[i].id_prodi == 'TITL') {
                if (data.data[i].Total > 0) { 
                    $('#jumlah-titl').html(data.data[i].Total);
                }else{
                    $('#jumlah-titl').html("0");
                }
            } 
        }  
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    });
}); 
}

// get data rombel
get_where_rombel_dashboard();
function get_where_rombel_dashboard()
{  
    $(".show_ta").change(function(){  
        $.ajax({
            url         : base_url+'admin/get_where_rombel_dashboard/' + $('#id-prodi').val()+'/'+this.value,
            method      : 'POST',
            datatype    : 'JSON',
            success     : function(data)
            {
              let option = "";
              option += '<option>'+"-Pilih-"+'</option>';
              for (let i = 0; i < data.data.length; i++)
              {   
                option += '<option value="'+data.data[i].id_rombel+'">'+data.data[i].rombel +'</option>';
                 
              }
              $('.show_id_rombel').html(option); 
            },
            error : function(data)
            {
              console.error('gagal');
            }
          });
    }); 
}

where_show_data_dashboard();
function where_show_data_dashboard(){    
    $(".btn-tampilkan").click(function(){   
        let id_ta = $('#id-ta').val();
        let id_rombel = $('#id-rombel').val();
        $.ajax({
            url         : base_url+'admin/get_where_show_data_dashboard/'+ id_ta +'/'+id_rombel,
            method      : 'POST',
            datatype    : 'JSON',
            beforeSend: function(e) {
              $(document).ajaxStart(function() { Pace.restart(); }); 
            },
            success     : function(data)
            {
              console.log(data);
              let html = "";
              let no = 1;
              let jan, feb, mar, apr, mei, jun, jul, agu, sep, okt, nov, des = "";
              for (let i = 0; i < data.data.length; i++)
              {  
                  
                  if(data.data[i].Januari == 1)
                  {
                    jan = 'L';
                  }else{
                    jan = 'BL';
                  }
                  
                  if(data.data[i].Februari == 1)
                  {
                    feb = 'L';
                  }else{
                    feb = 'BL';
                  }
                  
                  if(data.data[i].Maret == 1)
                  {
                    mar = 'L';
                  }else{
                    mar = 'BL';
                  }
                  
                  if(data.data[i].April == 1)
                  {
                    apr = 'L';
                  }else{
                    apr = 'BL';
                  } 
                  
                  if(data.data[i].Mei == 1)
                  {
                    mei = 'L';
                  }else{
                    mei = 'BL';
                  }
                  
                  if(data.data[i].Juni == 1)
                  {
                    jun = 'L';
                  }else{
                    jun = 'BL';
                  }
                  
                  if(data.data[i].Juli == 1)
                  {
                    jul = 'L';
                  }else{
                    jul = 'BL';
                  }
                  
                  if(data.data[i].Agustus == 1)
                  {
                    agu = 'L';
                  }else{
                    agu = 'BL';
                  }
                  
                  if(data.data[i].September == 1)
                  {
                    sep = 'L';
                  }else{
                    sep = 'BL';
                  }
                  
                  if(data.data[i].Oktober == 1)
                  {
                    okt = 'L';
                  }else{
                    okt = 'BL';
                  }
                  
                  if(data.data[i].November == 1)
                  {
                    nov = 'L';
                  }else{
                    nov = 'BL';
                  }
                  
                  if(data.data[i].Desember == 1)
                  {
                    des = 'L';
                  }else{
                    des = 'BL';
                  }

                  html +=  '<tr>'+
                              '<td>'+no+'</td>'+
                              '<td>'+data.data[i].no_induk+'</td>'+
                              '<td>'+data.data[i].nm_siswa+'</td>'+
                              '<td>'+jan+'</td>'+   
                              '<td>'+feb+'</td>'+   
                              '<td>'+mar+'</td>'+   
                              '<td>'+apr+'</td>'+   
                              '<td>'+mei+'</td>'+   
                              '<td>'+jun+'</td>'+   
                              '<td>'+jul+'</td>'+   
                              '<td>'+agu+'</td>'+   
                              '<td>'+sep+'</td>'+ 
                              '<td>'+okt+'</td>'+   
                              '<td>'+nov+'</td>'+   
                              '<td>'+des+'</td>'+   
                              '<td>'+data.data[i].Total+'</td>'+  
                            '</tr>';
                        no++;  
              }
              $('.show-data-box').show();
              $('#show-data').html(html);
            },
            error : function(data)
            {
              console.error('gagal');
            }
          }); 
    });  
  }