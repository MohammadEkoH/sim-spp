
get_where_data_pembayaran_siswa();
function get_where_data_pembayaran_siswa() {
  $.ajax({
    url: base_url + 'petugas/get_where_data_pembayaran_siswa/' + $('#id_tr_rombel').val(),
    method: 'POST',
    datatype: 'JSON',
    beforeSend: function(e) {
        $(document).ajaxStart(function() { Pace.restart(); }); 
    },
    cache: false,
    success: function (data) {
      let html = "";
      for (let i = 0; i < data.data.length; i++) {
        $('#id-tr-rombel-pembayaran').val(data.data[i].id_tr_rombel);
        $('#nm-siswa-pembayaran').html(data.data[i].nm_siswa);
        $('#kelas-siswa-pembayaran').html(data.data[i].tingkat + ' - ' + '(' + data.data[i].nm_rombel+')');
        $('#nominal-spp').val(data.data[i].nominal_spp);
        $('#nominal-spp-show').html(data.data[i].nominal_spp_show);
        $('#ta-siswa-pembayaran').html(data.data[i].id_ta);
      }
    },
    error: function (data) {
      console.error("Get Data Gagal!");
    }
  });
}

insert_pembayaran();
function insert_pembayaran()
{
  // insert pembayaran
  $(document).ready(function(){
    $('.btn-insert-bayar').click(function()
    {
      $.ajax({
          url : base_url+'petugas/insert_pembayaran',
          method : "POST",
          data   : $('.form-insert-bayar').serialize(),
          success: function(data){
            alert("Terbayar");
            get_show_data_pembayaran();
          },
          error : function(data){ 
          }
      });
    });
  });
}

// get petugas
get_petugas();
function get_petugas()
{
  $.ajax({
    url         : base_url+'petugas/get_petugas',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      for (let i = 0; i < data.data.length; i++)
      {
        $('#id-petugas').val(data.data[i].id_petugas);
      }
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}

// get data TA
// get_ta();
// function get_ta()
// { 
//   $.ajax({
//     url         : base_url+'petugas/get_ta',
//     method      : 'POST',
//     datatype    : 'JSON',
//     success     : function(data)
//     {
//       let html = ""; 
//       for (let i = 0; i < data.data.length; i++)
//       { 
//         html += '<option value="'+data.data[i].id_ta+'">'+data.data[i].id_ta+'</option>'; 
//       }
//       $('#ta-siswa-pembayaran').html(html);
//     },
//     error : function(data)
//     {
//       console.error('gagal');
//     }
//   });
// }



get_show_data_pembayaran();
function get_show_data_pembayaran()
{ 
  let id =  $('#get-id-tr-rombel').val();  
  $.ajax({
    url         : base_url+'petugas/get_show_data_pembayaran/'+ id ,
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      console.log(data);
      let no=1; 
      let html="";
      for (let i = 0; i < data.data.length; i++)
      {  
        $('#th-ajaran').html(data.data[i].id_ta);  

        html += '<tr>'+
                  '<td>'+ no +'</td>'+
                  '<td>'+data.data[i].bulan+'</td>'+
                  '<td>'+data.data[i].waktu_tr+'</td>'+
                  '<td>'+data.data[i].nm_petugas+'</td>'+
                  '<td>'+data.data[i].dibayarkan+'</td>'+
                  '<td>'+data.data[i].keterangan+'</td>'+
                  '<td>'+
                    '<div class="form-group col-md-12">'+
                      '<div class="col-md-6">'+
                        '<a href="'+base_url+'petugas/where_del'+'" class="btn btn-danger btn-sm"> <i class="fa fa-trash" title="Hapus"></i> </a> &nbsp;' +
                      '</div>'+ 
                      '<form class="" action="'+base_url+'petugas/struk'+'" method="post">'+
                        '<input type="hidden" name="id_tr_spp" value="'+data.data[i].id_tr_spp+'">'+
                        '<input type="hidden" name="id_tr_rombel" value="'+data.data[i].id_tr_rombel+'">'+

                        '<button type="submit" class="btn btn-success btn-sm"title="Cetak Struk"><i class="fa fa-print"></i></button>'+
                      '</form>'+ 
                    '</div>'+
                     
                  '</td>'+
                '</tr>';
        no++; 
      } 
      $('#data-bayar').html(html); 
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}
 
$(document).ready( function() {
  $("#confirm_del_data").click( function() {
    jConfirm('Apakah anda ingin menghapus data ini?', 'Confirmation Dialog Box', function(r) {
      if( r ) jAlert('Data berhasil dihapus', 'Confirmation Dialog Box Results | True')
      else jAlert('Data tidak jadi dihapus.', 'Confirmation Dialog Box Results | False');
      });
  });
});


 
