
pencarian_data_pembayaran();
function pencarian_data_pembayaran(){
  $("#btn-search-pembayaran").click(function() {  
    let cari = $("#id-search-data-pembayaran").val(); 
    let jurusan =  $("#jurusan-siswa").val(); 
    $.ajax({
      url         : base_url+'petugas/pencarian_data_pembayaran/'+ cari +'/'+jurusan,
      method      : 'POST',
      datatype    : 'JSON',
      beforeSend: function(e) {
        $(document).ajaxStart(function() { Pace.restart(); }); 
        let html="Memuat...";
        $('#data-transaksi').html(html);
      },
      success     : function(data)
      {
        let btn = '<i class="fa fa-search"></i>';
          $("#btn-search-siswa").html(btn).removeAttr("disabled"); 
          let html = "";
          let no = 1;
          for (let i = 0; i < data.data.length; i++)
          {
            if (data.data[i].status_del_siswa == 1 && data.data[i].status_aktif_ta == 1) {
              $('#aksi-transaksi-show').show();
              html +=   '<tr>'+
                          '<td>'+no+'</td>'+
                          '<td>'+data.data[i].no_induk+'</td>'+
                          '<td>'+data.data[i].nm_siswa+'</td>'+
                          '<td>'+data.data[i].tingkat+' - '+data.data[i].nm_rombel+'</td>'+
                          '<td align="center">'+
                            '<a onclick="where_detail_anggota_pembayaran('+"'"+data.data[i].id_tr_rombel+"'"+')" name="button" class="btn btn-primary btn-xs" title="detail" data-toggle="modal" data-target="#detail-anggota-transaksi">View</a> &nbsp &nbsp'+
                            '<a href="'+base_url+'pe/dashboard/data-pembayaran/'+data.data[i].id_tr_rombel+'" name="button" class="btn btn-success btn-xs" title="bayar">Bayar</a>'+
                          '</td>'+
                        '</tr>';
                no++;
            } 
          }
          $('#show-data-siswa-pembayaran').show();
          $('#data-pembayaran').html(html);
      },
      error : function(data)
      {
        console.error('gagal');
      }
    });
  });
}

// Where detail anggota transaksi
function where_detail_anggota_pembayaran(id)
{
  $.ajax({
    url         : base_url+'petugas/get_where_detail_anggota_pembayaran/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    cache:false,
    beforeSend: function(e) {
      $(document).ajaxStart(function() { Pace.restart(); }); 
      let html="Memuat...";
      $('#data-detail-anggota-transaksi').html(html);
    },
    success: function(data){
      let html="";
      for (let i = 0; i < data.data.length; i++)
      {
        html += '<tr>'+
                  '<td>'+data.data[i].no_induk+'</td>'+
                  '<td>'+data.data[i].nm_siswa+'</td>'+
                  '<td>'+data.data[i].tingkat+' - '+data.data[i].nm_rombel+'</td>'+
                  '<td>'+data.data[i].nm_prodi+'</td>'+
                  '<td>'+data.data[i].id_ta+'</td>'+
                  '<td>'+data.data[i].nominal_spp+'</td>'+
                '</tr>';
      }
      $('#data-detail-anggota-pembayaran').html(html);
    },
    error : function(data)
    {
      console.error("Get Data Gagal!");
    }
  });
}
