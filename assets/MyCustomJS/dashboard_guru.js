  pencarian_siswa();
  function pencarian_siswa()
  {
    $(document).ready(function(){
      $(".fa-search").click(function(){
      //   $(this).html("Cari...").attr("disabled", "disabled"); 
        let nama = $(".nama-siswa").val();   
        console.log(nama);
        
        $.ajax({
          url: base_url + 'guru/pencarian_siswa/'+ nama,
          type: 'POST',
          dataType: "json",
          beforeSend: function(e) { 
            let html="Memuat...";
            $('#data-siswa').html(html); 
          },
          success: function(data){ 
            console.log(data);
            
            let btn = '<i class="fa fa-search"></i>';
            $("btn-search-siswa").html(btn).removeAttr("disabled"); 
            let html = "";
            let no = 1;
            for (let i = 0; i < data.data.length; i++)
            {
              if (data.data[i].status_del_siswa == 1) { 
                html +=  '<tr>'+
                          '<td>'+no+'</td>'+
                          '<td>'+data.data[i].no_induk+'</td>'+
                          '<td>'+data.data[i].nm_siswa+'</td>'+
                          '<td>'+data.data[i].kota_lahir+', '+data.data[i].tgl_lahir+'</td>'+
                          '<td>'+data.data[i].alamat+'</td>'+
                          '<td>'+data.data[i].nm_ibu+'</td>'+
                          '<td>'+data.data[i].no_hp_1+'</td>'+
                          '<td>'+data.data[i].no_hp_2+'</td>'+ 
                          '<td align="center">'+
                          '<button onclick="where_show_data_dashboard(' + "'" + data.data[i].no_induk + "'" +')" name="button" title="aktif"  data-toggle="modal" data-target="#detail-siswa-pembayaran" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></button>'+
                          '</td>'+
                        '</tr>'; 
              }  
              no++;
            }
            $('#data-siswa').html(html);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.error(xhr.responseText);
          }
        });
      });
    });
  }
 
function where_show_data_dashboard(no_induk){        
  $.ajax({
      url         : base_url+'guru/show_data_pembayaran/'+no_induk,
      method      : 'POST',
      datatype    : 'JSON',
      beforeSend: function(e) {
        $(document).ajaxStart(function() { Pace.restart(); }); 
      },
      success     : function(data)
      {
        let html = "";
        let no = 1;
        let jan, feb, mar, apr, mei, jun, jul, agu, sep, okt, nov, des = "";
        for (let i = 0; i < data.data.length; i++)
        {  
            
            if(data.data[i].Januari == 1)
            {
              jan = 'L';
            }else{
              jan = 'BL';
            }
            
            if(data.data[i].Februari == 1)
            {
              feb = 'L';
            }else{
              feb = 'BL';
            }
            
            if(data.data[i].Maret == 1)
            {
              mar = 'L';
            }else{
              mar = 'BL';
            }
            
            if(data.data[i].April == 1)
            {
              apr = 'L';
            }else{
              apr = 'BL';
            } 
            
            if(data.data[i].Mei == 1)
            {
              mei = 'L';
            }else{
              mei = 'BL';
            }
            
            if(data.data[i].Juni == 1)
            {
              jun = 'L';
            }else{
              jun = 'BL';
            }
            
            if(data.data[i].Juli == 1)
            {
              jul = 'L';
            }else{
              jul = 'BL';
            }
            
            if(data.data[i].Agustus == 1)
            {
              agu = 'L';
            }else{
              agu = 'BL';
            }
            
            if(data.data[i].September == 1)
            {
              sep = 'L';
            }else{
              sep = 'BL';
            }
            
            if(data.data[i].Oktober == 1)
            {
              okt = 'L';
            }else{
              okt = 'BL';
            }
            
            if(data.data[i].November == 1)
            {
              nov = 'L';
            }else{
              nov = 'BL';
            }
            
            if(data.data[i].Desember == 1)
            {
              des = 'L';
            }else{
              des = 'BL';
            }

            html +=  '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+data.data[i].no_induk+'</td>'+
                        '<td>'+data.data[i].nm_siswa+'</td>'+
                        '<td>'+jan+'</td>'+   
                        '<td>'+feb+'</td>'+   
                        '<td>'+mar+'</td>'+   
                        '<td>'+apr+'</td>'+   
                        '<td>'+mei+'</td>'+   
                        '<td>'+jun+'</td>'+   
                        '<td>'+jul+'</td>'+   
                        '<td>'+agu+'</td>'+   
                        '<td>'+sep+'</td>'+ 
                        '<td>'+okt+'</td>'+   
                        '<td>'+nov+'</td>'+   
                        '<td>'+des+'</td>'+   
                        '<td>'+data.data[i].Total+'</td>'+  
                      '</tr>';
                  no++;  
        } 
        $('#show-data-pembayaran').html(html);
      },
      error : function(data)
      {
        console.error('gagal');
      }
    });  
  }

get_data_user();
function get_data_user()
{
  $.ajax({
    url         : base_url+'guru/akun_user/',
    method      : 'POST',
    datatype    : 'JSON', 
    cache:false,
    success: function(data){ 
        let obj = JSON.parse(data)  

        $('.email-akun-guru').val(obj['email_guru']);
        $('.nm-akun-guru').val(obj['nm_guru']);
        $('.alamat-akun-guru').val(obj['alamat_guru']);
        $('.no_hp-akun-guru').val(obj['no_hp_guru']);  
         
    },
    error : function(data)
    {
      console.error("Edit Gagal!");
    }
  });
}
 
$(document).ready(function(){ 
    $('.form-user').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'guru/update_user/',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            alert(data);
            location.reload();
        },
        error : function(data)
        {
            alert("Update Gagal");
        }
        })
    });
}); 

cek();
function cek() {
    $(document).ready(function(){ 
        $('.form-pass').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url: base_url + 'guru/cek_pass/',
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data){   
 
                    let obj = JSON.parse(data)  
    
                    alert(obj['data']);
                    if (obj['data'] == 'berhasil') {  
                        $('.callout-success').show();
                        $('.callout-danger').hide(); 
                    }

                    if (obj['data'] == 'gagal') {
                        $('.callout-danger').show(); 
                        $('.callout-success').hide();
                    }

                    $('#lama').val(''); 
                    $('#baru').val(''); 
                    $('#confirm').val(''); 
                },
                error : function(data) 
                {

                }
            })
        });
    }); 
}

 