// Cari prodi
pencarian_prodi();
function pencarian_prodi()
{
  $(document).ready(function(){
    $("#btn-search-prodi").click(function(){
    //   $(this).html("Cari...").attr("disabled", "disabled");
      // let value = $("#nama-siswa").val();
      // console.log(value);
      $.ajax({
        url: base_url + 'admin/pencarian_prodi/'+ $("#id-search-prodi").val(),
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) {
            $(document).ajaxStart(function() { Pace.restart(); }); 
            let html="Memuat...";
            $('#data-prodi').html(html);
        },
        success: function(data){
          let btn = '<i class="fa fa-search"></i>';
          $("#btn-search-prodi").html(btn).removeAttr("disabled");
          console.log(data);
          let html = "";
          let no = 1;
          for (let i = 0; i < data.data.length; i++)
          {
            if (data.data[i].status_aktif_prodi == 1) {
              html +=  '<tr>'+
                         '<td>'+no+'</td>'+
                         '<td>'+data.data[i].id_prodi+'</td>'+
                         '<td>'+data.data[i].nm_prodi+'</td>'+
                         '<td>'+data.data[i].keterangan_prodi+'</td>'+
                         '<td align="center">'+
                          '<button onclick="get_where_edit_prodi(' + "'" + data.data[i].id_prodi + "'" +')" name="button" data-toggle="modal" data-target="#edit-prodi" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                          '<button onclick="where_delete_prodi(' + "'" + data.data[i].id_prodi + "'" +')" name="button" title="non-aktif"  class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>'+
                         '</td>'+
                       '</tr>';
                       no++;
            }else {
              html += "";
            }
          }
          $('#prodi-th').show();
          $('#data-prodi').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}

$(document).ready(function(){
    $('.form-insert-prodi').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'admin/insert_prodi',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            alert(data);
            location.reload();
        },
        error : function(data)
        {
            alert("Insert Gagal");
        }
        })
    });

    $('.form-update-prodi').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'admin/update_prodi',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            alert(data);
            location.reload();
        },
        error : function(data)
        {
            alert("Update Gagal");
        }
        })
    });
}); 
  
function get_where_edit_prodi(id)
{
  $.ajax({
    url         : base_url+'admin/get_where_edit_prodi/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) {
        $(document).ajaxStart(function() { Pace.restart(); }); 
    },
    cache:false,
    success: function(data){
        for (let i = 0; i < data.data.length; i++) {
          $('#id-prodi-edit').val(data.data[i].id_prodi);
          $('#nm_prodi-prodi-edit').val(data.data[i].nm_prodi);
          $('.keterangan-edit-prodi').val(data.data[i].keterangan_prodi);
        }
    },
    error : function(data)
    {
      console.error("Edit Gagal!");
    }
  });
}
  
// Delete Prodi
function where_delete_prodi(id)
{
    $.ajax({
        url         : base_url+'admin/where_delete_prodi/'+id,
        method      : 'POST',
        datatype    : 'JSON',
        beforeSend: function(e) {
            $(document).ajaxStart(function() { Pace.restart(); }); 
        },
        cache:false,
        success: function(data){
        location.reload();
        alert("Data Non_Aktif");
        },
        error : function(data)
        {
        console.error("Non-Aktif Gagal!");
        }
    });
}
  