
Pencarian_anggota_rombel();
function Pencarian_anggota_rombel()
{
  $(document).ready(function(){
    $("#btn-search-anggota-rombel").click(function(){ 
      let cari = $('#id-search-anggota-rombel').val(); 
      $.ajax({
        url: base_url + 'admin/get_where_jumlah_anggota_rombel/'+ cari,
        type: 'POST',
        dataType: "json", 
        beforeSend: function(e) { 
            let html="Memuat..."; 
        },
        success: function(data){
          let html = '';
          let no = 1;
          for (let i = 1; i < data.data.length; i++)
          {  
            $('#id-data-rombel').val(data.data[i].id_rombel);
            html += '<tr>'+ 
                    '<td>'+no+'</td>'+  
                    '<td>'+data.data[i].tingkat+' - '+data.data[i].nm_rombel+'</td>'+ 
                    // '<td>'+data.data[i].Total+'</td>'+  
                    '<td>'+ 
                      '<center><button onclick="where_show_data_rombel(' + "'" + data.data[i].id_rombel + "'" +')" name="button" data-toggle="modal" data-target="#detail-show-data-anggota-rombel" title="Show"  class="btn btn-primary btn-xs"><i class="fa fa-users"></i> &nbsp; anggota rombel</button> '+
                      '<a href="data-anggota-room-belajar/'+data.data[i].id_rombel+'/'+data.data[i].id_prodi+'" name="button" title="go to"  class="btn btn-success btn-xs"><i class="fa fa-arrow-right"></i></button>  </center>'+
                    '</td>'+  
                '</tr>';  
                no++;  
          } 
          $('#data-anggota-rombel').html(html); 
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}
  
$(document).ready(function(){
  $('.form-insert-anggota-rombel').on('submit', function(event){
      event.preventDefault();
      $.ajax({
      url: base_url + 'admin/insert_anggota_rombel',
      method:"POST",
      data:new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      success:function(data){ 
          alert(data);
          location.reload();
      },
      error : function(data)
      {
          alert("Insert Gagal");
      }
      })
  });
});

// get data siswa
// get_siswa();
// function get_siswa()
// {
//   $('#get-anggota-rombel-insert').on('change', function()
//   {  
//     $.ajax({
//       url         : base_url+'admin/get_where_siswa/' + this.value,
//       method      : 'POST',
//       datatype    : 'JSON',
//       beforeSend: function(e) { 
//       },
//       success     : function(data)
//       {
//         let option = "";  
//         for (let i = 0; i < data.data.length; i++)
//         { 
//           if (!data.data[i].jurusan_siswa == '') {
//             option +=
//                     '<div class="checkbox">'+
//                       '<label>'+
//                         '<tr>'+
//                           '<td>'+'<input type="checkbox" class="sub_chk" name=no_induk[]:checked value="'+data.data[i].no_induk+'">'+'</td> '+
//                           '<td>'+data.data[i].nm_siswa+'</td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ 
//                           '<td>'+data.data[i].ttl+'</td>'+  
//                         '</tr>'+
//                       '<label>'+
//                     '</div>';
//           }else{
//             option = "oke";
//           }
  
//         }
//         $('#get-siswa').html(option);
//       },
//       error : function(data)
//       {
//         console.error('gagal');
//       }
//     });
//   }); 
// }

// get data rombel
get_anggota_rombel_insert();
function get_anggota_rombel_insert()
{
  $.ajax({
    url         : base_url+'admin/get_where_rombel',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let option = "";
      option += '<option>'+"-pilih-"+'</option>';
      for (let i = 0; i < data.data.length; i++)
      { 
        option += '<option value="'+data.data[i].id_prodi+'">'+data.data[i].rombel +'</option>';
      }
      $('#get-anggota-rombel-insert').html(option); 
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}

function where_edit_rombel(id)
{
  $.ajax({
    url         : base_url+'admin/get_where_edit_rombel/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    cache:false,
    success: function(data){
        for (let i = 0; i < data.data.length; i++) {
          $('.id_rombel-edit-rombel').val(data.data[i].id_rombel);
          if (data.data[i].status_aktif_ta == 1) { 
            $('#ta-siswa-pembayaran').val(data.data[i].id_ta);
          }else{ 
            $('#ta-siswa-pembayaran').val(0);
          } 
        }
    },
    error : function(data)
    {
      console.error("Get Data Gagal!");
    }
  });
}

// get ta
get_ta();
function get_ta()
{
  $.ajax({
    url         : base_url+'admin/get_ta',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let html = ""; 
      html += '<option>-Pilih-</option>';
      for (let i = 0; i < data.data.length; i++)
      {  
        $('#get-ta-kelulusan').val(data.data[i].id_ta);
        $('#get-ta-kenaikan').val(data.data[i].id_ta);
        html += '<option value="'+data.data[i].id_ta+'">'+data.data[i].id_ta +'</option>';
      }
      $('#get-ta-kelulusan').html(html);
      $('#get-ta-kenaikan').html(html); 
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
} 
 
// get data rombel
get_where_rombel();
function get_where_rombel()
{
  $.ajax({
    url         : base_url+'admin/get_where_rombel',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let html = ""; 
      html += '<option>-Pilih-</option>';
      for (let i = 0; i < data.data.length; i++)
      { 
        html += '<option value="'+data.data[i].id_rombel+'">'+data.data[i].rombel +'</option>';
      }  

      $('#get-id-rombel-kenaikan').html(html);
      $('#get-id-rombel-kelulusan').html(html);
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}

show_where_data_siswa();
function show_where_data_siswa(){ 
  $.ajax({
    url         : base_url+'admin/get_where_siswa/' + $('#get-id-prodi').val(),
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    success     : function(data)
    {
      let option = "";  
      let no = 1;
      for (let i = 0; i < data.data.length; i++)
      { 
        if (!data.data[i].jurusan_siswa == '') {
          option += 
                  // '<div class="checkbox">'+
                    // '<label>'+
                      '<tr>'+ 
                        '<td>'+'<input type="checkbox" class="sub_chk" name=no_induk[]:checked value="'+data.data[i].no_induk+'">'+'</td> '+
                        '<td>'+data.data[i].no_induk+'.  </td>'+ 
                        '<td>'+data.data[i].nm_siswa+'</td>'+ 
                        '<td>'+data.data[i].ttl+'</td>'+  
                      '</tr>'+
                    // '<label>'+
                  // '</div>'; 
                  no++;
        }else{
          option = "";
        }

      }
      $('#where-data-siswa').html(option);
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}

show_data_rombel();
function show_data_rombel(){   
  $.ajax({
    url         : base_url+'admin/show_data_rombel/'+$('#get-id_rombel').val(),
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    success     : function(data)
    {
      let html = "";
      let no = 1;
      for (let i = 0; i < data.data.length; i++)
      { 
        if(data.data[i].status_del_siswa == 1){
          html +=  '<tr>'+
                      '<td>'+no+'</td>'+
                      '<td>'+data.data[i].no_induk+'</td>'+
                      '<td>'+data.data[i].nm_siswa+'</td>'+   
                    '</tr>';
                no++; 
        }
      }
      $('#where-data-anggota-rombel').html(html); 
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });  
}  

function where_show_data_rombel(id){  
  $.ajax({
    url         : base_url+'admin/where_show_data_rombel/'+ id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    success     : function(data)
    {
      console.log(id);
      let html = "";
      let no = 1;
      for (let i = 0; i < data.data.length; i++)
      { 
        if(data.data[i].status_del_siswa == 1){
          html +=  '<tr>'+
                      '<td>'+no+'</td>'+
                      '<td>'+data.data[i].no_induk+'</td>'+
                      '<td>'+data.data[i].nm_siswa+'</td>'+   
                    '</tr>';
                no++; 
        }
      }
      $('#show-data-anggota-rombel').html(html); 
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });  
}

update_ta_anggota_rombel();
function update_ta_anggota_rombel()
{
  $(document).ready(function(){
    $('.btn-update-ta-anggota-rombel').click(function()
    {  
      let where = $('').val();
      $.ajax({
          url : base_url+'admin/update_ta_anggota_rombel',
          method : "POST",
          beforeSend: function(e) { 
          },
          data   : $('.form-update-ta-anggota-rombel').serialize(),
          success: function(data){
            // alert(data);
            console.log(data);
          },
          error : function(data){
            // alert("Insert Gagal!"+data);
          }
      });
    });
  });
} 