// get where rombel
get_where_rombel();
function get_where_rombel()
{
  $.ajax({
    url         : base_url+'admin/get_rombel',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let option = "";
      option += '<option>'+"--pilih--"+'</option>';
      for (let i = 0; i < data.data.length; i++)
      {
        option += '<option value="'+data.data[i].id_rombel+'">'+data.data[i].tingkat+'-'+data.data[i].nm_rombel+' ('+data.data[i].id_ta+') '+'</option>';
      }
      $('.id_rombel-edit-anggota-rombel').html(option);
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}

// get where siswa
get_where_siswa();
function get_where_siswa()
{
  $.ajax({
    url         : base_url+'admin/get_siswa',
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let option = "";
      option += '<option>'+"--pilih--"+'</option>';
      for (let i = 0; i < data.data.length; i++)
      {
        if (data.data[i].status_del_siswa == 1) {
          option += '<option value="'+data.data[i].no_induk+'">'+data.data[i].nm_siswa+'</option>';
        }else {
          option += "";
        }
      }
      $('.no_induk-edit-anggota-rombel').html(option);
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}

$(document).ready(function(){
  $('.form-insert-rombel').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/insert_rombel',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Insert Gagal");
    }
    })
  });

  $('.form-update-rombel').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/update_rombel',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Update Gagal");
    }
    })
  });
}); 


// Edit data guru
function get_where_edit_rombel(id)
{
  $.ajax({
    url         : base_url+'admin/get_where_edit_rombel/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    cache:false,
    success: function(data){
      let option="";
      for (let i = 0; i < data.data.length; i++) { 
        if (data.data[i].status_aktif_ta == 1) {
          $('.id_ta-edit-rombel').val(data.data[i].id_ta);
          $('.show_ta').val(data.data[i].id_ta);
          $('#ta-siswa-pembayaran').val(data.data[i].id_ta);
        }else{
          $('.id_ta-edit-rombel').val(0); 
          $('.show_ta').val(0);
          $('#ta-siswa-pembayaran').val(0);
        }
        
        $('.id_rombel-edit-rombel').val(data.data[i].id_rombel);
        $('.tingkat-jenjang').val(data.data[i].tingkat);
        $('.nm_rombel-edit-rombel').val(data.data[i].nm_rombel);
        $('.id_prodi-edit-rombel').val(data.data[i].id_prodi);
        $('.id_guru-edit-rombel').val(data.data[i].id_guru);
        $('.nominal_spp-edit-rombel').val(data.data[i].nominal_spp);
        $('.keterangan-edit-rombel').val(data.data[i].keterangan_rombel);
      }
    },
    error : function(data)
    {
      alert(data);
    }
  });
} 

// Delete Prodi
function where_delete_rombel(id)
{
  $.ajax({
    url         : base_url+'admin/where_delete_rombel/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    cache:false,
    success: function(data){
      location.reload();
      alert("Data Non_Aktif");
    },
    error : function(data)
    {
      console.error("Non-Aktif Gagal!");
    }
  });
}


// Cari Rombel
pencarian_rombel();
function pencarian_rombel()
{
  $(document).ready(function(){
    $("#btn-search-rombel").click(function(){
      $(this).html("Cari...").attr("disabled", "disabled");
      let seacrh = $("#id-search-rombel").val(); 
      let ta = $("#rombel-ta").val();
      $.ajax({
        url: base_url + 'admin/pencarian_rombel/'+ seacrh + '/' + ta,
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) {
          let html="Memuat...";
          $('#data-rombel').html(html);
        },
        success: function(data){
          let btn = '<i class="fa fa-search"></i>';
          $("#btn-search-rombel").html(btn).removeAttr("disabled");
          console.log(data);
          let html = "";
          let no = 1;
          for (let i = 0; i < data.data.length; i++)
          { 
            html +=  '<tr>'+
                       '<td>'+no+'</td>'+
                      //  '<td>'+data.data[i].id_rombel+'</td>'+
                       '<td>'+data.data[i].id_ta+'</td>'+
                       '<td>'+data.data[i].tingkat+' - '+data.data[i].nm_rombel+'</td>'+
                       '<td>'+data.data[i].nm_prodi+'</td>'+
                       '<td>'+data.data[i].nm_guru+'</td>'+
                       '<td>'+data.data[i].nominal_spp+'</td>'+
                       '<td>'+data.data[i].keterangan_rombel+'</td>'+
                       '<td align="center">'+
                          '<button onclick="get_where_edit_rombel(' + "'" + data.data[i].id_rombel + "'" +')" name="button" data-toggle="modal" data-target="#edit-rombel" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                          '<button onclick="get_where_delete_rombel(' + "'" + data.data[i].id_rombel + "'" +')" class="btn btn-danger btn-xs" title="non-aktif"><i class="fa fa-trash"></i></button> &nbsp;'+
                       '</td>'+
                     '</tr>';
                     no++;
          }
          $('#data-rombel').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}