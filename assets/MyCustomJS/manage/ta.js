show_ta();
function show_ta()
{
  $(document).ready(function(){ 
    $.ajax({
    url: base_url + 'admin/pencarian_ta/'+ $("#id-search-ta").val(),
    type: 'POST',
    dataType: "json",
    beforeSend: function(e) {
        $(document).ajaxStart(function() { Pace.restart(); }); 
        let html="Memuat...";
        $('#data-ta').html(html);
    }, 
    success: function(data){
        let btn = '<i class="fa fa-search"></i>'; 
        console.log(data);
        let html = "";
        let no = 1;
        for (let i = 0; i < data.data.length; i++)
        {
        if (data.data[i].status_aktif_ta == 1) {
            html +=  '<tr>'+
                       '<td>'+no+'</td>'+
                       '<td>'+data.data[i].id_ta+'</td>'+
                       '<td align="center">'+
                            '<button class="btn btn-flat" disabled><i class="fa fa-check-circle-o" style="color:green"> Aktif</i></button> &nbsp;'+
                       '</td>'+
                       '<td>'+data.data[i].keterangan_ta+'</td>'+
                       '<td align="center">'+
                            '<button onclick="where_edit_ta(' + "'" + data.data[i].id_ta + "'" + '/' + "'" + data.data[i].status_aktif_ta + "'"+')" name="button" data-toggle="modal" data-target="#edit-ta" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            
                       '</td>'+
                     '</tr>';
            no++; 
        } else {
            html +=  '<tr>'+
                       '<td>'+no+'</td>'+
                       '<td>'+data.data[i].id_ta+'</td>'+
                       '<td align="center">'+
                            '<button onclick="aktifkan_ta(' + "'" + data.data[i].id_ta + "'" +')" class="btn btn-flat btn-success" style="color:#fff"> Aktifkan</button> &nbsp;'+
                       '</td>'+
                       '<td>'+data.data[i].keterangan_ta+'</td>'+
                       '<td align="center">'+
                            '<button onclick="where_edit_ta(' + "'" + data.data[i].id_ta + "'" +')" name="button" data-toggle="modal" data-target="#edit-ta" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            
                       '</td>'+
                     '</tr>';
            no++; 
        }
        }
        $('#data-ta').html(html);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.error(xhr.responseText);
    }
    });
}); 
}

// Cari Tahun Ajaran
pencarian_ta();
function pencarian_ta()
{
  $(document).ready(function(){
    $(".btn-search-ta").click(function(){ 
      $.ajax({
        url: base_url + 'admin/pencarian_ta/'+ $("#id-search-ta").val(),
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) {
            $(document).ajaxStart(function() { Pace.restart(); }); 
            let html="Memuat...";
            $('#data-ta').html(html);
        }, 
        success: function(data){
          let btn = '<i class="fa fa-search"></i>'; 
          console.log(data);
          let html = "";
          let no = 1;
          for (let i = 0; i < data.data.length; i++)
          {
            if (data.data[i].status_aktif_ta == 1) {
                html +=  '<tr>'+
                       '<td>'+no+'</td>'+
                       '<td>'+data.data[i].id_ta+'</td>'+
                       '<td align="center">'+
                            '<button class="btn btn-flat" disabled><i class="fa fa-check-circle-o" style="color:green"> Aktif</i></button> &nbsp;'+
                       '</td>'+
                       '<td>'+data.data[i].keterangan_ta+'</td>'+
                       '<td align="center">'+
                            '<button onclick="where_edit_ta(' + "'" + data.data[i].id_ta + "'" +')" name="button" data-toggle="modal" data-target="#edit-ta" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            
                       '</td>'+
                     '</tr>';
            } else {
                html +=  '<tr>'+
                       '<td>'+no+'</td>'+
                       '<td>'+data.data[i].id_ta+'</td>'+
                       '<td align="center">'+
                            '<button onclick="aktifkan_ta(' + "'" + data.data[i].id_ta + "'" +')" class="btn btn-flat btn-success" style="color:#fff"> Aktifkan</button> &nbsp;'+
                       '</td>'+
                       '<td>'+data.data[i].keterangan_ta+'</td>'+
                       '<td align="center">'+
                            '<button onclick="where_edit_ta(' + "'" + data.data[i].id_ta + "'" +')" name="button" data-toggle="modal" data-target="#edit-ta" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                            
                       '</td>'+
                     '</tr>';
                no++; 
            }
          }
          $('#data-ta').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}
 

$(document).ready(function(){
    $('.form-insert-ta').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'admin/insert_ta',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            alert(data);
            location.reload();
        },
        error : function(data)
        {
            alert("Insert Gagal");
        }
        })
    });

    $('.form-update-ta').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'admin/update_ta',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            alert(data);
            location.reload();
        },
        error : function(data)
        {
            alert("Update Gagal");
        }
        })
    });
}); 

function where_edit_ta(id)
{
    $.ajax({
        url         : base_url+'admin/get_where_edit_ta/'+id,
        method      : 'POST',
        datatype    : 'JSON',
        cache:false,
        success: function(data){
            for (let i = 0; i < data.data.length; i++) {
                $('#id-ta-edit').val(data.data[i].id_ta);
                $('#keterangan-ta-edit').html(data.data[i].keterangan_ta);
            }
        },
        error : function(data)
        {
        console.error("Edit Gagal!");
        }
    });
}

// Delete Siswa
function where_delete_ta(id)
{
    $.ajax({
        url         : base_url+'admin/where_delete_ta/'+id,
        method      : 'POST',
        datatype    : 'JSON',
        cache:false,
        success: function(data){
        location.reload();
        alert("Data Non_Aktif");
        },
        error : function(data)
        {
        console.error("Non-Aktif Gagal!");
        }
    });
}

function aktifkan_ta(id_ta) {
    $(document).ready(function(){  
        $.ajax({
            url : base_url+'admin/aktifkan_ta/'+id_ta,
            method : "POST", 
            datatype : 'JSON',
            success: function(data){
                console.log(data);
                console.log("berhasil");
                let html = '';
                let no = 1;
                for (let i = 0; i < data.data.length; i++)
                {
                    if (data.data[i].status_aktif_ta == 1) {
                        html +=  '<tr>'+
                            '<td>'+no+'</td>'+
                            '<td>'+data.data[i].id_ta+'</td>'+
                            '<td align="center">'+
                                    '<button class="btn btn-flat" disabled><i class="fa fa-check-circle-o" style="color:green"> Aktif</i></button> &nbsp;'+
                            '</td>'+
                            '<td>'+data.data[i].keterangan_ta+'</td>'+
                            '<td align="center">'+
                                    '<button onclick="where_edit_ta(' + "'" + data.data[i].id_ta + "'" +')" name="button" data-toggle="modal" data-target="#edit-ta" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                                    
                            '</td>'+
                            '</tr>';
                    } else {
                        html +=  '<tr>'+
                            '<td>'+no+'</td>'+
                            '<td>'+data.data[i].id_ta+'</td>'+
                            '<td align="center">'+
                                    '<button onclick="aktifkan_ta(' + "'" + data.data[i].id_ta + "'" +')" class="btn btn-flat btn-success" style="color:#fff"> Aktifkan</button> &nbsp;'+
                            '</td>'+
                            '<td>'+data.data[i].keterangan_ta+'</td>'+
                            '<td align="center">'+
                                    '<button onclick="where_edit_ta(' + "'" + data.data[i].id_ta + "'" +')" name="button" data-toggle="modal" data-target="#edit-ta" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                                    
                            '</td>'+
                            '</tr>';
                        no++; 
                    }
                }
                $('#data-ta').html(html); 
            },
            error : function(data){ 
                console.log(data);
            }
        });
    }); 
}