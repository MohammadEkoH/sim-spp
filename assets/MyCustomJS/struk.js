let base_url = 'http://localhost/sim_spp/';

get_tr_struk();
function get_tr_struk()
{
  $.ajax({
    url         : base_url+'petugas/get_tr_struk/' + $('#id-struk').val(),
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      let bulan="";
      
      for (let i = 0; i < data.data.length; i++)
      {   
        $('#id-agt-rombel-yo').val(data.data[i].id_tr_rombel);
        $('#tgl-tr').html(data.data[i].tgl_tr);
        $('#waktu-tr').html(data.data[i].waktu_tr);
        $('#dibayar').html(data.data[i].dibayarkan);
        $('#spp-bulan').html(data.data[i].bulan);
      }
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}
 
get_siswa_struk();
function get_siswa_struk()
{ 
  $.ajax({
    url         : base_url+'petugas/get_siswa_struk/' + $('#id-agt-rombel-yo').val(),
    method      : 'POST',
    datatype    : 'JSON',
    success     : function(data)
    {
      console.log(data);
      for (let i = 0; i < data.data.length; i++)
      {
        $('#id_ta-siswa').html(data.data[i].id_ta);
        $('#no_induk-siswa').html(data.data[i].no_induk);
        $('#nm-siswa').html(data.data[i].nm_siswa);
        $('#kelas-siswa').html(data.data[i].tingkat + ' - ' + data.data[i].nm_rombel);
      }
    },
    error : function(data)
    {
      console.error('gagal');
    }
  });
}