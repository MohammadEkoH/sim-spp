pencarian_guru();
function pencarian_guru()
{
  $(document).ready(function(){
    $("#btn-search-guru").click(function(){
    //   $(this).html("Cari...").attr("disabled", "disabled");
      let status = $("#guru-aktif").val();
      let nama = $("#nama-guru").val();
      $.ajax({
        url: base_url + 'admin/pencarian_guru/'+ nama +'/'+ status,
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) { 
          let html="Memuat...";
          $('#data-guru').html(html);
        },
        success: function(data){
          let btn = '<i class="fa fa-search"></i>';
          $("#btn-search-guru").html(btn).removeAttr("disabled"); 
          let html = "";
          let no=1;
          for (let i = 0; i < data.data.length; i++)
          { 
            if(data.data[i].status_del_guru == 1){
              $('#guru-show').show();
              html += '<tr>'+
                        '<td>'+no+'</td>'+ 
                        '<td>'+data.data[i].email_guru+'</td>'+
                        '<td>'+data.data[i].nm_guru+'</td>'+
                        '<td>'+data.data[i].alamat+'</td>'+
                        '<td>'+data.data[i].no_hp+'</td>'+
                        '<td>'+data.data[i].keterangan_guru+'</td>'+
                        '<td align="center">'+
                          '<button onclick="get_where_edit_guru(' + "'" + data.data[i].id_guru + "'" +')" name="button" data-toggle="modal" data-target="#edit-guru" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                          '<button onclick="where_delete_guru(' + "'" + data.data[i].id_guru + "'" +')" name="button" title="non-aktif"  class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>'+
                        '</td>'+
                     '</tr>';
            }else{
              $('#guru-show').show();
              html += '<tr>'+
                        '<td>'+no+'</td>'+ 
                        '<td>'+data.data[i].email_guru+'</td>'+
                        '<td>'+data.data[i].nm_guru+'</td>'+
                        '<td>'+data.data[i].alamat+'</td>'+
                        '<td>'+data.data[i].no_hp+'</td>'+
                        '<td>'+data.data[i].keterangan_guru+'</td>'+
                        '<td align="center">'+ 
                          '<button onclick="where_restore_guru(' + "'" + data.data[i].id_guru + "'" +')" name="button" title="aktif" class="btn btn-success btn-xs"><i class="fa fa-repeat"></i></button>'+
                        '</td>'+
                     '</tr>'; 
            }
            no++;
          }
          $('#data-guru').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}

$(document).ready(function(){
  $('.form-insert-guru').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/insert_guru',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Insert Gagal");
    }
    })
  });

  $('.form-update-guru').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/update_guru',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Update Gagal");
    }
    })
  });
}); 
  
// Edit data guru
function get_where_edit_guru(id)
{
  $.ajax({
    url         : base_url+'admin/get_where_edit_guru/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    cache:false,
    success: function(data){
      let option="";
      for (let i = 0; i < data.data.length; i++) {
        $('#id-guru-edit').val(data.data[i].id_guru);
        $('#email-guru-edit').val(data.data[i].email_guru);
        $('#nm-guru-edit').val(data.data[i].nm_guru);
        $('#alamat-guru-edit').val(data.data[i].alamat);
        $('#no_hp-guru-edit').val(data.data[i].no_hp);
        $('.keterangan-guru-edit').val(data.data[i].keterangan_guru);
      }
    },
    error : function(data)
    {
      alert(data);
    }
  });
}
  
function where_delete_guru(id)
{
  $.ajax({
    url         : base_url+'admin/where_delete_guru/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    cache:false,
    success: function(data){
      alert(data);
      location.reload();
    },
    error : function(data)
    {
      console.error("Edit Gagal!");
    }
  });
}

function where_restore_guru(id)
{
  $.ajax({
    url         : base_url+'admin/where_restore_guru/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) { 
    },
    cache:false,
    success: function(data){
      alert(data);
      location.reload();
    },
    error : function(data)
    {
      console.error("Pemulihan Gagal!");
    }
  });
}