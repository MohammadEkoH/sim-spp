pencarian_siswa();
function pencarian_siswa()
{
  $(document).ready(function(){
    $("#btn-search-siswa").click(function(){
    //   $(this).html("Cari...").attr("disabled", "disabled"); 
      let nama = $("#nama-siswa").val();  
      let status = $("#siswa-aktif").val();  
      $.ajax({
        url: base_url + 'admin/pencarian_siswa/'+ nama +'/'+ status,
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) { 
          let html="Memuat...";
          $('#data-siswa').html(html); 
        },
        success: function(data){ 
          let btn = '<i class="fa fa-search"></i>';
          $("#btn-search-siswa").html(btn).removeAttr("disabled"); 
          let html = "";
          let no = 1;
          for (let i = 0; i < data.data.length; i++)
          {
            if (data.data[i].status_del_siswa == 1) {
              $('#siswa-show').show();
              html +=  '<tr>'+
                         '<td>'+no+'</td>'+
                         '<td>'+data.data[i].no_induk+'</td>'+
                         '<td>'+data.data[i].nm_siswa+'</td>'+
                         '<td>'+data.data[i].kota_lahir+', '+data.data[i].tgl_lahir+'</td>'+
                         '<td>'+data.data[i].alamat+'</td>'+
                         '<td>'+data.data[i].nm_ibu+'</td>'+
                         '<td>'+data.data[i].no_hp_1+'</td>'+
                         '<td>'+data.data[i].no_hp_2+'</td>'+
                         '<td>'+data.data[i].keterangan_siswa+'</td>'+
                         '<td align="center">'+
                          '<button onclick="where_edit_siswa(' + "'" + data.data[i].no_induk + "'" +')" name="button" data-toggle="modal" data-target="#edit-siswa" title="edit"  class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                          '<button onclick="where_delete_siswa(' + "'" + data.data[i].no_induk + "'" +')" name="button" title="non-aktif"  class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>'+
                         '</td>'+
                       '</tr>'; 
            } else {
              $('#siswa-show').show();
              html +=  '<tr>'+
                         '<td>'+no+'</td>'+
                         '<td>'+data.data[i].no_induk+'</td>'+
                         '<td>'+data.data[i].nm_siswa+'</td>'+
                         '<td>'+data.data[i].kota_lahir+', '+data.data[i].tgl_lahir+'</td>'+
                         '<td>'+data.data[i].alamat+'</td>'+
                         '<td>'+data.data[i].nm_ibu+'</td>'+
                         '<td>'+data.data[i].no_hp_1+'</td>'+
                         '<td>'+data.data[i].no_hp_2+'</td>'+
                         '<td>'+data.data[i].keterangan_siswa+'</td>'+
                         '<td align="center">'+ 
                          '<button onclick="where_restore_siswa(' + "'" + data.data[i].no_induk + "'" +')" name="button" title="aktif"  class="btn btn-success btn-xs"><i class="fa fa-repeat"></i></button>'+
                         '</td>'+
                       '</tr>';
            }
            no++;
          }
          $('#data-siswa').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}
 
$(document).ready(function(){
  $('.form-insert-siswa').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/insert_siswa',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Insert Gagal");
    }
    })
  });

  $('.form-update-siswa').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/update_siswa',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Update Gagal");
    }
    })
  });
}); 
 

function where_edit_siswa(no_induk)
{
  $.ajax({
    url         : base_url+'admin/get_where_edit_siswa/'+no_induk,
    method      : 'POST',
    datatype    : 'JSON', 
    cache:false,
    success: function(data){
        for (let i = 0; i < data.data.length; i++) {
          $('#no-induk-siswa-edit').val(data.data[i].no_induk);
          $('#nm-siswa-edit').val(data.data[i].nm_siswa);
          $('#kota-lahir-siswa-edit').val(data.data[i].kota_lahir);
          $('#tgl-lahir-siswa-edit').val(data.data[i].tgl_lahir);
          $('#alamat-siswa-edit').val(data.data[i].alamat);
          $('#nm-ibu-siswa-edit').val(data.data[i].nm_ibu);
          $('#no-hp-1-siswa-edit').val(data.data[i].no_hp_1);
          $('#no-hp-2-siswa-edit').val(data.data[i].no_hp_2);
          $('.keterangan-siswa-edit').val(data.data[i].keterangan_siswa);
        }
    },
    error : function(data)
    {
      console.error("Edit Gagal!");
    }
  });
}

// Delete Siswa
function where_delete_siswa(no_induk)
{
  $.ajax({
    url         : base_url+'admin/where_delete_siswa/'+no_induk,
    method      : 'POST',
    datatype    : 'JSON', 
    cache:false,
    success: function(data){
      alert(data);
      location.reload();
    },
    error : function(data)
    {
      console.error("Non-Aktif Gagal!");
    }
  });
}

// restore Siswa
function where_restore_siswa(no_induk)
{
  $.ajax({
    url         : base_url+'admin/where_restore_siswa/'+no_induk,
    method      : 'POST',
    datatype    : 'JSON', 
    cache:false,
    success: function(data){
      alert(data);
      location.reload();
    },
    error : function(data)
    {
      console.error("Pemulihan Gagal!");
    }
  });
}