// Import guru
$(document).ready(function(){ 
    load_data_guru();
    function load_data_guru()
    {
        $.ajax({
        url: base_url+'import/fetch_guru',
        method:"POST",
        success:function(data){
            $('#customer_data_guru').html(data);
        }
        })
    }

    $('#import_form_guru').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url: base_url + 'import/import_guru',
            method:"POST",
            data:new FormData(this),
            contentType:false,
            cache:false,
            processData:false,
            success:function(data){
                $('#file_guru').val('');
                load_data_guru();
                alert(data);
            },
            error : function(data)
            {
                alert("Import Gagal");
            }
        })
    });
});

// Import siswa
$(document).ready(function(){
    load_data_siswa();
    function load_data_siswa()
    {
        $.ajax({
            url: base_url+'import/fetch_siswa',
            method:"POST",
            success:function(data){
                $('#customer_data_siswa').html(data);
            }
        })
    }

    $('#import_form_siswa').on('submit', function(event){
        event.preventDefault();
        $.ajax({
        url: base_url + 'import/import_siswa',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
            $('#file_siswa').val('');
            load_data_siswa();
            alert(data);
        },
        error : function(data)
        {
            alert("Import Gagal");
        }
        })
    });
});

// Import petugas
$(document).ready(function(){
    load_data_petugas();
    function load_data_petugas()
    {
      $.ajax({
        url: base_url+'import/fetch_petugas',
        method:"POST",
        success:function(data){
          $('#customer_data_petugas').html(data);
        }
      })
    }
  
    $('#import_form_petugas').on('submit', function(event){
    event.preventDefault();
      $.ajax({
        url: base_url + 'import/import_petugas',
        method:"POST",
        data:new FormData(this),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
          $('#file_petugas').val('');
          load_data_petugas();
          alert(data);
        },
        error : function(data)
        {
          alert("Import Gagal");
        }
      })
    });
  });