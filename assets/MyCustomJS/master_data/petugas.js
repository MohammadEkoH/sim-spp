cari_petugas();
function cari_petugas()
{
  $(document).ready(function(){
    $("#btn-search-petugas").click(function(){
      $(this).html("Cari...").attr("disabled", "disabled");
      let status = $("#petugas-aktif").val();
      let cari = $("#nama-petugas").val();
      // console.log(status);
      $.ajax({
        url: base_url + 'admin/pencarian_petugas/'+ cari +'/'+ status,
        type: 'POST',
        dataType: "json",
        beforeSend: function(e) {
          $(document).ajaxStart(function() { Pace.restart(); }); 
          let html="Memuat...";
          $('#data-petugas').html(html);
        },
        success: function(data){
          let btn = '<i class="fa fa-search"></i>';
          $("#btn-search-petugas").html(btn).removeAttr("disabled"); 
          let html = "";
          let no=1;
          for (let i = 0; i < data.data.length; i++)
          {
            if(data.data[i].status_del_petugas == 1){
              $('#petugas-show').show();
              html += '<tr>'+
                        '<td>'+no+'</td>'+ 
                        '<td>'+data.data[i].email_petugas+'</td>'+
                        '<td>'+data.data[i].nm_petugas+'</td>'+
                        '<td>'+data.data[i].alamat_petugas+'</td>'+
                        '<td>'+data.data[i].no_hp_petugas+'</td>'+
                        '<td>'+data.data[i].keterangan_petugas+'</td>'+
                        '<td align="center">'+
                          '<button onclick="get_where_edit_petugas(' + "'" + data.data[i].id_petugas + "'" +')" name="button" data-toggle="modal" data-target="#edit-petugas" title="edit" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button> &nbsp;'+
                          '<button onclick="where_delete_petugas(' + "'" + data.data[i].id_petugas + "'" +')" name="button" title="non-aktif" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>'+
                        '</td>'+
                     '</tr>';
            }else{
              $('#petugas-show').show();
              html += '<tr>'+
                        '<td>'+no+'</td>'+ 
                        '<td>'+data.data[i].email_petugas+'</td>'+
                        '<td>'+data.data[i].nm_petugas+'</td>'+
                        '<td>'+data.data[i].alamat_petugas+'</td>'+
                        '<td>'+data.data[i].no_hp_petugas+'</td>'+
                        '<td>'+data.data[i].keterangan_petugas+'</td>'+ 
                        '<td align="center">'+ 
                          '<button onclick="where_restore_petugas(' + "'" + data.data[i].id_petugas + "'" +')" name="button" title="aktif" class="btn btn-success btn-xs"><i class="fa fa-repeat"></i></button>'+
                        '</td>'+
                     '</tr>'; 
            }
            no++;
          }
          $('#data-petugas').html(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.error(xhr.responseText);
        }
      });
    });
  });
}

$(document).ready(function(){
  $('.form-insert-petugas').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/insert_petugas',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Insert Gagal");
    }
    })
  });

  $('.form-update-petugas').on('submit', function(event){
    event.preventDefault();
    $.ajax({
    url: base_url + 'admin/update_petugas',
    method:"POST",
    data:new FormData(this),
    contentType:false,
    cache:false,
    processData:false,
    success:function(data){ 
        alert(data);
        location.reload();
    },
    error : function(data)
    {
        alert("Update Gagal");
    }
    })
  });
}); 
 



function get_where_edit_petugas(id)
{
  $.ajax({
    url         : base_url+'admin/get_where_edit_petugas/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) {
      $(document).ajaxStart(function() { Pace.restart(); }); 
    },
    cache:false,
    success: function(data){
      let option="";
      for (let i = 0; i < data.data.length; i++) {
        $('#id-petugas-edit').val(data.data[i].id_petugas);
        $('#email-petugas-edit').val(data.data[i].email_petugas);
        $('#nm-petugas-edit').val(data.data[i].nm_petugas);
        $('#alamat-petugas-edit').val(data.data[i].alamat_petugas);
        $('#no_hp-petugas-edit').val(data.data[i].no_hp_petugas);
        $('.keterangan-petugas-edit').val(data.data[i].keterangan_petugas);
      }
    },
    error : function(data)
    {
      console.error(data);
    }
  });
}

function where_delete_petugas(id)
{
  $.ajax({
    url         : base_url+'admin/where_delete_petugas/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) {
      $(document).ajaxStart(function() { Pace.restart(); }); 
    },
    cache:false,
    success: function(data){
      alert(data);
      location.reload();
    },
    error : function(data)
    {
      console.error("Edit Gagal!");
    }
  });
}

function where_restore_petugas(id)
{
  $.ajax({
    url         : base_url+'admin/where_restore_petugas/'+id,
    method      : 'POST',
    datatype    : 'JSON',
    beforeSend: function(e) {
      $(document).ajaxStart(function() { Pace.restart(); }); 
    },
    cache:false,
    success: function(data){
      alert(data);
      location.reload();
    },
    error : function(data)
    {
      console.error("Pemulihan Gagal!");
    }
  });
}